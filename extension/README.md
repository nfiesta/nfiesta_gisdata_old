This is brief description of installation and usage of PostgreSQL extension *nfiesta_gisdata*. 
You may be interested also in more complete *nfiesta_pg* [installation wiki page](https://gitlab.com/nfiesta/nfiesta_pg/-/wikis/Installation).

Following commands should be considered as an example (based on Czech FMI use-case) and need to be adapteted.

# Create roles:

```sql
create role adm_nfiesta_gisdata;
create role app_nfiesta_gisdata;
```

# Create (production) DB
```sql
create database nfiesta_gisdata 
	ENCODING UTF8 
	LC_COLLATE 'cs_CZ.UTF-8' 
	LC_CTYPE 'cs_CZ.UTF-8' 
	OWNER adm_nfiesta_gisdata 
	TABLESPACE nfi_gisdata 
	TEMPLATE template0;
```

# Create extension

```bash
cd <your repository directory>
sudo make install
```

and possibly run regression tests (will create separate DB contrib_regression)

```bash
make installcheck
```

connect to your production DB and create extension (schema)

```sql
\c nfiesta_gisdata

create extension postgis;
create extension postgis_raster;
create extension nfiesta_gisdata;
```

# Access data -- using [Foreign Data Wrapper](https://wiki.postgresql.org/wiki/Foreign_data_wrappers) (FDW):

It is perfectly possible to load data to (yor own) tables located directly in your production DB.
In case you have already data in some different DB (and you do not want to duplicate them) it may be worth try to use [FDW](https://wiki.postgresql.org/wiki/Foreign_data_wrappers): 

Following example is showing how to get data from different PostgreSQL instance.

```sql
create extension postgres_fdw;

create server nfi_data foreign data wrapper postgres_fdw OPTIONS 
	(dbname 'nfi_data', extensions 'postgis, postgis_raster', host 'bran-pg', use_remote_estimate 'True');
alter server nfi_data owner to adm_nfiesta_gisdata ;

create server nfi_esta foreign data wrapper postgres_fdw OPTIONS 
	(dbname 'nfi_esta', extensions 'postgis, postgis_raster', host 'localhost', use_remote_estimate 'True');
alter server nfi_esta owner to adm_nfiesta_gisdata ;

set role adm_nfiesta_gisdata;

create user MAPPING FOR adm_nfiesta_gisdata	SERVER nfi_data OPTIONS ( password '<Some Password>', "user" '<Some User>'); -- to import foreign schema
create user MAPPING FOR app_nfiesta_gisdata	SERVER nfi_data OPTIONS ( password '<Some Password>', "user" '<Some User>');
create user MAPPING FOR "<Some User>"		SERVER nfi_data OPTIONS ( password '<Some Password>', "user" '<Some User>');

create user MAPPING FOR adm_nfiesta_gisdata	SERVER nfi_esta OPTIONS ( password '<Some Password>', "user" '<Some User>'); -- to import foreign schema
create user MAPPING FOR app_nfiesta_gisdata	SERVER nfi_esta OPTIONS ( password '<Some Password>', "user" '<Some User>');
create user MAPPING FOR "<Some User>"		SERVER nfi_esta OPTIONS ( password '<Some Password>', "user" '<Some User>');

----------------------------------------------nfi_data
----------------------------------------------field_data_gps
create schema field_data_gps;
GRANT USAGE ON SCHEMA field_data_gps TO public;
import foreign schema field_data_gps limit to (f_p_gps_nfi2) from server nfi_data into field_data_gps;
GRANT SELECT ON TABLE 	field_data_gps.f_p_gps_nfi2 TO public;

----------------------------------------------gisdata_external
create schema gisdata_external;
GRANT USAGE ON SCHEMA gisdata_external TO public;
import foreign schema gisdata_external limit to (
	f_a_cuzk_survey_plan, 
	r_dtm_4g) 
from server nfi_data into gisdata_external;
GRANT SELECT ON TABLE 	gisdata_external.f_a_cuzk_survey_plan TO public;
GRANT SELECT ON TABLE 	gisdata_external.r_dtm_4g TO public;

----------------------------------------------gisdata_internal
create schema gisdata_internal;
GRANT USAGE ON SCHEMA gisdata_internal TO public;

select 
	format('drop foreign table %s.%s;', nspname, relname) 
from pg_class 
inner join pg_namespace on (pg_class.relnamespace = pg_namespace.oid) 
where nspname = 'gisdata_internal' and relkind = 'f';
\gexec

import foreign schema gisdata_internal 
from server nfi_data into gisdata_internal;

select 
	format('GRANT SELECT ON TABLE %s.%s TO public;', nspname, relname) 
from pg_class 
inner join pg_namespace on (pg_class.relnamespace = pg_namespace.oid) 
where nspname = 'gisdata_internal' and relkind = 'f';
\gexec

----------------------------------------------lookup_tables
create schema lookup_tables;
GRANT USAGE ON SCHEMA lookup_tables TO public;
import foreign schema lookup_tables limit to (
	c_ku, 
	c_nuts1, 
	c_nuts2, 
	c_nuts3, 
	c_nuts4, 
	c_orp) 
from server nfi_data into lookup_tables;
GRANT SELECT ON TABLE	lookup_tables.c_ku TO public;
GRANT SELECT ON TABLE	lookup_tables.c_nuts1 TO public;
GRANT SELECT ON TABLE	lookup_tables.c_nuts2 TO public;
GRANT SELECT ON TABLE	lookup_tables.c_nuts3 TO public;
GRANT SELECT ON TABLE	lookup_tables.c_nuts4 TO public;
GRANT SELECT ON TABLE	lookup_tables.c_orp TO public;

----------------------------------------------nfiesta_etl
create schema nfiesta_etl;
GRANT USAGE ON SCHEMA nfiesta_etl TO public;

import foreign schema nfiesta_etl limit to (plots) from server nfi_data into nfiesta_etl;
GRANT SELECT ON TABLE 	nfiesta_etl.plots TO public;

----------------------------------------------nfi_esta
----------------------------------------------nfiesta
create schema nfiesta;
GRANT USAGE ON SCHEMA nfiesta TO public;

import foreign schema nfiesta limit to (
	c_auxiliary_variable_category
) from server nfi_esta into nfiesta;
GRANT SELECT ON TABLE	nfiesta.c_auxiliary_variable_category TO public;
```

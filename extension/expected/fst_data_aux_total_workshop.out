--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
\set srcdir `echo $SRC_DIR`
--------------------------------------------------------------------------------------------------;
-- t_config_collection --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/config_collection.csv'
CREATE FOREIGN TABLE csv.config_collection
(
	id int4 NOT NULL,
	auxiliary_variable int4 NULL,
	config_function int4 NOT NULL,
	"label" varchar NOT NULL,
	aggregated bool NOT NULL,
	catalog_name varchar NULL,
	schema_name varchar NULL,
	table_name varchar NULL,
	column_ident varchar NULL,
	column_name varchar NULL,
	unit float8 NULL,
	tag text NULL,
	description text NOT NULL,
	closed bool NOT NULL,
	edit_date timestamp NOT NULL,
	"condition" varchar NULL,
	total_points int4 NULL,
	ref_id_layer_points int4 NULL,
	ref_id_total int4 NULL
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');
INSERT INTO gisdata.t_config_collection(id, auxiliary_variable, config_function, label, aggregated,
catalog_name, schema_name, table_name, column_ident, column_name, unit, tag, description, closed,
edit_date, condition, total_points, ref_id_layer_points, ref_id_total)
SELECT
id, auxiliary_variable, config_function, label, aggregated,
catalog_name, schema_name, table_name, column_ident, column_name, unit, tag, description, closed,
edit_date, condition, total_points, ref_id_layer_points, ref_id_total
FROM
	csv.config_collection order by id;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
-- t_config --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/config.csv'
CREATE FOREIGN TABLE csv.config
(
	id int4 NOT NULL,
	auxiliary_variable_category int4 NULL,
	config_collection int4 NOT NULL,
	config_query int4 NOT NULL,
	"label" varchar NOT NULL,
	complete varchar NULL,
	categories varchar NULL,
	vector int4 NULL,
	raster int4 NULL,
	raster_1 int4 NULL,
	band int4 NULL,
	reclass int4 NULL,
	"condition" varchar NULL,
	tag text NULL,
	description text NOT NULL,
	edit_date timestamp NOT NULL
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');
INSERT INTO gisdata.t_config(id, auxiliary_variable_category, config_collection, config_query, label,
complete, categories, vector, raster, raster_1, band, reclass, condition, tag, description, edit_date)
SELECT
id, auxiliary_variable_category, config_collection, config_query, label,
complete, categories, vector, raster, raster_1, band, reclass, condition, tag, description, edit_date
FROM
	csv.config order by id;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
-- t_aux_total --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/aux_total.csv'
CREATE FOREIGN TABLE csv.aux_total
(
	id int4 NOT NULL,
	config int4 NOT NULL,
	estimation_cell int4 NULL,
	cell int4 NULL,
	aux_total float8 NOT NULL,
	est_date timestamp DEFAULT now() NOT NULL,
	ext_version int4 NOT NULL,
	gui_version int4 NOT NULL
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');
INSERT INTO gisdata.t_aux_total(id, config, estimation_cell, cell, aux_total, est_date, ext_version, gui_version)
SELECT
id, config, estimation_cell, cell, aux_total, est_date, ext_version, gui_version
FROM
	csv.aux_total order by id;
select setval('gisdata.seq__t_aux_total_id',(select max(id) from gisdata.t_aux_total));
 setval  
---------
 3769199
(1 row)

--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;
analyze gisdata.t_config_collection;
analyze gisdata.t_config;
analyze gisdata.t_aux_total;

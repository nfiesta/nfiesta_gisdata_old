--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance WITH the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes WITHin any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------
-- [INSPIRE 25x25km] -- DLT-coniferous X TCD
-- STEP 1
with
w1 as 	(
		select * from gisdata.fn_get_gids4aux_total_app
			(
			113,
			array[48,52,53],
			(select max(id) from gisdata.c_gui_version),
			false
			)
		),
w2 as 	(	
		select
		(gisdata.fn_get_aux_total_app(config_id,estimation_cell,gid)) as res
		from w1 where step = 1
		order by estimation_cell, gid
		)
insert into gisdata.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version)
select 
	(res).config,
	(res).estimation_cell,
	(res).cell,
	(res).aux_total,
	(res).ext_version,
	(select max(id) from gisdata.c_gui_version) as gui_version
from
	w2 order by (res).cell
;
-- STEP 2
with
w1 as 	(
		select * from gisdata.fn_get_gids4aux_total_app
			(
			113,
			array[48,52],
			(select max(id) from gisdata.c_gui_version),
			false
			)
		),
w2 as 	(
		select
		(gisdata.fn_get_aux_total4estimation_cell_app(config_id,estimation_cell,gid,false)) as res
		from w1 where step = 2 order by estimation_cell
		)
insert into gisdata.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version)
select 
	(res).config,
	(res).estimation_cell,
	(res).cell,
	(res).aux_total,
	(res).ext_version,
	(select max(id) from gisdata.c_gui_version) as gui_version
from
	w2 order by (res).estimation_cell
;
-------------------------------------------------
-- [INSPIRE 25x25km] -- DLT-broadleaves X TCD
-- STEP 1
with
w1 as 	(
		select * from gisdata.fn_get_gids4aux_total_app
			(
			114,
			array[48,52,53],
			(select max(id) from gisdata.c_gui_version),
			false
			)
		),
w2 as 	(	
		select
		(gisdata.fn_get_aux_total_app(config_id,estimation_cell,gid)) as res
		from w1 where step = 1
		order by estimation_cell, gid
		)
insert into gisdata.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version)
select 
	(res).config,
	(res).estimation_cell,
	(res).cell,
	(res).aux_total,
	(res).ext_version,
	(select max(id) from gisdata.c_gui_version) as gui_version
from
	w2 order by (res).cell
;
-- STEP 2
with
w1 as 	(
		select * from gisdata.fn_get_gids4aux_total_app
			(
			114,
			array[48,52],
			(select max(id) from gisdata.c_gui_version),
			false
			)
		),
w2 as 	(
		select
		(gisdata.fn_get_aux_total4estimation_cell_app(config_id,estimation_cell,gid,false)) as res
		from w1 where step = 2 order by estimation_cell
		)
insert into gisdata.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version)
select 
	(res).config,
	(res).estimation_cell,
	(res).cell,
	(res).aux_total,
	(res).ext_version,
	(select max(id) from gisdata.c_gui_version) as gui_version
from
	w2 order by (res).estimation_cell
;
-------------------------------------------------
-- [INSPIRE 25x25km] -- DLT-others X TCD
with
w1 as 	(
		select * from gisdata.fn_get_gids4aux_total_app
			(
			115,
			array[48,52,53],
			(select max(id) from gisdata.c_gui_version),
			false
			)
		),
w2 as 	(	
		select
		(gisdata.fn_get_aux_total_app(config_id,estimation_cell,gid)) as res
		from w1 where step = 2
		order by estimation_cell, gid
		)
insert into gisdata.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version)
select 
	(res).config,
	(res).estimation_cell,
	(res).cell,
	(res).aux_total,
	(res).ext_version,
	(select max(id) from gisdata.c_gui_version) as gui_version
from
	w2 order by (res).cell
;
-------------------------------------------------
-- [INSPIRE 50x50km] -- DLT-coniferous X TCD
with
w1 as 	(
		select * from gisdata.fn_get_gids4aux_total_app
			(
			113,
			array[21],
			(select max(id) from gisdata.c_gui_version),
			false
			)
		),
w2 as 	(
		select
		(gisdata.fn_get_aux_total4estimation_cell_app(config_id,estimation_cell,gid,false)) as res
		from w1 order by estimation_cell
		)
insert into gisdata.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version)
select 
	(res).config,
	(res).estimation_cell,
	(res).cell,
	(res).aux_total,
	(res).ext_version,
	(select max(id) from gisdata.c_gui_version) as gui_version
from
	w2 order by (res).estimation_cell
;
-------------------------------------------------
-- [INSPIRE 50x50km] -- DLT-broadleaves X TCD
with
w1 as 	(
		select * from gisdata.fn_get_gids4aux_total_app
			(
			114,
			array[21],
			(select max(id) from gisdata.c_gui_version),
			false
			)
		),
w2 as 	(
		select
		(gisdata.fn_get_aux_total4estimation_cell_app(config_id,estimation_cell,gid,false)) as res
		from w1 order by estimation_cell
		)
insert into gisdata.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version)
select 
	(res).config,
	(res).estimation_cell,
	(res).cell,
	(res).aux_total,
	(res).ext_version,
	(select max(id) from gisdata.c_gui_version) as gui_version
from
	w2 order by (res).estimation_cell
;
-------------------------------------------------
-- [INSPIRE 50x50km] -- DLT-others X TCD
with
w1 as 	(
		select * from gisdata.fn_get_gids4aux_total_app
			(
			115,
			array[21],
			(select max(id) from gisdata.c_gui_version),
			false
			)
		),
w2 as 	(	
		select
		(gisdata.fn_get_aux_total_app(config_id,estimation_cell,gid)) as res
		from w1 where step = 2
		order by estimation_cell, gid
		)
insert into gisdata.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version)
select 
	(res).config,
	(res).estimation_cell,
	(res).cell,
	(res).aux_total,
	(res).ext_version,
	(select max(id) from gisdata.c_gui_version) as gui_version
from
	w2 order by (res).estimation_cell
;
-------------------------------------------------
-- CHECK
WITH
w1 AS	(
		SELECT config, estimation_cell, aux_total
		FROM gisdata.t_aux_total
		WHERE estimation_cell IN (SELECT id FROM gisdata.c_estimation_cell)
		and config in (113,114,115)
		)
SELECT
	c_estimation_cell.label AS estimation_cell_label,
	t_config.label AS area_domain_category,
	round(w1.aux_total::numeric,6) AS aux_total
FROM w1
INNER JOIN gisdata.t_config ON w1.config = t_config.id
INNER JOIN gisdata.c_estimation_cell ON w1.estimation_cell = c_estimation_cell.id
ORDER BY estimation_cell_label, area_domain_category;
 estimation_cell_label |            area_domain_category             |   aux_total    
-----------------------+---------------------------------------------+----------------
 25kmE4650N2950        | dlt_2012_100m X tcd_2012_100m - broadleaves |  355882.745117
 25kmE4650N2950        | dlt_2012_100m X tcd_2012_100m - coniferous  |  281368.998776
 25kmE4650N2950        | dlt_2012_100m X tcd_2012_100m - others      |       0.000000
 25kmE4675N2950        | dlt_2012_100m X tcd_2012_100m - broadleaves |  592605.744972
 25kmE4675N2950        | dlt_2012_100m X tcd_2012_100m - coniferous  |  293695.120189
 25kmE4675N2950        | dlt_2012_100m X tcd_2012_100m - others      |       0.000000
 25kmE4675N2975        | dlt_2012_100m X tcd_2012_100m - broadleaves |  395132.647226
 25kmE4675N2975        | dlt_2012_100m X tcd_2012_100m - coniferous  |  184402.555878
 25kmE4675N2975        | dlt_2012_100m X tcd_2012_100m - others      |       0.000000
 50kmE465N295          | dlt_2012_100m X tcd_2012_100m - broadleaves | 1343621.137315
 50kmE465N295          | dlt_2012_100m X tcd_2012_100m - coniferous  |  759466.674842
 50kmE465N295          | dlt_2012_100m X tcd_2012_100m - others      |       0.000000
(12 rows)

---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

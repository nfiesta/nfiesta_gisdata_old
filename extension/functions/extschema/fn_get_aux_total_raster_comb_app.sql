--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_raster_comb_app
( 
	_config_id			integer,
	_gid				integer
)
RETURNS double precision AS
$BODY$
DECLARE
	_config_id_raster		integer;
	_config_id_raster_1		integer;
	_schema_name			character varying;
	_table_name			character varying;
	_column_name			character varying;
	_unit				double precision;
	_schema_name_1			character varying;
	_table_name_1			character varying;
	_column_name_1			character varying;
	_unit_1				double precision;
	_band				integer;
	_reclass			integer;
	_condition			character varying;
	_band_1				integer;
	_reclass_1			integer;
	_condition_1			character varying;
	_gid_text			character varying;
	_command			text;
	_res				double precision;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_raster_comb_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_raster_comb_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;						
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vstupni _config_id
	SELECT
		raster,
		raster_1
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id
	INTO
		_config_id_raster,
		_config_id_raster_1;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster z tabulky t_config_collection
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = (SELECT config_collection FROM @extschema@.t_config WHERE id = _config_id_raster)
	INTO
		_schema_name,
		_table_name,
		_column_name,
		_unit;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster_1 z tabulky t_config_collection
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = (SELECT config_collection FROM @extschema@.t_config WHERE id = _config_id_raster_1)
	INTO
		_schema_name_1,
		_table_name_1,
		_column_name_1,
		_unit_1;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster z tabulky t_config
	SELECT
		band,
		reclass,
		condition
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id_raster
	INTO
		_band,
		_reclass,
		_condition;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro raster_1 z tabulky t_config
	SELECT
		band,
		reclass,
		condition
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id_raster_1
	INTO
		_band_1,
		_reclass_1,
		_condition_1;
	-------------------------------------------------------------------------------------------
	-- prevedeni gidu na character varying
	_gid_text := _gid::character varying;
	-------------------------------------------------------------------------------------------
	-- vytvoreni (sestaveni) vysledneho sql dotazu pro ziskatni hodnoty aux_total
	SELECT @extschema@.fn_sql_aux_total_raster_comb_app
		(
		_config_id,
		_schema_name, _table_name, _column_name, _band, _reclass, _condition, _unit,
		_schema_name_1, _table_name_1, _column_name_1, _band_1, _reclass_1,  _condition_1, _unit_1,
		_gid_text
		)
	INTO _command;	
	-------------------------------------------------------------------------------------------
	EXECUTE ''||_command||'' INTO _res;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	RETURN _res;
	-------------------------------------------------------------------------------------------
END ;
$BODY$
LANGUAGE plpgsql VOLATILE ;
	
ALTER FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_raster_comb_app(integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total_raster_combination pro zadanou konfiguraci (config_id) a zadany gid geometrie z tabulky f_a_cell.';

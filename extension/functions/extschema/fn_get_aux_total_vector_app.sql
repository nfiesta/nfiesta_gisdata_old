--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_vector_app
(
    _config_id integer,
    _gid integer
)
  RETURNS double precision AS
$BODY$
DECLARE
	_config_collection		integer;
	_condition			character varying;
	_schema_name			character varying;
	_table_name			character varying;
	_column_name			character varying;
	_unit				double precision;
	_gid_text			character varying;
	_command			text;
	_res				double precision;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_vector_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_vector_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;
	-------------------------------------------------------------------------------------------
	-- proces ziskani konfiguraci pro vstupni _config_id
	SELECT
		config_collection,
		condition
	FROM
		@extschema@.t_config
	WHERE
		id = _config_id
	INTO
		_config_collection,
		_condition;
	-------------------------------------------------------------------------------------------
	SELECT
		schema_name,
		table_name,
		column_name,
		unit
	FROM
		@extschema@.t_config_collection
	WHERE
		id = _config_collection
	INTO
		_schema_name,
		_table_name,
		_column_name,
		_unit;
	-------------------------------------------------------------------------------------------
	-- prevedeni gidu na character varying
	_gid_text := _gid::character varying;
	-------------------------------------------------------------------------------------------
	-- vytvoreni (sestaveni) vysledneho sql dotazu pro ziskatni hodnoty aux_total
	SELECT @extschema@.fn_sql_aux_total_vector_app(_config_id, _schema_name, _table_name, _column_name, _condition, _unit, _gid_text)
	INTO _command;
	-------------------------------------------------------------------------------------------
	EXECUTE ''||_command||'' INTO _res;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------	
	RETURN _res;
	-------------------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_vector_app(integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total_vector pro zadanou konfiguraci (config_id) a zadany gidu geometrie z tabulky f_a_cell.';

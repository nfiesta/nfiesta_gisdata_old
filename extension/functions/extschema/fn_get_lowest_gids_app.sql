--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- fn_get_lowest_gids_app
---------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION @extschema@.fn_get_lowest_gids_app
(
	_appropriate_gids	integer[]	-- vstupem je gid(gidy) nejake urovne [gid odpovida gid z f_a_cell] a hledaji se gidy az te nejnizsi urovne, ktere tvori zadany vstupni gid 
)
RETURNS integer[] AS
$BODY$
DECLARE
	_res_1	integer[];
	_res	integer[];
BEGIN
	IF _appropriate_gids IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_lowest_gids_app: Vstupni argument _appropriate_gids nesmi byt NULL !';
	END IF;

	SELECT array_agg(cell order by cell) FROM @extschema@.cm_f_a_cell
	WHERE cell_sup IN (SELECT unnest(_appropriate_gids))
	INTO _res_1;

	IF _res_1 IS NULL
	THEN
		_res := _appropriate_gids;
	ELSE
		_res := @extschema@.fn_get_lowest_gids_app(_res_1);
	END IF;

	RETURN _res;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_lowest_gids_app(integer[]) IS
'Funkce vraci gidy nejnizsi urovne, a to pro zadany gid (zadany seznam gidu). Vracene gidy jsou cizim klicem na pole gid do tabulky f_a_cell.';
---------------------------------------------------------------------------------------------------
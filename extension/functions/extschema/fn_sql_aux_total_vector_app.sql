--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------;
--	fn_sql_aux_total_vector_app
--------------------------------------------------------------------------------;

DROP FUNCTION IF EXISTS @extschema@.fn_sql_aux_total_vector_app(
	integer, character varying, character varying,
	character varying, character varying,
	double precision, character varying);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_total_vector_app
(
    config_id		integer,
    schema_name		character varying,
    table_name		character varying,
    column_name		character varying,
    condition		character varying,
    unit		double precision,
    gid			character varying
)
  RETURNS text AS
$BODY$
DECLARE
	result	text;
BEGIN
	-----------------------------------------------------------------------------------
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_total_vector_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_total_vector_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_name
	IF (column_name IS NULL) OR (trim(column_name) = '')
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_total_vector_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- condition [povoleno NULL]
	IF (condition IS NULL) OR (trim(condition) = '')
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_total_vector_app: Hodnota parametru unit nesmí být NULL.';
	END IF;

	-- gid
	IF (gid IS NULL) OR (trim(gid) = '')
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_total_vector_app: Hodnota parametru gid nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro vypocet uhrnu
	result :=
		'
		WITH
		---------------------------------------------------------------
		w1 AS	(
			SELECT
				t1.geom as geom_cell,
				t2.geom as geom_layer
			FROM
				(SELECT * FROM @extschema@.f_a_cell WHERE gid = #GID#) as t1
			LEFT JOIN
				#SCHEMA_NAME#.#TABLE_NAME# AS t2 
			ON
				ST_Intersects(ST_Envelope(t1.geom),t2.#COLUMN_NAME#)
			AND
				NOT ST_Touches(t1.geom, t2.#COLUMN_NAME#)
			AND
				#CONDITION#
			)
		---------------------------------------------------------------
		-- vypocet rozlohy pruniku obou geometrii
		,w2 AS	(
			SELECT
				SUM(ST_Area(ST_Intersection (geom_layer,geom_cell))*#UNIT#) AS suma
			FROM
				w1
			)
		---------------------------------------------------------------		
		SELECT
			coalesce(suma,0) AS aux_total_#CONFIG_ID#_#GID#
		FROM 
			w2;
		';
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	result := replace(result, '#CONFIG_ID#', config_id::character varying);
	result := replace(result, '#SCHEMA_NAME#', schema_name);
	result := replace(result, '#TABLE_NAME#', table_name);
	result := replace(result, '#COLUMN_NAME#', column_name);
	result := replace(result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	result := replace(result, '#UNIT#', unit::character varying);
	result := replace(result, '#GID#', gid);
	-----------------------------------------------------------------------------------
	RETURN result;
	-----------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;

--------------------------------------------------------------------------------;

ALTER FUNCTION @extschema@.fn_sql_aux_total_vector_app(
	integer, character varying, character varying,
	character varying, character varying,
	double precision, character varying)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_app(
	integer, character varying, character varying,
	character varying, character varying,
	double precision, character varying)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_app(
	integer, character varying, character varying,
	character varying, character varying,
	double precision, character varying)
TO app_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_vector_app(
	integer, character varying, character varying,
	character varying, character varying,
	double precision, character varying)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_total_vector_app(
	integer, character varying, character varying,
	character varying, character varying,
	double precision, character varying)
IS
'Funkce vrací SQL textový řetězec pro výpočet úhrnu pomocné proměnné z vektorové vrstvy.';

--------------------------------------------------------------------------------;

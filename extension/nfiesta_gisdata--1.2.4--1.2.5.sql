--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------
-- schema export_api views
--------------------------------------------------------
DROP VIEW export_api.v_aux_total;

-- <view name="aux_total" schema="export_api" src="views/export_api/aux_total.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DDL
create or replace view export_api.aux_total as
with
w_config as		(
			-- vyber vypocitanych config z tabulky t_aux_total
			-- a vyber potrebnych atributu, kde je hlavne
			-- dulezity atribut config_query, ktery v pripade hodnoty 500 nam 
			-- rika, ze jde o refefenci [pozn. referencni data se v tabulce]
			-- t_aux_total neduplikuji
			select
				t_config.id,
				t_config.auxiliary_variable_category,
				t_config.config_collection,
				t_config.config_query,
				t_config.label,
				t_config.categories
			from
				@extschema@.t_config
			where
				config_collection in	(
							select distinct config_collection from @extschema@.t_config
							where id in (select distinct config from @extschema@.t_aux_total)
							)
			order 
				by id
			)
,w_no_ref as 		(
			-- vyber zaznamu z withu w_config, ktere nejsou referenci [config_query != 500]
			select w_config.* from w_config
			where w_config.config_query is distinct from 500
			)
,w_ref as 		(
			-- vyber zaznamu z withu w_config, ktere jsou referecni [config_query = 500]
			select w_config.* from w_config
			where w_config.config_query = 500
			)
,w_data_no_ref as 	(
			-- vyber dat z tabulky t_aux_total pro zaznamy z withu w_no_ref,
			-- kde vyber lze primo provest pres atribut config = id z withu
			-- w_no_ref a HLAVNE estimation_cell is not null
			select
				t_aux_total.id,
				t_aux_total.config as config_res,
				t_aux_total.estimation_cell,
				t_aux_total.aux_total,
				t_aux_total.ext_version,
				t_aux_total.gui_version
			from @extschema@.t_aux_total
			inner join w_no_ref
			on t_aux_total.config = w_no_ref.id
			and t_aux_total.estimation_cell is not null
			)
-------------------------------------------------------------------------------
,w_data_ref as 		(
			-- vyber dat z tabulky t_aux_total pro zaznamy z withu w_ref
			select
				t_aux_total.id,
				w_ref.id as config_res,
				t_aux_total.estimation_cell,
				t_aux_total.aux_total,
				t_aux_total.ext_version,
				t_aux_total.gui_version
			from @extschema@.t_aux_total 
			inner join w_ref
			on t_aux_total.config::character varying = w_ref.categories
			order by w_ref.id
			)
-------------------------------------------------------------------------------
,w_no_ref_max as 	(
			-- z withu w_data_nor_ref a z withu w_data_ref se musi pro unikatni klic,
			-- resp pro unikatni kombinaci config, estimation_cell, ext_version
			-- se musi vybrat zaznam pro nejaktualnejsi EXT_VERSION
			select config_res, estimation_cell, max(ext_version) as max_ext_version
			from w_data_no_ref group by config_res, estimation_cell
			)
-------------------------------------------------------------------------------
,w_ref_max as 		(
			select config_res, estimation_cell, max(ext_version) as max_ext_version
			from w_data_ref group by config_res, estimation_cell
			)
-------------------------------------------------------------------------------
,w_data_no_ref_res as 	(
			select
				w_data_no_ref.config_res as config,
				w_data_no_ref.estimation_cell,
				w_data_no_ref.aux_total
			from w_data_no_ref inner join w_no_ref_max
			on w_data_no_ref.config_res = w_no_ref_max.config_res
			and w_data_no_ref.estimation_cell = w_no_ref_max.estimation_cell
			and w_data_no_ref.ext_version = w_no_ref_max.max_ext_version
			)
-------------------------------------------------------------------------------
,w_data_ref_res as 	(
			select
				w_data_ref.config_res as config,
				w_data_ref.estimation_cell,
				w_data_ref.aux_total
			from w_data_ref inner join w_ref_max
			on w_data_ref.config_res = w_ref_max.config_res
			and w_data_ref.estimation_cell = w_ref_max.estimation_cell
			and w_data_ref.ext_version = w_ref_max.max_ext_version
			)
-------------------------------------------------------------------------------
,w_data_res as 		(
			select * from w_data_no_ref_res union all
			select * from w_data_ref_res
			)
-------------------------------------------------------------------------------
select
	t_config_collection.auxiliary_variable,
	t_config.auxiliary_variable_category,
	--w_data_res.config,
	w_data_res.estimation_cell,
	w_data_res.aux_total
from 
	w_data_res

	left join @extschema@.t_config on w_data_res.config = t_config.id
	left join @extschema@.t_config_collection on t_config.config_collection = t_config_collection.id
UNION ALL
SELECT
	0 AS auxiliary_variable,
	0 AS auxiliary_variable_category,
	estimation_cell,
	aux_total
FROM
	(SELECT estimation_cell, sum(ST_Area(geom))/10000.0 AS aux_total FROM @extschema@.f_a_cell GROUP BY estimation_cell) AS t1
ORDER BY
	auxiliary_variable_category, estimation_cell
;


-- authorization
ALTER TABLE export_api.aux_total OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.aux_total TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.aux_total TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.aux_total TO public;

-- documentation
COMMENT ON VIEW export_api.aux_total IS 'The view displays data from the t_aux_total table.';
COMMENT ON COLUMN export_api.aux_total.auxiliary_variable IS 'Foreign key to superior c_auxiliary_variable.';
COMMENT ON COLUMN export_api.aux_total.auxiliary_variable_category IS 'Foreign key to c_auxiliary_variable_category.';
COMMENT ON COLUMN export_api.aux_total.estimation_cell IS 'Foreign key to c_estimation_cell.';
COMMENT ON COLUMN export_api.aux_total.aux_total IS 'Total of auxiliary local density.';

-- </view>

DROP VIEW export_api.v_auxiliary_data;

-- <view name="auxiliary_data" schema="export_api" src="views/export_api/auxiliary_data.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DDL
CREATE OR REPLACE VIEW export_api.auxiliary_data AS
with
w_plots as 		(
			select
				country,
				strata_set,
				stratum,
				panel,
				cluster,
				plot
			from
				@extschema@.f_p_plot
			)
,w_config_collection as (
			-- a selection of calculated config_collection from table t_auxiliary_data
			-- and addition of attributes from table t_config, where config_query is the most important
			-- attribute, which in case of value 500 it tells it is only a reference 
			-- [note reference data are not duplicated in table t_auxiliary_data]
						select
							a.*,
							b.id as id_config,
							b.config_collection,
							b.config_query,
							b.label,
							b.categories
						from
								(
								select
									id,
									label,
									ref_id_layer_points,
									ref_id_total
								from
									@extschema@.t_config_collection
								where
									id in	(select distinct config_collection from @extschema@.t_auxiliary_data)
								) as a
						inner
						join	@extschema@.t_config as b
						
						on		a.ref_id_total = b.config_collection
						)
,w_no_ref as 	(
				-- selection of records from CTE w_config_collection, which are not a reference [config_query != 500]
				select w_config_collection.* from w_config_collection
				where w_config_collection.config_query is distinct from 500
				)
,w_ref as 		(
				-- selection of records from CTE w_config_collection, which are a reference [config_query = 500]
				select w_config_collection.* from w_config_collection
				where w_config_collection.config_query = 500
				)
,w_data_no_ref as 	(
					-- selection of data from table t_auxiliary_data for records from CTE w_no_ref,
					-- can be done right through the attribute config_collection = id from CTE
					-- w_no_ref and config = id_config from CTE w_no_ref
					select
						t_auxiliary_data.id,
						t_auxiliary_data.config_collection as config_collection_res,
						t_auxiliary_data.config as config_res,
						t_auxiliary_data.ident_point as pid,
						t_auxiliary_data.value,
						t_auxiliary_data.ext_version,
						t_auxiliary_data.gui_version
					from @extschema@.t_auxiliary_data
					inner join w_no_ref
					on t_auxiliary_data.config_collection = w_no_ref.id
					and t_auxiliary_data.config = w_no_ref.id_config
					)
,w_ref_attribute as 	(
						-- preparation of records with attributes for data selection from
						-- table t_auxiliary_data which area only references
						select
							w_ref_subselect.*,
							tcc_subselect.id as config_collection4auxiliary_data
						from
								(
								select
									w_ref.*,
									t_config.id as categories_integer,
									t_config.config_collection as config_colletion4cf600
								from
										w_ref
								inner
								join	@extschema@.t_config
								
								on		w_ref.categories = t_config.id::character varying 
								) as
									w_ref_subselect 
						inner
						join	(select * from @extschema@.t_config_collection where config_function = 600) as tcc_subselect
						
						on w_ref_subselect.config_colletion4cf600 = tcc_subselect.ref_id_total						
						)						
,w_data_ref as 		(
					-- selection of data from table t_auxiliary_data for records from CTE w_ref
					-- the selection is done through CTE w_ref_attribute, where join keywords are
					-- config_collection4auxiliary_data and categories_integer
					select
						t_auxiliary_data.id,
						w_ref_attribute.id as config_collection_res,
						w_ref_attribute.id_config as config_res,
						t_auxiliary_data.ident_point as pid,
						t_auxiliary_data.value,
						t_auxiliary_data.ext_version,
						t_auxiliary_data.gui_version						
					from
							@extschema@.t_auxiliary_data
					inner
					join	w_ref_attribute
					
					on t_auxiliary_data.config_collection = w_ref_attribute.config_collection4auxiliary_data
					and t_auxiliary_data.config = w_ref_attribute.categories_integer
					)
,w_no_ref_max as 	(
					-- from CTE w_data_nor_ref and from CTE w_data_ref a unique key must be gained,
					-- resp for unique combination of config_collection,config,ident_point,ext_version
					-- must be chosen a record for the most actual EXT_VERSION
					select config_collection_res, config_res, pid, max(ext_version) as max_ext_version
					from w_data_no_ref group by config_collection_res, config_res, pid
					)
,w_ref_max as 		(
					select config_collection_res, config_res, pid, max(ext_version) as max_ext_version
					from w_data_ref group by config_collection_res, config_res, pid
					)
,w_data_no_ref_res as 	(
						select
							w_data_no_ref.config_collection_res as config_collection,
							w_data_no_ref.config_res as config,
							w_data_no_ref.pid,
							w_data_no_ref.value
						from w_data_no_ref inner join w_no_ref_max
						on w_data_no_ref.config_collection_res = w_no_ref_max.config_collection_res
						and w_data_no_ref.config_res = w_no_ref_max.config_res
						and w_data_no_ref.pid = w_no_ref_max.pid
						and w_data_no_ref.ext_version = w_no_ref_max.max_ext_version
						)
,w_data_ref_res as 		(
						select
							w_data_ref.config_collection_res as config_collection,
							w_data_ref.config_res as config,
							w_data_ref.pid,
							w_data_ref.value
						from w_data_ref inner join w_ref_max
						on w_data_ref.config_collection_res = w_ref_max.config_collection_res
						and w_data_ref.config_res = w_ref_max.config_res
						and w_data_ref.pid = w_ref_max.pid
						and w_data_ref.ext_version = w_ref_max.max_ext_version
						)
,w_data_res as 			(
						select * from w_data_no_ref_res	union all
						select * from w_data_ref_res
						)
select
		w_plots.country,
		w_plots.strata_set,
		w_plots.stratum,
		w_plots.panel,
		w_plots.cluster,
		w_plots.plot,
		--w_data_res.config_collection,
		t_config_collection.auxiliary_variable,
		--w_data_res.config,
		t_config.auxiliary_variable_category,
		w_data_res.value,
		t_config.label as comment
from
		w_data_res
inner join	w_plots					on w_data_res.pid::character varying = w_plots.plot
left join 	@extschema@.t_config			on w_data_res.config = t_config.id
left join	@extschema@.t_config_collection		on t_config.config_collection = t_config_collection.id
UNION ALL
SELECT
	country,
	strata_set,
	stratum,
	panel,
	cluster,
	plot,
	0 AS auxiliary_variable,
	0 AS auxialiry_variable_category,
	1.0 AS value,
	'intercept'::text AS comment
FROM
	w_plots
ORDER BY auxiliary_variable_category, plot
;

-- authorization
ALTER TABLE export_api.auxiliary_data OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.auxiliary_data TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.auxiliary_data TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.auxiliary_data TO public;

-- documentation
COMMENT ON VIEW export_api.auxiliary_data IS 'View with the auxiliary plot data.';

COMMENT ON COLUMN export_api.auxiliary_data.country IS 'Identifier of the country. Foreign key to c_country.';
COMMENT ON COLUMN export_api.auxiliary_data.strata_set IS 'Character identifier of strata set.';
COMMENT ON COLUMN export_api.auxiliary_data.stratum IS 'Character identifier of stratum.';
COMMENT ON COLUMN export_api.auxiliary_data.panel IS 'Character identifier of panel.';
COMMENT ON COLUMN export_api.auxiliary_data.cluster IS 'Average of relative plot sampling weights per cluster. Is used to compute pix and pixy.';
COMMENT ON COLUMN export_api.auxiliary_data.plot IS 'Identifier of the plot within cluster.';
COMMENT ON COLUMN export_api.auxiliary_data.auxiliary_variable IS 'Foreign key to c_auxiliary_variable.';
COMMENT ON COLUMN export_api.auxiliary_data.auxiliary_variable_category IS 'Foreign key to c_auxiliary_variable_category.';
COMMENT ON COLUMN export_api.auxiliary_data.value IS 'Value of the auxiliary plot data.';
COMMENT ON COLUMN export_api.auxiliary_data.comment IS 'Comment.';


-- </view>

-- <view name="available_datasets" schema="export_api" src="views/export_api/available_datasets.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP VIEW IF EXISTS export_api.available_datasets;

-- DDL
CREATE OR REPLACE VIEW export_api.available_datasets AS
WITH
w_plots AS (
	SELECT
		country,
		strata_set,
		stratum,
		panel,
		cluster,
		plot
	FROM
		@extschema@.f_p_plot
)
	SELECT
		t1.panel,
		NULL::int AS reference_year_set,
		t2.auxiliary_variable_category
	FROM
		w_plots AS t1
	INNER JOIN
		export_api.auxiliary_data AS t2
	ON t1.plot = t2.plot
	GROUP BY
		t1.panel, t2.auxiliary_variable_category
;

-- authorization
ALTER TABLE export_api.available_datasets OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.available_datasets TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.available_datasets TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.available_datasets TO public;

-- documentation
COMMENT ON VIEW export_api.available_datasets IS 'View declaring for which panel are defined auxiliaries available.';


-- </view>

DROP VIEW export_api.v_estimation_cell_hierarchy;
-- <view name="estimation_cell_hierarchy" schema="export_api" src="views/export_api/estimation_cell_hierarchy.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DDL
create or replace view export_api.estimation_cell_hierarchy as
	with w_data as (
	select
		c_estimation_cell.id as c_estimation_cell__id,
		c_estimation_cell.label as c_estimation_cell__label,
		c_estimation_cell_sup.id as c_estimation_cell_sup__id,
		c_estimation_cell_sup.label as c_estimation_cell_sup__label,
		c_estimation_cell_sup.estimation_cell_collection as c_estimation_cell_sup__estimation_cell_collection
	from @extschema@.cm_f_a_cell
	inner join @extschema@.f_a_cell 						on cm_f_a_cell.cell = f_a_cell.gid
	inner join @extschema@.c_estimation_cell 					ON c_estimation_cell.id = f_a_cell.estimation_cell
	inner join @extschema@.f_a_cell 		as f_a_cell_sup 		on cm_f_a_cell.cell_sup = f_a_cell_sup.gid
	inner join @extschema@.c_estimation_cell 	as c_estimation_cell_sup 	ON c_estimation_cell_sup.id = f_a_cell_sup.estimation_cell
	group by 
		c_estimation_cell.id, 
		c_estimation_cell.label, 
		c_estimation_cell_sup.id, 
		c_estimation_cell_sup.label, 
		c_estimation_cell_sup.estimation_cell_collection
	)
	select 
		c_estimation_cell__id as cell,
		/*c_estimation_cell__label,
		c_estimation_cell_sup__estimation_cell_collection,
		array_agg(c_estimation_cell_sup__id) as agg__c_estimation_cell_sup__id,
		array_agg(c_estimation_cell_sup__label) as agg__c_estimation_cell_sup__label,*/
		(array_agg(c_estimation_cell_sup__id))[1] as cell_superior
	from w_data
	group by 
		c_estimation_cell__id, 
		c_estimation_cell__label, 
		c_estimation_cell_sup__estimation_cell_collection
	having count(*) = 1
	order by c_estimation_cell__id, c_estimation_cell_sup__estimation_cell_collection
;

-- authorization
ALTER TABLE export_api.estimation_cell_hierarchy OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.estimation_cell_hierarchy TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.estimation_cell_hierarchy TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.estimation_cell_hierarchy TO public;

-- documentation
COMMENT ON VIEW export_api.estimation_cell_hierarchy IS 'View providing hierarchy of estimation cells to nfi_esta ETL process.';
COMMENT ON COLUMN export_api.estimation_cell_hierarchy.cell IS 'Foreign key to c_estimation_cell.';
COMMENT ON COLUMN export_api.estimation_cell_hierarchy.cell_superior IS 'Foreign key to superior c_estimation_cell.';


-- </view>


--------------------------------------------------------------------------------;
-- add new version into c_ext_version and cm_ext_gui_version
--------------------------------------------------------------------------------;

	INSERT INTO @extschema@.c_ext_version(id, label, description)
	VALUES
		(900,'1.2.5','Version 1.2.5 - consolidated situation in export_api views.');

	INSERT INTO @extschema@.cm_ext_gui_version(ext_version, gui_version)
	VALUES
		(900,500);

--------------------------------------------------------------------------------;


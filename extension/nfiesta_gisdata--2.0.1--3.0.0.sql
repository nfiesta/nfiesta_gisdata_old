--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
/*
	Implementation of the allowed interval version of the extension => Issue 16 on GitLab
*/

---------------------------------------------------------------------------------------------------
-- add ext_version 3.0.0
---------------------------------------------------------------------------------------------------
INSERT INTO @extschema@.c_ext_version(id, label, description, label_en, description_en)
VALUES
	(
		1200,
		'3.0.0',
		'Verze 3.0.0 - extenze nfiesta_gisdata pro pomocná data. Rozšíření o povolený interval verzí extenze.',
		'3.0.0',
		'Version 3.0.0 - nfiesta_gisdata extension for auxiliary data. Implementation of the allowed interval version of the extension.'
	);
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- cm_ext_config_function --
---------------------------------------------------------------------------------------------------
CREATE TABLE @extschema@.cm_ext_config_function
(
	id serial NOT NULL,
	config_function int4 NOT NULL,
	ext_version_valid_from int4 NOT NULL,
	ext_version_valid_until int4 not null,
	active boolean not null default true,
	CONSTRAINT pkey__cm_ext_config_function PRIMARY KEY (id),
	CONSTRAINT ukey__cm_ext_config_function UNIQUE (config_function, ext_version_valid_from, ext_version_valid_until),
	CONSTRAINT fkey__cm_ext_config_function__c_config_function FOREIGN KEY (config_function)  REFERENCES @extschema@.c_config_function(id) ON UPDATE CASCADE,
	CONSTRAINT fkey__cm_ext_config_function__c_ext_version_1 FOREIGN KEY (ext_version_valid_from)  REFERENCES @extschema@.c_ext_version(id) ON UPDATE CASCADE,
	CONSTRAINT fkey__cm_ext_config_function__c_ext_version_2 FOREIGN KEY (ext_version_valid_until) REFERENCES @extschema@.c_ext_version(id) ON UPDATE CASCADE
);

ALTER TABLE @extschema@.cm_ext_config_function OWNER										TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE @extschema@.cm_ext_config_function										TO adm_nfiesta_gisdata;
GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE @extschema@.cm_ext_config_function	TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE @extschema@.cm_ext_config_function									TO public;

COMMENT ON TABLE @extschema@.cm_ext_config_function IS 'Mapping table between ext_version and config_function.';
COMMENT ON COLUMN @extschema@.cm_ext_config_function.id IS 'Primary key, identifier of the record.';
COMMENT ON COLUMN @extschema@.cm_ext_config_function.config_function IS 'Foreign key to id of c_config_function.';
COMMENT ON COLUMN @extschema@.cm_ext_config_function.ext_version_valid_from IS 'Foreign key to id of c_ext_version.';
COMMENT ON COLUMN @extschema@.cm_ext_config_function.ext_version_valid_until IS 'Foreign key to id of c_ext_version.';
COMMENT ON COLUMN @extschema@.cm_ext_config_function.active IS 'Identifier of the record which is active.';

COMMENT ON CONSTRAINT pkey__cm_ext_config_function ON @extschema@.cm_ext_config_function IS 'Primary key, identifier of the record.';
COMMENT ON CONSTRAINT ukey__cm_ext_config_function ON @extschema@.cm_ext_config_function IS 'Unique key for columns config_function, ext_version_valid_from, ext_version_valid_until.';
COMMENT ON CONSTRAINT fkey__cm_ext_config_function__c_config_function ON @extschema@.cm_ext_config_function IS 'Foreign key to table c_config_function.';
COMMENT ON CONSTRAINT fkey__cm_ext_config_function__c_ext_version_1 ON @extschema@.cm_ext_config_function IS 'Foreign key to table c_ext_version.';
COMMENT ON CONSTRAINT fkey__cm_ext_config_function__c_ext_version_2 ON @extschema@.cm_ext_config_function IS 'Foreign key to table c_ext_version.';

CREATE INDEX fki__cm_ext_config_function__c_config_function ON @extschema@.cm_ext_config_function USING btree (config_function);
COMMENT ON INDEX @extschema@.fki__cm_ext_config_function__c_config_function IS 'BTree index on foreign key fki__cm_ext_config_function__c_config_function.';

CREATE INDEX fki__cm_ext_config_function__c_ext_version_1 ON @extschema@.cm_ext_config_function USING btree (ext_version_valid_from);
COMMENT ON INDEX @extschema@.fki__cm_ext_config_function__c_ext_version_1 IS 'BTree index on foreign key fki__cm_ext_config_function__c_ext_version_1.';

CREATE INDEX fki__cm_ext_config_function__c_ext_version_2 ON @extschema@.cm_ext_config_function USING btree (ext_version_valid_until);
COMMENT ON INDEX @extschema@.fki__cm_ext_config_function__c_ext_version_2 IS 'BTree index on foreign key fki__cm_ext_config_function__c_ext_version_2.';

CREATE INDEX idx__cm_ext_gui_version__active ON @extschema@.cm_ext_config_function USING btree (active);
COMMENT ON INDEX @extschema@.idx__cm_ext_gui_version__active IS 'BTree index on column active.';

-- data
INSERT INTO @extschema@.cm_ext_config_function(config_function,ext_version_valid_from,ext_version_valid_until,active) values
(100,100,1100,false),
(200,100,1100,false),
(300,100,1100,false),
(400,100,1100,false),
(100,1200,1200,true),
(200,1200,1200,true),
(300,1200,1200,true),
(400,1200,1200,true);
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- cm_estimation_cell_collection
---------------------------------------------------------------------------------------------------
ALTER TABLE @extschema@.cm_estimation_cell_collection ADD COLUMN estimation_cell_collection_highest integer;
COMMENT ON COLUMN @extschema@.cm_estimation_cell_collection.estimation_cell_collection_highest IS 'Foreign key to id of c_estimation_cell_collection.';

ALTER TABLE @extschema@.cm_estimation_cell_collection
  ADD CONSTRAINT fkey__cm_ecc__c_ecc_3 FOREIGN KEY (estimation_cell_collection_highest)
      REFERENCES @extschema@.c_estimation_cell_collection (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__cm_ecc__c_ecc_3 ON @extschema@.cm_estimation_cell_collection IS 'Foreign key to id of c_estimation_cell_collection.';

CREATE INDEX fki__cm_ecc__c_ecc_3 ON @extschema@.cm_estimation_cell_collection USING btree (estimation_cell_collection_highest);
COMMENT ON INDEX @extschema@.fki__cm_ecc__c_ecc_3 IS 'BTree index on foreign key fkey__cm_ecc__c_ecc_3.';

--update @extschema@.cm_estimation_cell_collection set estimation_cell_collection_highest = 1 where id in (1,2,3,4,7);
--update @extschema@.cm_estimation_cell_collection set estimation_cell_collection_highest = 6 where id in (5,6);
--ALTER TABLE @extschema@.cm_estimation_cell_collection ALTER COLUMN estimation_cell_collection_highest SET NOT NULL;
---------------------------------------------------------------------------------------------------




---------------------------------------------------------------------------------------------------
-- Trigger functions --
---------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- <function name="fn_cm_ext_config_function_after_insert_app" schema="@extschema@" src="functions/extschema/fn_cm_ext_config_function_after_insert_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- Function: @extschema@.fn_cm_ext_config_function_after_insert_app()

-- DROP FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app();

CREATE OR REPLACE FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app()
  RETURNS trigger AS
$BODY$
BEGIN

	if new.active = false
	then
		raise exception 'fn_cm_ext_config_function_after_insert_app: New value of active must not by false!';
	end if;

	if	(
		select count(*) > 1
		from @extschema@.cm_ext_config_function
		where config_function = new.config_function
		and active = true
		)
	then
		raise exception 'fn_cm_ext_config_function_after_insert_app: For config_function = % are 2 or more records where active is true!',new.config_function;
	end if;

	RETURN NEW;
	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
ALTER FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app() OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app() TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app() TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app() TO public;

COMMENT ON FUNCTION @extschema@.fn_cm_ext_config_function_after_insert_app() IS 'Function checks that for each config_function argument must be only one record with value active = true.';
-- </function>
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- <function name="fn_cm_ext_config_function_after_update_app" schema="@extschema@" src="functions/extschema/fn_cm_ext_config_function_after_update_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- Function: @extschema@.fn_cm_ext_config_function_after_update_app()

-- DROP FUNCTION @extschema@.fn_cm_ext_config_function_after_update_app();

CREATE OR REPLACE FUNCTION @extschema@.fn_cm_ext_config_function_after_update_app()
  RETURNS trigger AS
$BODY$
BEGIN

	if	(
		select count(*) > 1
		from @extschema@.cm_ext_config_function
		where config_function = new.config_function
		and active = true
		)
	then
		raise exception 'fn_cm_ext_config_function_after_update_app: For config_function = % are 2 or more records where active is true!',new.config_function;
	end if;

	RETURN NEW;
	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
ALTER FUNCTION @extschema@.fn_cm_ext_config_function_after_update_app() OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_cm_ext_config_function_after_update_app() TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_cm_ext_config_function_after_update_app() TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_cm_ext_config_function_after_update_app() TO public;

COMMENT ON FUNCTION @extschema@.fn_cm_ext_config_function_after_update_app() IS 'Function checks that for each config_function argument must be only one record with value active = true.';
-- </function>
------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------



---------------------------------------------------------------------------------------------------
-- Functions --
---------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- <function name="fn_check_cell_sup_app" schema="@extschema@" src="functions/extschema/fn_check_cell_sup_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_check_cell_sup_app();

CREATE OR REPLACE FUNCTION @extschema@.fn_check_cell_sup_app()
RETURNS void AS
$BODY$
DECLARE
	_ec_highest	integer[];
	_check		integer;
BEGIN
	---------------------------------------------------
	SELECT array_agg(t.estimation_cell_collection_highest ORDER BY t.estimation_cell_collection_highest)
	FROM (SELECT DISTINCT estimation_cell_collection_highest FROM @extschema@.cm_estimation_cell_collection) AS t
	INTO _ec_highest;
	---------------------------------------------------
	FOR i IN 1..array_length(_ec_highest,1)
	LOOP
		WITH
		w1 AS	(
			SELECT id AS estimation_cell FROM @extschema@.c_estimation_cell
			WHERE estimation_cell_collection = _ec_highest[i]
			),
		w2 AS	(
			SELECT estimation_cell FROM @extschema@.f_a_cell WHERE gid IN
			(SELECT cell FROM @extschema@.cm_f_a_cell WHERE cell_sup IS NULL)
			AND estimation_cell IN (SELECT estimation_cell FROM w1)
			),
		w3 AS	(
			SELECT estimation_cell FROM w1 except
			SELECT estimation_cell FROM w2
			),
		w4 AS	(
			SELECT estimation_cell FROM w2 except
			SELECT estimation_cell FROM w1
			)
		SELECT count(t.*) FROM (SELECT * FROM w3 UNION ALL SELECT * FROM w4) AS t
		INTO _check;

		IF _check > 0
		THEN
			RAISE EXCEPTION 'Error 01: fn_check_cell_sup_app: For estimation_cell_collection = % miss null records in table cm_f_a_cell for column cell_sup!',_ec_highest[i];
		END IF;
			
	END LOOP;
	---------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
ALTER FUNCTION @extschema@.fn_check_cell_sup_app() OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_cell_sup_app() TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_cell_sup_app() TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_cell_sup_app() TO public;

COMMENT ON FUNCTION @extschema@.fn_check_cell_sup_app() IS
'Function checks that for all estimation_cell_collections are null records in table cm_f_a_cell for column cell_sup';
---------------------------------------------------------------------------------------------------
-- </function>
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- <function name="fn_get_aux_data_app" schema="@extschema@" src="functions/extschema/fn_get_aux_data_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_aux_data_app(integer, integer, boolean, integer, integer)
  
CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_data_app
(
	_config_collection 	integer,
	_config			integer,
	_intersects		boolean,
	_gid_start		integer,
	_gid_end		integer
)
RETURNS TABLE
(
	config_collection		integer,
	config				integer,
	gid				integer,
	value				double precision,
	ext_version			integer
) AS
$BODY$
DECLARE
	_ext_version_label_system			text;
	_ext_version_current				integer;
	_config_function_600				integer;
	_ref_id_layer_points				integer;
	_config_collection_4_config			integer;
	_config_query					integer;
	_vector						integer;			
	_raster						integer;
	_raster_1					integer;
	_band						integer;
	_reclass					integer;		
	_condition					character varying;
	_config_function				integer;
	_schema_name					character varying;
	_table_name					character varying;
	_column_ident					character varying;
	_column_name					character varying;
	_unit						double precision;
	_q						text;
	_ids4comb					integer[];
	_config_collection_transform_comb_i		integer;
	_config_collection_transform_comb		integer[];
	_check_gid					integer;
	_complete					character varying;
	_categories					character varying;
	_config_ids_text_complete			text;
	_complete_length				integer;
	_config_ids_complete				integer[];
	_config_ids4check_complete			integer[];
	_complete_exists				boolean[];
	_config_ids_text_categories			text;
	_categories_length				integer;
	_config_ids_categories				integer[];
	_config_ids4check_categories			integer[];
	_categories_exists				boolean[];
	_config_ids					integer[];
	_complete_categories_exists			boolean[];
	_config_collection_transform_i			integer;
	_aggregated					boolean;
	_config_collection_transform			integer[];
	_check_count_config_ids_i			integer;
	_check_count_config_ids				integer[];
	_check_gid_i					integer;
	_config_ids_4_complete_array			integer[];
	_config_collection_4_complete_array		integer[];
	_config_ids_4_categories_array			integer[];
	_config_collection_4_categories_array		integer[];
	_aggregated_200					boolean;
	_check_categories_i				double precision[];


	-- NEW --
	_ext_version_valid_from				integer;
	_ext_version_valid_until			integer;
	_ext_version_valid_from_label			character varying;
	_ext_version_valid_until_label			character varying;
	_config_function4version_interval		integer;		
BEGIN 
	-------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_data_app: Vstupni argument _config_collection nesmi byt NULL!';
	END IF;

	IF _config IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_data_app: Vstupni argument _config nesmi byt NULL!';
	END IF;

	IF _intersects IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_data_app: Vstupni argument _intersects nesmi byt NULL!';
	END IF;

	IF _gid_start IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_data_app: Vstupni argument _gid_start nesmi byt NULL!';
	END IF;

	IF _gid_end IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_data_app: Vstupni argument _gid_end nesmi byt NULL!';
	END IF;
	-------------------------------------------------------------
	-------------------------------------------------------------
	-- proces ziskani aktualni extenze nfiesta_gisdata
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_data_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_data_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;	
	-------------------------------------------------------------
	-------------------------------------------------------------	
	-- ziskani promennych z konfigurace tabulky t_config_collection pro _config_collection
	SELECT
		tcc.config_function,
		tcc.ref_id_layer_points
	FROM
		@extschema@.t_config_collection AS tcc
	WHERE
		id = _config_collection
	INTO
		_config_function_600,
		_ref_id_layer_points;
	-------------------------------------------------------------
	-- kontrola ziskanych promennych jestli nejsou NULL
	IF _config_function_600 IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam config_function v tabulce t_config_collection!',_config_collection;
	END IF;

	IF _ref_id_layer_points IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_data_app: Pro vstupni argument config_collection = % nenalezen zaznam ref_id_layer_points v tabulce t_config_collection!',_config_collection;
	END IF;
	-------------------------------------------------------------
	-- kontrola, ze config_function musi byt hodnota 600
	IF _config_function_600 IS DISTINCT FROM 600
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_get_aux_data_app: Pro vstupni argument config_collection = % nalezen v tabulce t_config_collection zaznam config_function, ktery ale neni hodnota 600!',_config_collection;
	END IF;	
	-------------------------------------------------------------
	-- ziskani promennych z konfigurace tabulky t_config pro _config
	SELECT
		tc.config_collection,
		tc.config_query,
		tc.vector,
		tc.raster,
		tc.raster_1,
		tc.band,
		tc.reclass,		
		tc.condition
	FROM
		@extschema@.t_config AS tc
	WHERE
		tc.id = _config
	INTO
		_config_collection_4_config,
		_config_query,
		_vector,
		_raster,
		_raster_1,
		_band,
		_reclass,
		_condition;
	-------------------------------------------------------------
	-- kontrola ziskanych promennych jestli nejsou NULL
	IF _config_collection_4_config IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_get_aux_data_app: Pro vstupni argument config = % nenalezen zaznam config_collection v tabulce t_config!',_config;
	END IF;	
		
	IF _config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 12: fn_get_aux_data_app: Pro vstupni argument config = % nenalezen zaznam config_query v tabulce t_config!',_config;
	END IF;	
	-------------------------------------------------------------
	-- ziskani promennych z konfigurace tabulky t_config_collection pro _config_collection_4_config
	SELECT
		tcc.config_function,
		tcc.schema_name,
		tcc.table_name,
		tcc.column_ident,
		tcc.column_name,
		tcc.unit
	FROM
		@extschema@.t_config_collection AS tcc
	WHERE
		tcc.id = _config_collection_4_config
	INTO
		_config_function,
		_schema_name,
		_table_name,
		_column_ident,
		_column_name,
		_unit;
	-------------------------------------------------------------
	-- kontrola ziskanych promennych jestli nejsou NULL
	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 13: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam config_function v tabulce t_config_collection!',_config_collection_4_config;
	END IF;
	-------------------------------------------------------------
	-- NEW --
	IF NOT(_config_function = ANY(array[100,200,300,400]))
	THEN
		RAISE EXCEPTION 'Error 14: fn_get_aux_data_app: Interni promenna _config_function = % musi byt hodnota 100, 200, 300 nebo 400!',_config_function;
	END IF;

	IF _config_function = ANY(array[100,200,400])
	THEN
		IF _config_function = 400 THEN _config_function4version_interval := 200; ELSE _config_function4version_interval := _config_function; END IF;

		-- zjisteni ext_version_valid_from a ext_version_valid_until
		SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
		FROM @extschema@.cm_ext_config_function AS cmcf
		WHERE cmcf.active = TRUE
		AND cmcf.config_function = _config_function4version_interval
		INTO
			_ext_version_valid_from,
			_ext_version_valid_until;		
	ELSE
		-- varianta VxR => _config_function = 300

		WITH
		w1 AS	(
			SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
			FROM @extschema@.cm_ext_config_function AS cmcf
			WHERE cmcf.active = TRUE
			AND cmcf.config_function = 100
			),
		w2 AS	(
			SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
			FROM @extschema@.cm_ext_config_function AS cmcf
			WHERE cmcf.active = TRUE
			AND cmcf.config_function = 200
			),
		w3 AS	(
			SELECT * FROM w1 UNION ALL
			SELECT * FROM w2
			)
		SELECT
			max(ext_version_valid_from),
			max(ext_version_valid_until)
		FROM
			w3
		INTO
			_ext_version_valid_from,
			_ext_version_valid_until;
	END IF;	
	
	IF (_ext_version_valid_from IS NULL OR _ext_version_valid_until IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 15: fn_get_aux_data_app: Pro interni promennou _config_function = % nenalezeno ext_version_valid_from nebo ext_version_valid_until v tabulce cm_ext_config_function!',_config_function;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_from  INTO _ext_version_valid_from_label;
	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_until INTO _ext_version_valid_until_label;
	-------------------------------------------------------------
	-------------------------------------------------------------	 
	CASE
	WHEN _config_query = 100 -- vetev pro zakladni protinani
	THEN
		-- kontrola ziskanych promennych pro protinani
		IF _config_function = ANY(array[100,200])
		THEN
			-- _schema_name
			IF ((_schema_name IS NULL) OR (trim(_schema_name) = ''))
			THEN
				RAISE EXCEPTION 'Chyba 16: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam schema_name v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- _table_name
			IF ((_table_name IS NULL) OR (trim(_table_name) = ''))
			THEN
				RAISE EXCEPTION 'Chyba 17: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam table_name v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- _column_ident
			IF (_column_ident IS NULL) OR (trim(_column_ident) = '')
			THEN
				RAISE EXCEPTION 'Chyba 18: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam column_ident v tabulce t_config_collection!',_config_collection_4_config;
			END IF;
			
			-- _column_name
			IF (_column_name IS NULL) OR (trim(_column_name) = '')
			THEN
				RAISE EXCEPTION 'Chyba 19: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam column_name v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- unit
			IF _unit IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 20: fn_get_aux_data_app: Pro argument config_collection = % nenalezen zaznam unit v tabulce t_config_collection!',_config_collection_4_config;
			END IF;

			-- pokud unit neni NULL => vynasobit vzdy hodnotou 10000
			_unit := _unit * 10000.0;
		END IF;
		-----------------------------------------------------------------------------------
		CASE
			WHEN (_config_function = 100)	-- VECTOR
			THEN
				-- raise notice 'PRUCHOD CQ 100 VEKTOR';
				
				-- kontrola, ze _intersects je opravdu nastaveno na TRUE
				IF _intersects = TRUE
				THEN
					_q := @extschema@.fn_sql_aux_data_vector_app(_config_collection,_config,_schema_name,_table_name,_column_ident,_column_name,_condition,_gid_start,_gid_end);
				ELSE
					RAISE EXCEPTION 'Chyba 21: fn_get_aux_data_app: Jde-li se o konfiguraci, ktera je protinaci (config_query = 100), pak vstupni argument _intersects musi byt TRUE!';
				END IF;

			WHEN (_config_function = 200)	-- RASTER
			THEN
				-- raise notice 'PRUCHOD CQ 100 RASTER';
			
				-- kontrola, ze _intersects je opravdu nastaveno na TRUE
				IF _intersects = TRUE
				THEN			
					_q := @extschema@.fn_sql_aux_data_raster_app(_config_collection,_config,_schema_name,_table_name,_column_ident,_column_name,_band,_reclass,_condition,_unit,_gid_start,_gid_end);
				ELSE
					RAISE EXCEPTION 'Chyba 22: fn_get_aux_data_app: Jde-li se o konfiguraci, ktera je protinaci (config_query = 100), pak vstupni argument _intersects musi byt TRUE!';
				END IF;

			WHEN (_config_function IN (300,400))	-- KOMBINACE
			THEN
				-- raise notice 'PRUCHOD CQ 100 KOMBINACE';	
							
				-- spojeni hodnot _vector,_raster,_raster_1 do pole a odstraneni NULL hodnoty z pole
				SELECT array_agg(t.ids)
				FROM (SELECT unnest(array[_vector,_raster,_raster_1]) AS ids) AS t
				WHERE t.ids IS NOT NULL
				INTO _ids4comb;

				IF array_length(_ids4comb,1) != 2
				THEN
					RAISE EXCEPTION 'Chyba 23: fn_get_aux_data_app: Pocet prvku v interni promenne (_ids4comb = %) musi byt 2! Respektive jde-li v konfiguraci o interakci, pak v tabulce t_config u id = % muze byt vyplnena hodnota jen pro kombinaci vector a raster nebo raster a raster_1.',_ids4comb, _config;
				END IF;

				-- proces ziskani ID config_collection pro vector a raster z neagregovane skupiny config_collection 600
				-- a zaroven kontrola PROVEDITELNOSTI, ze pro kombinaci jsou potrebna data jiz ulozena v DB
				
				FOR i IN 1..array_length(_ids4comb,1)
				LOOP
					-- zde v teto casti jde o CQ 100 a kombinaci, proto neni nutne
					-- se ohlizet na aggregated, ale vzdy se musi provest TRANSFORMACE config_collection
					-- pro VxR nebo pro RxR [obecne receno data pro provedeni kombinace uz musi existovat
					-- v 600vkove skupine]
					
					-- proces nalezeni transformovane config_collection
					SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
					WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _ids4comb[i])
					AND tcc.ref_id_layer_points = _ref_id_layer_points
					INTO _config_collection_transform_comb_i; 

					IF _config_collection_transform_comb_i IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 24: fn_get_aux_data_app: Pro provedeni interakce (konfigurace config_collection = % a config = %) nejsou v tabulce t_auxiliary_data ulozena pozadovana data!',_config_collection,_config;
					END IF;

					-- kontrola proveditelnoti kombinace
					IF	(
						SELECT count(t.gid) = 0
						FROM
							(
							SELECT DISTINCT tad.gid
							FROM @extschema@.t_auxiliary_data AS tad
							WHERE tad.config_collection = _config_collection_transform_comb_i
							AND tad.config = _ids4comb[i]
							AND (tad.ext_version >= _ext_version_valid_from AND tad.ext_version <= _ext_version_valid_until)
							AND (tad.gid >= _gid_start AND tad.gid <= _gid_end)
							) AS t
						)
					THEN
						RAISE EXCEPTION 'Chyba 25: fn_get_aux_data_app: Pro provedeni interakce (konfigurace config_collection = % a config = %) nejsou v tabulce t_auxiliary_data ulozena pozadovana data!',_config_collection,_config;
					END IF;

					IF i = 1
					THEN
						_config_collection_transform_comb := array[_config_collection_transform_comb_i];
					ELSE
						_config_collection_transform_comb := _config_collection_transform_comb || array[_config_collection_transform_comb_i];
					END IF;
				END LOOP;
				
				-- kontrola stejnych gid pro kombinaci
				WITH
				w1a AS	(
					SELECT
					tad1.*,
					max(tad1.est_date) OVER (PARTITION BY tad1.gid, tad1.config_collection, tad1.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad1
					WHERE tad1.config_collection = _config_collection_transform_comb[1]
					AND tad1.config = _ids4comb[1]
					AND (tad1.ext_version >= _ext_version_valid_from AND tad1.ext_version <= _ext_version_valid_until)
					AND (tad1.gid >= _gid_start AND tad1.gid <= _gid_end)
					),
				w1 AS	(
					SELECT w1a.gid FROM w1a WHERE w1a.est_date = w1a.max_est_date
					),
				-------------------------------------------------------------------				
				w2a AS	(
					SELECT
					tad2.*,
					max(tad2.est_date) OVER (PARTITION BY tad2.gid, tad2.config_collection, tad2.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad2
					WHERE tad2.config_collection = _config_collection_transform_comb[2]
					AND tad2.config = _ids4comb[2]
					AND (tad2.ext_version >= _ext_version_valid_from AND tad2.ext_version <= _ext_version_valid_until)
					AND (tad2.gid >= _gid_start AND tad2.gid <= _gid_end)
					),
				w2 AS	(
					SELECT w2a.gid FROM w2a WHERE w2a.est_date = w2a.max_est_date					
					),
				-------------------------------------------------------------------					
				w3 AS	(SELECT w1.gid FROM w1 EXCEPT SELECT w2.gid FROM w2),
				w4 AS	(SELECT w2.gid FROM w2 EXCEPT SELECT w1.gid FROM w1),
				w5 AS	(SELECT w3.gid FROM w3 UNION ALL SELECT w4.gid FROM w4)
				SELECT
					count(w5.gid) FROM w5 INTO _check_gid;

				IF _check_gid > 0
				THEN
					RAISE EXCEPTION 'Chyba 26: fn_get_aux_data_app: Pro konfiguraci config_collection = % a config = % nejsou v tabulce t_auxiliary_data vsechna potrebna data!',_config_collection,_config;
				END IF;

				_q :=
					'				
					WITH
					w1a AS	(
						SELECT
						tad1.*,
						max(tad1.est_date) OVER (PARTITION BY tad1.gid, tad1.config_collection, tad1.config) as max_est_date
						FROM @extschema@.t_auxiliary_data AS tad1
						WHERE tad1.config = $1
						AND tad1.config_collection = $6
						AND (tad1.ext_version >= $14 AND tad1.ext_version <= $15)
						AND (tad1.gid >= $12 AND tad1.gid <= $13)
						),
					w1 AS	(
						SELECT w1a.gid, w1a.value FROM w1a WHERE w1a.est_date = w1a.max_est_date
						),
					-----------------------------------------------------------					
					w2a AS	(
						SELECT
						tad2.*,
						max(tad2.est_date) OVER (PARTITION BY tad2.gid, tad2.config_collection, tad2.config) as max_est_date
						FROM @extschema@.t_auxiliary_data AS tad2
						WHERE tad2.config = $2
						AND tad2.config_collection = $11
						AND (tad2.ext_version >= $14 AND tad2.ext_version <= $15)
						AND (tad2.gid >= $12 AND tad2.gid <= $13)
						),
					w2 AS	(
						SELECT w2a.gid, w2a.value FROM w2a WHERE w2a.est_date = w2a.max_est_date
						),
					-----------------------------------------------------------				
					w3a AS	(
						SELECT
						tad3.*,
						max(tad3.est_date) OVER (PARTITION BY tad3.gid, tad3.config_collection, tad3.config) as max_est_date
						FROM @extschema@.t_auxiliary_data AS tad3
						WHERE tad3.config = $4
						AND tad3.config_collection = $3
						AND (tad3.ext_version >= $14 AND tad3.ext_version <= $15)
						AND (tad3.gid >= $12 AND tad3.gid <= $13)
						),
					w3 AS	(
						SELECT w3a.gid AS gid_except FROM w3a WHERE w3a.est_date = w3a.max_est_date
						),
					-----------------------------------------------------------						
					w4 AS	(
						SELECT
							$3 AS config_collection,
							$4 AS config,
							w1.gid,
							(w1.value * w2.value)::double precision AS value,
							$5 AS ext_version,
							CASE WHEN w3.gid_except IS NULL THEN FALSE ELSE TRUE END AS gid_exists
						FROM
								w1
						INNER JOIN 	w2 ON w1.gid = w2.gid
						LEFT  JOIN	w3 ON w1.gid = w3.gid_except
						)
					-----------------------------------------------------------	
					SELECT
						w4.config_collection,
						w4.config,
						w4.gid,
						w4.value,
						w4.ext_version
					FROM
						w4 WHERE w4.gid_exists = FALSE
					';
			ELSE
				RAISE EXCEPTION 'Chyba 27: fn_get_aux_data_app: Neznama hodnota _config_function = %!',_config_function;
		END CASE;
		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------
	WHEN _config_query IN (200,300,400,500) -- vetev pro soucet, doplnky nebo referenci
	THEN	
		IF _config_query = 500
		THEN
			-- raise notice 'PRUCHOD CQ 500';
			
			RAISE EXCEPTION 'Chyba 28: fn_get_aux_data_app: Jde-li o konfiguraci, ktera je referenci (config_query = %), tak u ni se protinani bodu neprovadi!',_config_query;
		END IF;

		---------------------------------------------------------------
		-- KONTROLA PROVEDITELNOSTI
		---------------------------------------------------------------
		-- pro config_query 200,300,400 => kontrola ze v DB jsou ulozena data pro complete a categories

		-- pokud se jedna o konfiguraci souctu nebo doplnku => pak nutna kontrola,
		-- ze complete nebo categories pro danou konfiguraci jsou jiz v DB pritomny
		-- a odpovidaji verzi spadajici do povoleneho intervalu VERZI [poznamka: tato
		-- kontrola je stejna jak pro vetev vypocet, tak i pro vetev prepocet]

		-- pokud nejde o agregovanou skupinu => pak se jako config_collection pouzije VSTUPNI config_collection
		-- pokud   jde o agregovanou skupinu => pak se jako config_collection pouzije transformace

		--------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------
		-- zjisteni complete a categories
		SELECT
			tc.complete,
			tc.categories
		FROM
			@extschema@.t_config AS tc
		WHERE
			tc.id = _config
		INTO
			_complete,
			_categories;
		-------------------------------------------
		-- _complete muze byt NULL
		-------------------------------------------
		IF _categories IS NULL	-- categories nesmi byt nikdy NULL
		THEN
			RAISE EXCEPTION 'Chyba 29: fn_get_aux_data_app: Pro vstupni argument _config = % nenalezen v tabulce t_config zaznam categories!',_config;
		END IF;
		-------------------------------------------
		-- _complete
		IF _complete IS NOT NULL
		THEN
			-- pridani ke _complete textu array[]
			_config_ids_text_complete := concat('array[',_complete,']');

			-- zjisteni poctu idecek tvorici _complete v
			EXECUTE 'SELECT array_length('||_config_ids_text_complete||',1)'
			INTO _complete_length;

			-- zjisteni existujicich konfiguracnich idecek pro _complete
			EXECUTE
			'SELECT array_agg(tc.id ORDER BY tc.id)
			FROM @extschema@.t_config AS tc
			WHERE tc.id in (select unnest('||_config_ids_text_complete||'))
			'
			INTO _config_ids_complete;

			-- splneni podminky stejneho poctu idecek
			IF _complete_length != array_length(_config_ids_complete,1)
			THEN
				RAISE EXCEPTION 'Chyba 30: fn_get_aux_data_app: Pro nektere nakonfigurovane complete = % neexistuje konfigurace (ID) v tabulce t_config!',_complete;
			END IF;

			-- kontrola zda i obsah je stejny
			EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text_complete||') AS ids) AS t'
			INTO _config_ids4check_complete;

			IF _config_ids4check_complete != _config_ids_complete
			THEN
				RAISE EXCEPTION 'Chyba 31: fn_get_aux_data_app: Pro nektere nakonfigurovane complete = % neexistuje konfigurace (ID) v tabulce t_config!',_complete;
			END IF;

			-- sestaveni _complete_exists
			SELECT array_agg(t.complete_exists) FROM (SELECT TRUE AS complete_exists, generate_series(1,_complete_length) AS id) AS t
			INTO _complete_exists;
			
		ELSE
			_complete_length := 0;
			_config_ids_complete := NULL::integer[];
			_complete_exists := NULL::boolean[];
		END IF;
		-------------------------------------------
		-- _categories
		
		-- bude se pracovat zvlast s _categories
		_config_ids_text_categories := concat('array[',_categories,']');

		-- zjisteni poctu idecek tvorici _categories v
		EXECUTE 'SELECT array_length('||_config_ids_text_categories||',1)'
		INTO _categories_length;	-- TOTO

		-- zjisteni existujicich konfiguracnich idecek pro _categories
		EXECUTE
		'SELECT array_agg(tc.id ORDER BY tc.id)
		FROM @extschema@.t_config AS tc
		WHERE tc.id in (select unnest('||_config_ids_text_categories||'))
		'
		INTO _config_ids_categories;

		-- splneni podminky stejneho poctu idecek
		IF _categories_length != array_length(_config_ids_categories,1)
		THEN
			RAISE EXCEPTION 'Chyba 32: fn_get_aux_data_app: Pro nektere nakonfigurovane categories = % neexistuje konfigurace (ID) v tabulce t_config!',_categories;
		END IF;

		-- kontrola zda i obsah je stejny
		EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text_categories||') AS ids) AS t'
		INTO _config_ids4check_categories;

		IF _config_ids4check_categories != _config_ids_categories
		THEN
			RAISE EXCEPTION 'Chyba 33: fn_get_aux_data_app: Pro nektere nakonfigurovane categories = % neexistuje konfigurace (ID) v tabulce t_config!',_categories;
		END IF;

		-- sestaveni _categories_exists
		SELECT array_agg(t.categories_exists) FROM (SELECT FALSE AS categories_exists, generate_series(1,_categories_length) AS id) AS t
		INTO _categories_exists;
		-------------------------------------------
		-- spojeni konfiguracnich idecek _complete/_categories
		IF _complete_length = 0
		THEN
			-- jen categories
			_config_ids := _config_ids_categories;
			_complete_categories_exists := _categories_exists;
		ELSE
			-- _complete || _categories
			_config_ids := _config_ids_complete || _config_ids_categories;
			_complete_categories_exists := _complete_exists || _categories_exists;
		END IF;
		--------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------

		------------------------------------------------------------------------------------------------------
		-- CYKLUS KONTROLY PROVEDITELNOSTI => cast zjisteni _config_collection_transform_i do POLE a naplneni promenne _complete_categories boolean[]
		------------------------------------------------------------------------------------------------------
		FOR i IN 1..array_length(_config_ids,1)
		LOOP
			-------------------------------------------------
			-------------------------------------------------
			-- proces zjisteni promenne _config_collection_tranform_i:
			IF _complete_categories_exists[i] = TRUE
			THEN			
				-- proces nalezeni transformovane config_collection [TRANSFORMACE]
				SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
				WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _config_ids[i])
				AND tcc.ref_id_layer_points = _ref_id_layer_points
				INTO _config_collection_transform_i;

			ELSE
				SELECT tcc.aggregated FROM @extschema@.t_config_collection AS tcc
				WHERE tcc.id =	(
						SELECT tc.config_collection
						FROM @extschema@.t_config AS tc
						WHERE tc.id = _config
						)
				INTO _aggregated;

				IF _aggregated = TRUE
				THEN
					-- proces nalezeni transformovane config_collection
					SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
					WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _config_ids[i])
					AND tcc.ref_id_layer_points = _ref_id_layer_points
					INTO _config_collection_transform_i;
				ELSE
					_config_collection_transform_i := _config_collection;
				END IF;
			END IF;
			-------------------------------------------------
			-------------------------------------------------
			-- proces sestaveni _config_collection_transform
			IF i = 1
			THEN
				_config_collection_transform := array[_config_collection_transform_i];
			ELSE
				_config_collection_transform := _config_collection_transform || array[_config_collection_transform_i];
			END IF;
			
		END LOOP;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------

		------------------------------------------------------------------------------------------------------
		-- CYKLUS KONTROLY PROVEDITELNOSTI --
		------------------------------------------------------------------------------------------------------
		FOR i IN 1..array_length(_config_ids,1)	-- pole _config_ids je spojeni complete/categories, pocet prvku odpovida poctu prvku v _config_collection_transform i _complete_categories
		LOOP										
			SELECT count(t.gid)
			FROM	(
				SELECT DISTINCT tad.gid FROM @extschema@.t_auxiliary_data AS tad
				WHERE tad.config_collection = _config_collection_transform[i]
				AND tad.config = _config_ids[i]
				AND (tad.ext_version >= _ext_version_valid_from AND tad.ext_version <= _ext_version_valid_until)
				AND (tad.gid >= _gid_start AND tad.gid <= _gid_end)
				) AS t
			INTO
				_check_count_config_ids_i;

			IF _check_count_config_ids_i = 0
			THEN
				RAISE EXCEPTION 'Chyba 34: fn_get_aux_data_app: V tabulce t_auxiliary_data pro [nektere complete nebo categories] = % a pro config_collection = % schazi data!',_config_ids[i],_config_collection_transform[i];
			END IF;

			IF i = 1
			THEN
				_check_count_config_ids := array[_check_count_config_ids_i];
			ELSE
				_check_count_config_ids := _check_count_config_ids || array[_check_count_config_ids_i];
			END IF;
			
		END LOOP;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------

		------------------------------------------------------------------------------------------------------
		-- kontrola stejneho poctu bodu u complete a categories
		------------------------------------------------------------------------------------------------------
		IF	(
			SELECT count(tt.ccci) IS DISTINCT FROM 1
			FROM (SELECT DISTINCT t.ccci FROM (SELECT unnest(_check_count_config_ids) AS ccci) AS t) AS tt
			)
		THEN
			RAISE EXCEPTION 'Chyba 35: fn_get_aux_data_app: Pocty bodu v tabulce t_auxiliary_data se pro [nektere complete nebo nektere categories] neshoduji! Jde o vstupni konfiguraci config_collection = % a config = %.',_config_collection,_config;
		END IF;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------

		------------------------------------------------------------------------------------------------------
		-- CYKLUS KONTROLY PROVEDITELNOSTI => cast kontroly stejnych gid
		------------------------------------------------------------------------------------------------------
		IF array_length(_config_ids,1) > 1
		THEN
			-- kontrola stejnych gid pro _config_ids
			FOR i IN 1..(array_length(_config_ids,1) - 1)
			LOOP
				WITH
				w1a AS	(
					SELECT
						tad1.*,
						max(tad1.est_date) OVER (PARTITION BY tad1.gid, tad1.config_collection, tad1.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad1
					WHERE tad1.config_collection = _config_collection_transform[1]
					AND tad1.config = _config_ids[1]
					AND (tad1.ext_version >= _ext_version_valid_from AND tad1.ext_version <= _ext_version_valid_until)
					AND (tad1.gid >= _gid_start AND tad1.gid <= _gid_end)
					),
				w1 AS	(
					SELECT w1a.gid FROM w1a WHERE w1a.est_date = w1a.max_est_date				
					),					
				-------------------------------------------------------------------
				w2a AS	(
					SELECT
						tad2.*,
						max(tad2.est_date) OVER (PARTITION BY tad2.gid, tad2.config_collection, tad2.config) as max_est_date 
					FROM @extschema@.t_auxiliary_data AS tad2
					WHERE tad2.config_collection = _config_collection_transform[i+1]
					AND tad2.config = _config_ids[i+1]
					AND (tad2.ext_version >= _ext_version_valid_from AND tad2.ext_version <= _ext_version_valid_until)
					AND (tad2.gid >= _gid_start AND tad2.gid <= _gid_end)
					),
				w2 AS	(
					SELECT w2a.gid FROM w2a WHERE w2a.est_date = w2a.max_est_date
					),
				-------------------------------------------------------------------
				w3 AS	(SELECT w1.gid FROM w1 EXCEPT SELECT w2.gid FROM w2),
				w4 AS	(SELECT w2.gid FROM w2 EXCEPT SELECT w1.gid FROM w1),
				w5 AS	(SELECT w3.gid FROM w3 UNION ALL SELECT w4.gid FROM w4)
				SELECT
					count(w5.gid) FROM w5
				INTO
					_check_gid_i;

				IF _check_gid_i IS DISTINCT FROM 0
				THEN
					RAISE EXCEPTION 'Chyba 36: fn_get_aux_data_app: U konfigurace config_collection = % a config = % se u kontroly proveditelnosti (cast shoda identifikace bodu indent_point) v tabulce t_auxiliary_data u nektereho complete nebo u nektereho categories neshoduji identifikace bodu gid!',_config_collection, _config;
				END IF;
			END LOOP;
		END IF;
		------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------

		------------------------------------------------------------------------------------------------------------------------------
		-- proces rozhozeni pole _config_ids a pole _config_collection_transform zpetne na cast complete a categories
		------------------------------------------------------------------------------------------------------------------------------
		FOR i IN 1..array_length(_complete_categories_exists,1)
		LOOP
			IF _complete_categories_exists[i] = TRUE	-- complete
			THEN
				IF i = 1
				THEN
					_config_ids_4_complete_array := array[_config_ids[i]];
					_config_collection_4_complete_array := array[_config_collection_transform[i]];
				ELSE
					_config_ids_4_complete_array := _config_ids_4_complete_array || array[_config_ids[i]];
					_config_collection_4_complete_array := _config_collection_4_complete_array || array[_config_collection_transform[i]];
				END IF;
			ELSE						-- categories
				IF i = 1
				THEN
					_config_ids_4_categories_array := array[_config_ids[i]];
					_config_collection_4_categories_array := array[_config_collection_transform[i]];
				ELSE
					_config_ids_4_categories_array := _config_ids_4_categories_array || array[_config_ids[i]];
					_config_collection_4_categories_array := _config_collection_4_categories_array || array[_config_collection_transform[i]];
				END IF;
			END IF;
		END LOOP;
		---------------------------------------------------------------
		-- kontrola ze delka pole _config_ids_4_complete_array odpovida _complete_length
		-- kontrola ze delka pole _config_collection_4_complete_array odpovida _complete_length
		IF _config_collection_4_complete_array IS NOT NULL
		THEN
			IF array_length(_config_ids_4_complete_array,1) != _complete_length
			THEN
				RAISE EXCEPTION 'Chyba 37: fn_get_aux_data_app: Pocet prvku v interni promenne _config_ids_4_complete_array neodpovida interni promenne _complete_length!';
			END IF;
		
			IF array_length(_config_collection_4_complete_array,1) != _complete_length
			THEN
				RAISE EXCEPTION 'Chyba 38 fn_get_aux_data_app: Pocet prvku v interni promenne _config_collection_4_complete_array neodpovida interni promenne _complete_length!';
			END IF;
		ELSE
			_config_collection_4_complete_array := NULL::integer[];
		END IF;
		---------------------------------------------------------------
		-- kontrola ze delka pole _config_ids_4_categories_array odpovida _categories_length
		-- kontrola ze delka pole _config_collection_4_categories_array odpovida _categories_length
		IF array_length(_config_ids_4_categories_array,1) != _categories_length
		THEN
			RAISE EXCEPTION 'Chyba 39: fn_get_aux_data_app: Pocet prvku v interni promenne _config_ids_4_categories_array neodpovida interni promenne _categories_length!';
		END IF;
			
		IF array_length(_config_collection_4_categories_array,1) != _categories_length
		THEN
			RAISE EXCEPTION 'Chyba 40: fn_get_aux_data_app: Pocet prvku v interni promenne _config_collection_4_categories_array neodpovida interni promenne _categories_length!';
		END IF;
		---------------------------------------------------------------	
		------------------------------------------------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------------------------------------------------

		
		---------------------------------------------------------------
		-- CONFIG_QUERY 200 --
										
		IF _config_query = 200
		THEN
			-- raise notice 'PRUCHOD CQ 200';

			---------------------------------------------
			-- kontrola, ze CQ 200 je agregace
			SELECT aggregated FROM @extschema@.t_config_collection
			WHERE id = (SELECT ref_id_total FROM @extschema@.t_config_collection WHERE id = _config_collection)
			INTO _aggregated_200;

			IF _aggregated_200 IS DISTINCT FROM TRUE
			THEN
				RAISE EXCEPTION 'Chyba 41: fn_get_aux_data_app: Jde-li o konfiguraci souctu [config_query = 200], pak musi jit vzdy o agregacni skupinu, respektive v konfiguraci musi platit aggregated = TRUE! Jde o vstupni konfiguraci config_collection = % a config = %.',_config_collection,_config;
			END IF;
			---------------------------------------------		

			-- provedeni sumy za categories pro jednotlive body
			_q :=
				'
				WITH
				w2a AS	(
					SELECT
						tad.gid,
						tad.value,
						tad.config_collection,
						tad.config,
						tad.ext_version,
						tad.est_date,
						max(tad.est_date) OVER (PARTITION BY tad.gid, tad.config_collection, tad.config) as max_est_date
					FROM
						@extschema@.t_auxiliary_data AS tad
					WHERE
						tad.config_collection IN (SELECT unnest($10))
					AND
						tad.config IN (SELECT unnest($9))
					AND
						(tad.ext_version >= $14 AND	tad.ext_version <= $15)
					AND
						(tad.gid >= $12 AND tad.gid <= $13)
					),
				w2 AS	(
					SELECT w2a.gid, w2a.value FROM w2a WHERE w2a.est_date = w2a.max_est_date
					),
				-------------------------------------------------------------------						
				w3 AS 	(
					SELECT
						w2.gid,
						sum(w2.value)::double precision AS value
					FROM
						w2
					GROUP
						BY w2.gid
					),
				-------------------------------------------------------------------	
				w4a AS	(
					SELECT
					tad1.*,
					max(tad1.est_date) OVER (PARTITION BY tad1.gid, tad1.config_collection, tad1.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad1
					WHERE tad1.config_collection = $3
					AND tad1.config = $4
					AND (tad1.ext_version >= $14 AND tad1.ext_version <= $15)
					AND (tad1.gid >= $12 AND tad1.gid <= $13)
					),
				w4 AS	(
					SELECT w4a.gid AS gid_except FROM w4a WHERE w4a.est_date = w4a.max_est_date
					),
				-------------------------------------------------------------------					
				w5 AS	(
					SELECT
						$3 AS config_collection,
						$4 AS config,
						w3.gid,
						w3.value,
						$5 AS ext_version,
						CASE WHEN w4.gid_except IS NULL THEN FALSE ELSE TRUE END AS gid_exists
					FROM
						w3 LEFT JOIN w4 ON w3.gid = w4.gid_except
					)
				-------------------------------------------------------------------
				SELECT
					w5.config_collection,
					w5.config,
					w5.gid,
					w5.value,
					w5.ext_version
				FROM
					w5 WHERE w5.gid_exists = FALSE
				';			
		END IF;		
		---------------------------------------------------------------
		-- CONFIG_QUERY 300 --
		IF _config_query = 300
		THEN
			-- raise notice 'PRUCHOD CQ 300';	
			-- kontrola, ze u vsech categories jsou opravdu hodnoty v intervalu 0 az 1
			FOR i IN 1..array_length(_config_ids_4_categories_array,1)
			LOOP		
				SELECT array_agg(t.value ORDER BY t.value)
				FROM	(
					SELECT DISTINCT tad.value::double precision
					FROM @extschema@.t_auxiliary_data AS tad
					WHERE tad.config_collection = _config_collection_4_categories_array[i]
					AND tad.config = _config_ids_4_categories_array[i]
					AND (tad.ext_version >= _ext_version_valid_from AND tad.ext_version <= _ext_version_valid_until)
					AND (tad.gid >= _gid_start AND tad.gid <= _gid_end)
					) AS t
				INTO
					_check_categories_i;

				IF _check_categories_i IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 42 fn_get_aux_data_app: Pro categories = % u konfigurace _config_collection = % a _config = % a _ext_version = % schazi data v tabulce t_auxiliary_data!',_config_ids_4_categories_array[i],_config_collection_4_categories_array[i],_config,_ext_version_current;
				END IF;

				FOR i IN 1..array_length(_check_categories_i,1)
				LOOP
					IF	(
						_check_categories_i[i] < 0.0	OR
						_check_categories_i[i] > 1.0
						)
					THEN
						RAISE EXCEPTION 'Chyba 43: fn_get_aux_data_app: Hodnoty values u categories = % u konfigurace _config_collection = % a _config = % a _ext_version = % nejsou v intervalu <0.0;1.0>!',_config_ids_4_categories_array[i],_config_collection_4_categories_array[i],_config,_ext_version_current;
					END IF;
				END LOOP;
				
			END LOOP;
			-- provedeni doplnku pro jednotlive body
			_q :=
				'
				WITH
				w2a AS	(
					SELECT
						tad.gid,
						round(tad.value::numeric,2) AS value,
						tad.config_collection,
						tad.config,
						tad.ext_version,
						tad.est_date,
						max(tad.est_date) OVER (PARTITION BY tad.gid, tad.config_collection, tad.config) as max_est_date
					FROM
						gisdata_test.t_auxiliary_data AS tad
					WHERE
						tad.config_collection IN (SELECT unnest($10))
					AND
						tad.config IN (SELECT unnest($9))
					AND
						(tad.ext_version >= $14 AND	tad.ext_version <= $15)
					AND
						(tad.gid >= $12 AND tad.gid <= $13)						
					),
				w2 AS	(
					SELECT w2a.* FROM w2a WHERE w2a.est_date = w2a.max_est_date
					),
				-------------------------------------------------------------------
				w3 AS	(
					SELECT
						w2.gid,
						(sum(w2.value))::double precision AS value
					FROM
						w2
					GROUP
						BY w2.gid
					),
				-------------------------------------------------------------------
				w4a AS	(
					SELECT
					tad1.*,
					max(tad1.est_date) OVER (PARTITION BY tad1.gid, tad1.config_collection, tad1.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad1
					WHERE tad1.config_collection = $3
					AND tad1.config = $4
					AND (tad1.ext_version >= $14 AND tad1.ext_version <= $15)
					AND (tad1.gid >= $12 AND tad1.gid <= $13)
					),
				w4 AS	(
					SELECT w4a.gid as gid_except FROM w4a WHERE w4a.est_date = w4a.max_est_date
					),
				-------------------------------------------------------------------
				w5 AS	(			
					SELECT
						$3 AS config_collection,
						$4 AS config,
						w3.gid,
						(1.0::double precision - w3.value) AS value,
						$5 AS ext_version,
						CASE WHEN w4.gid_except IS NULL THEN FALSE ELSE TRUE END AS gid_exists
					FROM
						w3 LEFT JOIN w4 ON w3.gid = w4.gid_except
					)
				-------------------------------------------------------------------
				SELECT
					w5.config_collection,
					w5.config,
					w5.gid,
					w5.value,
					w5.ext_version
				FROM
					w5 WHERE w5.gid_exists = FALSE
				';
		END IF;
		---------------------------------------------------------------
		-- CONFIG_QUERY 400 --
		IF _config_query = 400
		THEN
			-- raise notice 'PRUCHOD CQ 400';		

			IF _complete IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 44: fn_get_aux_data_app: Pro konfiguraci kategorie _config = % nenalezena hodnota complete v tabulce t_config!',_config;
			END IF;

			-- provedeni doplnku pro jednotlive body
			_q :=
				'
				WITH
				-------------------------------------------------------------------
				-- categories
				w2a AS	(
					SELECT
						tad.gid,
						(tad.value)::numeric AS value,
						tad.config_collection,
						tad.config,
						tad.ext_version,
						tad.est_date,
						max(tad.est_date) OVER (PARTITION BY tad.gid, tad.config_collection, tad.config) as max_est_date
					FROM
						@extschema@.t_auxiliary_data AS tad
					WHERE
						tad.config_collection IN (SELECT unnest($10))
					AND
						tad.config IN (SELECT unnest($9))
					AND
						(tad.ext_version >= $14 AND	tad.ext_version <= $15)
					AND
						(tad.gid >= $12 AND tad.gid <= $13)
					),
				w2 AS	(
					SELECT w2a.gid, w2a.value FROM w2a WHERE w2a.est_date = w2a.max_est_date
					),
				-------------------------------------------------------------------
				-- complete
				w4a AS	(
					SELECT
						tad.gid,
						(tad.value)::numeric AS value,
						tad.config_collection,
						tad.config,
						tad.ext_version,
						tad.est_date,
						max(tad.est_date) OVER (PARTITION BY tad.gid, tad.config_collection, tad.config) as max_est_date
					FROM
						@extschema@.t_auxiliary_data AS tad
					WHERE
						tad.config_collection IN (SELECT unnest($8))
					AND
						tad.config IN (SELECT unnest($7))
					AND
						(tad.ext_version >= $14 AND	tad.ext_version <= $15)
					AND
						(tad.gid >= $12 AND tad.gid <= $13)
					),
				w4 AS	(
					SELECT w4a.gid, w4a.value FROM w4a WHERE w4a.est_date = w4a.max_est_date					
					),		
				-------------------------------------------------------------------
				w5 AS	(
					SELECT
						w2.gid,
						sum(w2.value) AS value
					FROM
						w2
					GROUP
						BY w2.gid
					),
				-------------------------------------------------------------------
				w6 AS	(
					SELECT
						w4.gid,
						sum(w4.value) AS value
					FROM
						w4
					GROUP
						BY w4.gid					
					),
				-------------------------------------------------------------------
				w7a AS	(
					SELECT
					tad1.*,
					max(tad1.est_date) OVER (PARTITION BY tad1.gid, tad1.config_collection, tad1.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad1
					WHERE tad1.config_collection = $3
					AND tad1.config = $4
					AND (tad1.ext_version >= $14 AND tad1.ext_version <= $15)
					AND (tad1.gid >= $12 AND tad1.gid <= $13)
					),
				w7 AS	(
					SELECT w7a.gid AS gid_except FROM w7a WHERE w7a.est_date = w7a.max_est_date
					),					
				-----------------------------------------------
				w8 AS	(
					SELECT
						$3 AS config_collection,
						$4 AS config,
						w6.gid,
						((w6.value - w5.value))::double precision AS value,
						$5 AS ext_version,
						CASE WHEN w7.gid_except IS NULL THEN FALSE ELSE TRUE END AS gid_exists
					FROM
							w6
					INNER JOIN	w5 ON w6.gid = w5.gid
					LEFT  JOIN	w7 ON w6.gid = w7.gid_except
					)
				-----------------------------------------------
				SELECT
					w8.config_collection,
					w8.config,
					w8.gid,
					w8.value,
					w8.ext_version
				FROM
					w8 WHERE w8.gid_exists = FALSE
				';	
		
		END IF;
		---------------------------------------------------------------	
	ELSE
		RAISE EXCEPTION 'Chyba 45: fn_get_aux_data_app: Neznama hodnota _config_query = %!',_config_query;
	END CASE;
	-------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||''
	USING
		_ids4comb[1],				-- $1
		_ids4comb[2],				-- $2
		_config_collection,			-- $3
		_config,				-- $4
		_ext_version_current,			-- $5
		_config_collection_transform_comb[1],	-- $6
		_config_ids_4_complete_array,		-- $7
		_config_collection_4_complete_array,	-- $8
		_config_ids_4_categories_array,		-- $9
		_config_collection_4_categories_array,	-- $10
		_config_collection_transform_comb[2],	-- $11
		_gid_start,				-- $12
		_gid_end,				-- $13
		_ext_version_valid_from,		-- $14
		_ext_version_valid_until;		-- $15		
	-------------------------------------------------------------------------------------------
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


ALTER FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean,integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean,integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean,integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_data_app(integer,integer,boolean,integer,integer) IS
'Funkce vraci data pro insert do tabulky t_auxiliary_data.';
-- </function>
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- <function name="fn_get_aux_total_app" schema="@extschema@" src="functions/extschema/fn_get_aux_total_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- Function: @extschema@.fn_get_aux_total_app(integer, integer, integer)

-- DROP FUNCTION @extschema@.fn_get_aux_total_app(integer, integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_app(
    IN _config_id integer,
    IN _estimation_cell integer,
    IN _gid integer)
  RETURNS TABLE(estimation_cell integer, config integer, aux_total double precision, cell integer, ext_version integer) AS
$BODY$
DECLARE
	_config_collection			integer;
	_config_query				integer;
	_config_function			integer;
	_estimation_cell_exit			integer;
	_function_aux_total			text;
	_categories				character varying;
	_categories_sum				double precision;
	_res_aux_total				double precision;
	_estimation_cell_area			double precision;	
	_complete				character varying;
	_complete_sum				double precision;
	_aux_total				double precision;
	_estimation_cell_collection		integer;
	_estimation_cell_collection_lowest	integer;
	_estimation_cell_result			integer;
	_check_pocet				integer;
	_gid_result				integer;	
	_q					text;

	_ext_version_label_system		text;
	_ext_version_current			integer;

	-- NEW --
	_ext_version_valid_from			integer;
	_ext_version_valid_until		integer;
	_ext_version_valid_from_label		character varying;
	_ext_version_valid_until_label		character varying;
BEGIN
	--------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;	
	
	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- proces ziskani aktualni extenze nfiesta_gisdata
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_total_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;	
	--------------------------------------------------------------------------------------------
	-- proces ziskani potrebnych hodnot z konfigurace
	SELECT
		t.config_collection,
		t.config_query
	FROM
		@extschema@.t_config AS t
	WHERE
		id = _config_id
	INTO
		_config_collection,
		_config_query;
	--------------------------------------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_total_app: Argument _config_collection nesmi byt NULL!';
	END IF;	

	IF _config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_total_app: Argument _config_query nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	IF _config_query = 500
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_total_app: Vypocet nelze provest pro config_query = %!',_config_query;
	END IF;
	--------------------------------------------------------------------------------------------
	IF _gid IS NOT NULL AND _config_query = ANY(array[200,300,400])
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_aux_total_app: Vstupni argument _gid musi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- proces ziskani potrebnych hodnot z kolekce
	SELECT tcc.config_function FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = _config_collection
	INTO _config_function;
	--------------------------------------------------------------------------------------------
	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_total_app: Argument _config_function nesmi byt NULL!';
	END IF;	
	--------------------------------------------------------------------------------------------
	-- NEW --
	IF NOT(_config_function = ANY(array[100,200,300,400]))
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_get_aux_total_app: Interni promenna _config_function = % musi byt hodnota 100, 200, 300 nebo 400!',_config_function;
	END IF;

	-- zjisteni ext_version_valid_from a ext_version_valid_until
	SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
	FROM @extschema@.cm_ext_config_function AS cmcf
	WHERE cmcf.active = TRUE
	AND cmcf.config_function = _config_function
	INTO
		_ext_version_valid_from,
		_ext_version_valid_until;

	IF (_ext_version_valid_from IS NULL OR _ext_version_valid_until IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_get_aux_total_app: Pro interni promennou _config_function = % nenalezeno ext_version_valid_from nebo ext_version_valid_until v tabulce cm_ext_config_function!',_config_function;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_from  INTO _ext_version_valid_from_label;
	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_until INTO _ext_version_valid_until_label;
	--------------------------------------------------------------------------------------------
	-- kontrola zdali pro zadanou konfiguraci a zadanou _estimation_cell
	-- uz je ci neni hodnota aux_total vypocitana a jestli neni nahodou duplicitni
	-- => toto by mel hlidat bud trigger nebo check-constraint v tabulce t_aux_total
	-- => nebo je implementovano jiz ve funkci fn_get_gids4aux_total
	--------------------------------------------------------------------------------------------	
	--------------------------------------------------------------------------------------------
	-- rozhodovaci proces co funkce fn_get_aux_total_app bude delat:
	CASE
		WHEN (_config_query = 100) -- zakladni with [vypocet z GIS vrstvy]
		THEN
			-- _gid zde nesmi byt hodnota 0 => to je pro identifikaci pro sumarizaci
			-- _calculated zde muze byt jen FALSE => JIRKOVA aplikace tuto funkci bude poustet jen na gidy, 
			-- ktere nejsou doposud vypocitany, nebo pokud se jedna o prepocet, a provede insert do t_aux_total
			-- pokud se    jedna o gid zakladu, pak do vystupu musi jit estimation_cell zakladu
			-- pokud se ne-jedna o gid zakladu, pak do vystupu pujde estimation_cell co je zde na vstupu

			IF _gid > 0
			THEN
				-- zjisteni estimation_cell do vystupu teto funkce
				SELECT fac.estimation_cell FROM @extschema@.f_a_cell AS fac WHERE fac.gid = _gid
				INTO _estimation_cell_exit;

				IF _estimation_cell_exit IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 12: fn_get_aux_total_app: Pro gid = % nenalezeno estimation_cell v tabulce f_a_cell!',_gid;
				END IF;
				
				-- povolen vypocet aux_total hodnoty pro vstupni gid [zakladni protinani]
				CASE
					WHEN (_config_function = 100)	-- vector
					THEN
						_function_aux_total := 'fn_get_aux_total_vector_app';

					WHEN (_config_function = 200)	-- raster
					THEN
						_function_aux_total := 'fn_get_aux_total_raster_app';

					WHEN (_config_function = 300)	-- vector_comb
					THEN
						_function_aux_total := 'fn_get_aux_total_vector_comb_app';

					WHEN (_config_function = 400)	-- raster_comb
					THEN
						_function_aux_total := 'fn_get_aux_total_raster_comb_app';
					ELSE
						RAISE EXCEPTION 'Chyba 13: fn_get_aux_total_app: Neznama hodnota parametru _config_function [%].',_config_function;
				END CASE;
			ELSE
				RAISE EXCEPTION 'Chyba 14: fn_get_aux_total_app: Nepovolena kombinace vstupnich argumentu [_config_id = %, _estimation_cell = %, _gid = %] pro vypocet aux_total hodnoty pro config_query 100!',_config_id,_estimation_cell,_gid;
			END IF;

		WHEN (_config_query = ANY(array[200,300,400]))
		THEN
			-- 200 [soucet existujicich kategorii]
			-- 300 [doplnek do rozlohy vypocetni bunky]
			-- 400 [doplnek do rozlohy existujici kategorie]
			
			_function_aux_total := NULL::text;

			IF _gid IS NULL
			THEN
				-- povolen vypocet aux_total hodnoty pro soucet/doplnky

				_estimation_cell_exit := _estimation_cell;

				--raise notice '_estimation_cell_exit:%',_estimation_cell_exit;

				-- 1. zde se nejpreve z konfigurace zjisti _categories
				SELECT tc.categories FROM @extschema@.t_config AS tc WHERE tc.id = _config_id
				INTO _categories;

				--raise notice '_categories:%',_categories;

				-- 2. potom se pro _categories provede kontrola zda v tabulce t_aux_total vubec _categories existuji
				-- => toto uz je implementovano ve funkci fn_get_gids4aux_total

				-- 3. zde se provede suma za _categories pro danou _estimation_cell, ale tim ze povoluje interval VERZI
				--    musi se suma provest pro zaznam s max ext_version a nejspise i s max est_date
				EXECUTE '
				WITH
				w1 AS	(
					SELECT
					t.*,
					max(t.est_date) OVER (PARTITION BY t.estimation_cell, t.config) as max_est_date
					FROM @extschema@.t_aux_total AS t
					WHERE t.estimation_cell = $1
					AND t.config IN (SELECT unnest(array['||_categories||']))
					AND t.ext_version >= $2
					AND t.ext_version <= $3
					),
				w2 AS	(
					SELECT w1.* FROM w1 WHERE w1.est_date = w1.max_est_date			
					)
				SELECT sum(w2.aux_total) FROM w2
				'
				USING _estimation_cell, _ext_version_valid_from, _ext_version_valid_until
				INTO _categories_sum;

				--raise notice '_categories_sum: %',_categories_sum;

				-- 4. proces ziskani _res_aux_total pro jednotlive varianty _config_query 200,300,400
				CASE
				WHEN _config_query = 200	-- soucet
				THEN
					_res_aux_total := _categories_sum;
					
				WHEN _config_query = 300	-- doplnek do rozlohy vypocetni bunky [doplnek pro danou estimation_cell]
				THEN
					-- zjisteni plochy pro danou estimation_cell z tabulky f_a_cell
					SELECT sum(ST_Area(fac.geom))/10000.0 FROM @extschema@.f_a_cell AS fac
					WHERE fac.estimation_cell = _estimation_cell
					INTO _estimation_cell_area;

					IF _estimation_cell_area IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 15: fn_get_aux_total_app: Pro zadanou _estimation_cell = % nezjistena plocha z tabulky f_a_cell!',_estimation_cell;
					END IF;

					_res_aux_total := _estimation_cell_area - _categories_sum;

				WHEN _config_query = 400
				THEN
					-- 1. zde se nejpreve z konfigurace zjisti complete
					SELECT tc.complete FROM @extschema@.t_config AS tc WHERE tc.id = _config_id
					INTO _complete;

					-- 2. potom se pro _complete provede kontrola zda v tabulce t_aux_total vubec existuje
					-- => toto uz je implementovano ve funkci fn_get_gids4aux_total

					-- 3. zjisteni sumy za _complete;
					EXECUTE '
					WITH
					w1 AS	(
						SELECT
						t.*,
						max(t.est_date) OVER (PARTITION BY t.estimation_cell, t.config) as max_est_date
						FROM @extschema@.t_aux_total AS t
						WHERE t.estimation_cell = $1
						AND t.config IN (SELECT unnest(array['||_complete||']))
						AND t.ext_version >= $2
						AND t.ext_version <= $3
						),
					w2 AS	(
						SELECT w1.* FROM w1 WHERE w1.est_date = w1.max_est_date				
						)
					SELECT sum(w2.aux_total) FROM w2
					'
					USING _estimation_cell, _ext_version_valid_from, _ext_version_valid_until
					INTO _complete_sum;

					_res_aux_total := _complete_sum - _categories_sum;	
				ELSE
					RAISE EXCEPTION 'Chyba 16: fn_get_aux_total_app: Pro _config_query = % doposud v tele funkce neprovedena implemetace procesu ziskani hodnoty _res_aux_total!',_config_query;
				END CASE;

			ELSE
				RAISE EXCEPTION 'Chyba 17: fn_get_aux_total_app: Nepovolena kombinace vstupnich argumentu [_config_id = %, _estimation_cell = %, _gid = %, _calculated = %] pro vypocet aux_total hodnoty pro config_query 200,300 nebo 400!',_config_id,_estimation_cell,_gid,_calculated;
			END IF;
		ELSE
			RAISE EXCEPTION 'Chyba 18: fn_get_aux_total_app: Neznama hodnota parametru _config_query [%].',_config_query;
	END CASE;
	--------------------------------------------------------------------------------------------
	IF _function_aux_total IS NOT NULL
	THEN
		EXECUTE 'SELECT @extschema@.'||_function_aux_total||'($1,$2)'
		USING _config_id, _gid
		INTO _aux_total;
	ELSE
		_aux_total := _res_aux_total;
	END IF;
	--------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- zjisteni do jake collection patri zadana vstupni estimation_cell
	SELECT cec.estimation_cell_collection FROM @extschema@.c_estimation_cell  AS cec WHERE cec.id = _estimation_cell
	INTO _estimation_cell_collection;
	--------------------------------------------------------------------------------------------
	-- zjisteni estimation_cell_collection_lowest pro zadanou _estimation_cell_collection
	SELECT cecc.estimation_cell_collection_lowest FROM @extschema@.cm_estimation_cell_collection AS cecc
	WHERE cecc.estimation_cell_collection = _estimation_cell_collection
	INTO _estimation_cell_collection_lowest;
	--------------------------------------------------------------------------------------------
	IF _config_query = 100
	THEN
		IF _estimation_cell_collection != _estimation_cell_collection_lowest	-- VSTUPNI estimation_cell NENI zaklad [pozn. gidy pro protinani jsou vzdy zaklad]
		THEN
			-- ale pokud funkce fn_get_gids4aux_total_app vratila gid pro danou vstupni estimation_cell => tzn. ze zakladni gid jeste neni v t_aux_total
			-- a musel se jiz drive v kodu pro gid dohledat estimation_cell
			-- napr. chci TREBIC a zakladni gidy pro tuto estimation_cell nejsou jeste ulozeny v t_aux_total
			-- vystup za NUTS1-4 a rajonizace => pujde pres step 2 a sumarizaci
			-- vystup za OPLO => pujde pres step 2 a sumarizaci

			IF _gid > 0
			THEN		
				_estimation_cell_result := _estimation_cell_exit;
			ELSE
				RAISE EXCEPTION 'Chyba 19: fn_get_aux_total_app: Jde-li o config_query = 100, pak gid nesmi byt nikdy NULL nebo hodnota 0!';
			END IF;
		ELSE
			-- VSTUPNI estimation_cell JE zakladd
			
			-- zjisteni poctu geometrii, ktere tvori vstupni estimation_cell
			SELECT count(fac.gid) FROM @extschema@.f_a_cell AS fac WHERE fac.estimation_cell = _estimation_cell
			INTO _check_pocet;

			IF _check_pocet IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 20: fn_get_aux_total_app: Pro estimation_cell = % nenalezeny geometrie v tabulce f_a_cell!',_estimation_cell;
			END IF;

			IF _check_pocet = 1 -- vstupni estimation_cell uz neni nijak geometricky rozdrobena v f_a_cell
			THEN
				_estimation_cell_result := _estimation_cell_exit;
			ELSE
				_estimation_cell_result := NULL::integer;
			END IF;
		END IF;

		_gid_result := _gid;
	ELSE
		IF _config_query = ANY(array[200,300,400])
		THEN
			_estimation_cell_result := _estimation_cell_exit;
			_gid_result := NULL::integer;
		ELSE
			RAISE EXCEPTION 'Chyba 21: fn_get_aux_total_app: Neznama hodnota config_query = %!',_config_query;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	_q := 'SELECT $1,$2,$3,$4,$5';
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||'' USING _estimation_cell_result,_config_id,_aux_total,_gid_result,_ext_version_current;
	--------------------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total pro zadanou konfiguraci (config_id) a celu (estimation_cell) nebo gid.';
-- </function>
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- <function name="fn_get_aux_total4estimation_cell_app" schema="@extschema@" src="functions/extschema/fn_get_aux_total4estimation_cell_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP function @extschema@.fn_get_aux_total4estimation_cell_app (integer,integer,integer,boolean)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app
	(
	_config_id			integer,
	_estimation_cell		integer,
	_gid				integer,
	_recount			boolean DEFAULT FALSE
	)
RETURNS TABLE
	(
        estimation_cell			integer,
        config				integer, 
        aux_total			double precision,
        cell				integer,
        ext_version			integer
        )
AS
$BODY$
DECLARE
	_estimation_cell_collection		integer;
	_estimation_cell_collection_lowest	integer;
	_check_estimation_cell			boolean;
	_check_pocet				integer;
	_gids4estimation_cell			integer[];
	_gids4estimation_cell_lowest		integer[];
	_gids_in_t_aux_total_check		integer;
	_aux_total				double precision;
	_q					text;

	_ext_version_label_system		text;
	_ext_version_current			integer;

	-- NEW --
	_config_function				integer;
	_ext_version_valid_from				integer;
	_ext_version_valid_until			integer;
	_ext_version_valid_from_label			character varying;
	_ext_version_valid_until_label			character varying;		
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total4estimation_cell_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total4estimation_cell_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total4estimation_cell_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;

	IF _gid != 0
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_total4estimation_cell_app: Vstupni argument _gid musi byt hodnota 0!';
	END IF;

	IF _recount IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_total4estimation_cell_app: Vstupni argument _recount nesmi byt NULL!';
	END IF;	
	-------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- proces ziskani aktualni systemove extenze nfiesta_gisdata
	
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_total4estimation_cell_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_total4estimation_cell_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;		
	-------------------------------------------------------------------------------------------
	-- NEW --
	-- zjisteni config_function pro zjistene _config_collection potazmo pro vstupni _config_id
	SELECT tcc.config_function FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = (SELECT config_collection FROM @extschema@.t_config WHERE id = _config_id)
	INTO _config_function;

	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_aux_total4estimation_cell_app: Pro vstupni argument _config_id = % nenalezeno config_function v tabulce t_config_collection!',_config_id;
	END IF;

	IF NOT(_config_function = ANY(array[100,200,300,400]))
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_total4estimation_cell_app: Interni promenna _config_function = % musi byt hodnota 100, 200, 300 nebo 400!',_config_function;
	END IF;

	-- zjisteni ext_version_valid_from a ext_version_valid_until
	SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
	FROM @extschema@.cm_ext_config_function AS cmcf
	WHERE cmcf.active = TRUE
	AND cmcf.config_function = _config_function
	INTO
		_ext_version_valid_from,
		_ext_version_valid_until;

	IF (_ext_version_valid_from IS NULL OR _ext_version_valid_until IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_get_gids4aux_total_app: Pro interni promennou _config_function = % nenalezeno ext_version_valid_from nebo ext_version_valid_until v tabulce cm_ext_config_function!',_config_function;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_from  INTO _ext_version_valid_from_label;
	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_until INTO _ext_version_valid_until_label;
	-------------------------------------------------------------------------------------------
	-- kontrola, ze estimation_cell:
	-- 1. nesmi byt zakladni stavebni jednotka [ZSJ] => vyjimka plati jen pro viz. bod 2
	-- 2. muze to byt ZSJ ale pocet geometrii, ktery tvori onu ZSJ musi byt vice nez 1
	--------------------------------------------------------------------------------------------
	-- zjisteni do jake estimation_cell_collection patri zadana vstupni estimation_cell
	SELECT cec.estimation_cell_collection FROM @extschema@.c_estimation_cell AS cec WHERE cec.id = _estimation_cell
	INTO _estimation_cell_collection;
	--------------------------------------------------------------------------------------------
	-- zjisteni estimation_cell_collection_lowest pro zjistenou _estimation_cell_collection
	SELECT cecc.estimation_cell_collection_lowest FROM @extschema@.cm_estimation_cell_collection AS cecc
	WHERE cecc.estimation_cell_collection = _estimation_cell_collection
	INTO _estimation_cell_collection_lowest;
	--------------------------------------------------------------------------------------------
	IF _estimation_cell_collection != _estimation_cell_collection_lowest
	THEN
		-- vstupni estimation_cell neni ZSJ => splnena 1. podminka kontroly
		_check_estimation_cell := TRUE;
	ELSE
		-- vstupni estimation_cell je ZSJ
		
		-- zjisteni poctu geometrii, ktere tvori vstupni estimation_cell, ktera je ZSJ
		SELECT count(fac.gid) FROM @extschema@.f_a_cell AS fac
		WHERE fac.estimation_cell = _estimation_cell
		INTO _check_pocet;

		IF _check_pocet IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 11: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = % nenalezena zadna geometrie v tabulce f_a_cell!',_estimation_cell;
		END IF;

		IF _check_pocet = 1 -- vstupni estimation_cell (ZSJ) uz neni nijak geometricky rozdrobena v f_a_cell na mensi casti
		THEN
			_check_estimation_cell := FALSE;
		ELSE
			_check_estimation_cell := TRUE;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	IF _check_estimation_cell = FALSE
	THEN
		RAISE EXCEPTION 'Chyba 12: fn_get_aux_total4estimation_cell_app: Zadanou estimation_cell = % neni mozno sumarizovat. Jedna se totiz o nejnizsi geografickou uroven, ktera neni geometricky rozdrobena na mensi casti!',_estimation_cell;
	END IF;
	--------------------------------------------------------------------------------------------
	-- kontrola zda jiz pro zadanou estimation_cell, config_id a verzi spadajici do povoleneho
	-- intervalu VERZI jiz existuje hodnota aux_total v tabulce t_aux_total
	-- kontrola se provadi pri VYPOCTU i PREPOCTU [puvodne zde bylo jen pri VYPOCTU]
	-- jelikoz je u vstupnich argumentu ponechan _recount, pak IF jsem upravil nasledovne
	IF _recount = ANY(array[TRUE,FALSE])
	THEN
		IF	(
			SELECT count(tat.*) > 0
			FROM @extschema@.t_aux_total AS tat
			WHERE tat.config = _config_id
			AND tat.estimation_cell = _estimation_cell
			AND tat.ext_version = _ext_version_valid_from
			AND tat.ext_version = _ext_version_valid_until
			)
		THEN
			RAISE EXCEPTION 'Chyba 13: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = %, config_id = % a verzi od % do % jiz existuje hodnota aux_total v tabulce t_aux_total. Sumarizace neni mozna!',_estimation_cell,_config_id,_ext_version_valid_from_label,_ext_version_valid_until_label;
		END IF;
	END IF;
	-------------------------------------------------------------------------------------------
	-- zjisteni gidu (ZSJ) z tabulky f_a_cell, ktere tvori zadanou _estimation_cell
	SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
	WHERE fac.estimation_cell = _estimation_cell
	INTO _gids4estimation_cell;

	IF _gids4estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 14: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell;
	END IF;

	-- volani funkce fn_get_lowest_gids [funkce si saha do mapovaci tabulky cm_f_a_cell]
	-- funkce by mela vratit seznam gidu nejnizsi urovne (ZSJ), ktere tvori zadany seznam vyssich gidu
	_gids4estimation_cell_lowest := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell);
	-------------------------------------------------------------------------------------------
	-- pro jistotu provedeni DISTINCTU pro gidy
	SELECT array_agg(t2.gids ORDER BY t2.gids) FROM
	(SELECT DISTINCT t1.gids FROM (SELECT unnest(_gids4estimation_cell_lowest) AS gids) as t1) AS t2
	INTO _gids4estimation_cell_lowest;
	-------------------------------------------------------------------------------------------
	-- kontrola zda v tabulce t_aux_total jsou pro sumarizaci vsechny gidy nejnizsi urovne
	-- pro config_id a pro verzi spadajici do povoleneho intervalu VERZI
	WITH
	w1 AS	(SELECT unnest(_gids4estimation_cell_lowest) AS gids),
	w2 AS	(
		SELECT
			DISTINCT tat.config, tat.cell
		FROM
			@extschema@.t_aux_total AS tat
		WHERE
			tat.config = _config_id
		AND
			tat.cell IN (SELECT gids FROM w1)
		AND
			tat.ext_version >= _ext_version_valid_from
		AND
			tat.ext_version <= _ext_version_valid_until
		),
	w3 AS	(
		SELECT
			w1.gids,
			w2.cell
		FROM
			w1 LEFT JOIN w2
		ON
			w1.gids = w2.cell
		)
	SELECT
		count(*) FROM w3 WHERE w3.cell IS NULL
	INTO
		_gids_in_t_aux_total_check;
	-------------------------------------------------------------------------------------------
	IF _gids_in_t_aux_total_check > 0
	THEN
		RAISE EXCEPTION 'Chyba 15: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = %, config_id = % a verzi od % do % neni v tabulce t_aux_total kompletni seznam hodnot aux_total pro sumarizaci!',_estimation_cell,_config_id,_ext_version_valid_from_label,_ext_version_valid_until_label;
	END IF;
	-------------------------------------------------------------------------------------------
	-- vypocet hodnoty aux_total pro danou estimation_cell
	WITH
	w1 AS	(SELECT unnest(_gids4estimation_cell_lowest) AS gids),
	w2 AS	(
		SELECT
			tat.*,
			max(tat.est_date) OVER (PARTITION BY tat.cell, tat.config) as max_est_date
		FROM
			@extschema@.t_aux_total AS tat
		WHERE
			tat.config = _config_id
		AND
			tat.cell IN (SELECT gids FROM w1)
		AND
			tat.ext_version >= _ext_version_valid_from
		AND
			tat.ext_version <= _ext_version_valid_until			
		),
	w3 AS	(
		SELECT w2.* FROM w2 WHERE w2.est_date = w2.max_est_date		
		)
	SELECT
		sum(w3.aux_total) AS aux_total
	FROM
		w3
	INTO
		_aux_total;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	_q := 'SELECT $1,$2,$3,NULL::integer,$4';
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||'' USING _estimation_cell,_config_id,_aux_total,_ext_version_current;
	--------------------------------------------------------------------------------------------
END ;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) IS
'Funkce vraci hodnotu aux_total pro zadanou estimation_cell a config_id.';
-- </function>
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- <function name="fn_get_configs4aux_data_app" schema="@extschema@" src="functions/extschema/fn_get_configs4aux_data_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_configs4aux_data_app(integer, integer, integer, boolean)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_configs4aux_data_app
(
	_config_collection	integer,
	_gui_version		integer,
	_interval_points	integer DEFAULT 0,
	_recount		boolean DEFAULT FALSE
)
RETURNS TABLE
(
	step			integer,
	config_collection	integer,
	config			integer,
	intersects		boolean,
	step4interval		integer,
	gid_start		integer,
	gid_end			integer	
)
AS
$BODY$
DECLARE
	_ref_id_layer_points		integer;
	_ref_id_total			integer;
	_ref_id_total_categories	integer[];
	_ref_id_total_config_query	integer[];
	_config_function		integer;
	_aggregated			boolean;
	_ext_version_current		integer;
	_ext_version_current_label	text;
	_step4interval_res		integer[];
	_gid_start_res			integer[];
	_gid_end_res			integer[];
	_interval_points_res		integer[];
	_intersects			boolean;
	_config_id_reference		integer;
	_config_id_base			integer;
	_config_collection_base		integer;
	_check				integer;
	_check_exists_ii		boolean;
	_res_step_ii			integer[];
	_res_config_collection_ii	integer[];
	_res_config_ii			integer[];
	_res_intersects_ii		boolean[];
	_res_config_query_ii		integer[];
	_res_step4interval_ii		integer[];
	_res_check_exists_ii		boolean[];
	_res_step			integer[];
	_res_config_collection		integer[];
	_res_config			integer[];
	_res_intersects			boolean[];
	_res_config_query		integer[];
	_res_step4interval		integer[];
	_res_check_exists		boolean[];	
	_q				text;
	_check_1			integer;
	_check_2			integer;
	_check_3			integer;

	-- NEW --
	_ext_version_valid_from				integer;
	_ext_version_valid_until			integer;
	_ext_version_valid_from_label			character varying;
	_ext_version_valid_until_label			character varying;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Error 01: fn_get_configs4aux_data_app: The input argument _config_collection must not by NULL!';
	END IF;

	IF _gui_version IS NULL
	THEN
		RAISE EXCEPTION 'Error 02: fn_get_configs4aux_data_app: The input argument _gui_version must not by NULL!';
	END IF;

	IF _recount IS NULL
	THEN
		RAISE EXCEPTION 'Error 03: fn_get_configs4aux_data_app: The input argument _recount must not by NULL!';
	END IF;

	IF _interval_points IS NULL OR _interval_points < 0
	THEN
		RAISE EXCEPTION 'Error 04: fn_get_configs4aux_data_app: The input argument _interval_points must not by NULL or a negative value!';
	END IF;
	-------------------------------------------------------------------------------------------
	-- finding reference IDs for the point layer and total configuration
	SELECT
		tcc.ref_id_layer_points,
		tcc.ref_id_total
	FROM
		@extschema@.t_config_collection AS tcc
	WHERE
		tcc.id = _config_collection
	INTO
		_ref_id_layer_points,
		_ref_id_total;
	-------------------------------------------------------------------------------------------
	IF _ref_id_layer_points IS NULL
	THEN
		RAISE EXCEPTION 'Error 05: fn_get_configs4aux_data_app: For input argument _config_collection = % not be in configuration found a reference to point layer configuration!',_config_collection;
	END IF;
	-------------------------------------------------------------------------------------------
	IF _ref_id_total IS NULL
	THEN
		RAISE EXCEPTION 'Error 06: fn_get_configs4aux_data_app: For input argument _config_collection = % not be in configuration found a reference to configuration of total!',_config_collection;
	END IF;
	-------------------------------------------------------------------------------------------
	-- found CATEGORY and config_query for TOTAL REFERENCE
	SELECT
		array_agg(tc.id ORDER BY tc.id) AS id,
		array_agg(tc.config_query ORDER BY tc.id) AS config_query
	FROM
		@extschema@.t_config AS tc
	WHERE
		tc.config_collection = _ref_id_total
	INTO
		_ref_id_total_categories,
		_ref_id_total_config_query;
	-------------------------------------------------------------------------------------------
	IF _ref_id_total_categories IS NULL
	THEN
		RAISE EXCEPTION 'Error 07: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] nenalezena v konfiguraci [v tabulce t_config] zadna kategorie teto konfigurace!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	IF _ref_id_total_config_query IS NULL
	THEN
		RAISE EXCEPTION 'Error 08: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] nenalezena v konfiguraci [v tabulce t_config] zadna hodnota config_query u kategorii teto konfigurace!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	-- detected config_function and aggregated for _ref_id_total
	SELECT
		config_function,
		aggregated
	FROM
		@extschema@.t_config_collection
	WHERE
		id = _ref_id_total
	INTO
		_config_function,
		_aggregated;
	-------------------------------------------------------------------------------------------
	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Error 09: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] nenalezena hodnota _config_function u teto konfigurace!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	IF _aggregated IS NULL
	THEN
		RAISE EXCEPTION 'Error 10: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] nenalezena hodnota _aggregated u teto konfigurace!',_ref_id_total;
	END IF;
	-------------------------------------------------------------------------------------------
	-- find the latest version of the extension for the input version of the GUI application
	select max(ext_version) FROM @extschema@.cm_ext_gui_version
	WHERE gui_version = _gui_version
	INTO _ext_version_current;

	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Error 11: fn_get_configs4aux_data_app: Pro vstupni argument _gui_version = % nenalezena verze extenze nfiesta_gisdata v mapovaci tabulce cm_ext_gui_version!',_gui_version;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_current
	INTO _ext_version_current_label;

	IF _ext_version_current_label IS NULL
	THEN
		RAISE EXCEPTION 'Error 12: fn_get_configs4aux_data_app: Pro argument _ext_version_current = % nenalezena verze extenze v ciselniku c_ext_version!',_ext_version_current;
	END IF;
	-------------------------------------------------------------------------------------------
	-- NEW --
	IF NOT(_config_function = ANY(array[100,200,300,400]))
	THEN
		RAISE EXCEPTION 'Error 13: fn_get_configs4aux_data_app: Interni promenna _config_function = % musi byt hodnota 100, 200, 300 nebo 400!',_config_function;
	END IF;

	IF _config_function = ANY(array[100,200,400])
	THEN
		IF _config_function = 400 THEN _config_function := 200; ELSE _config_function := _config_function; END IF;

		-- zjisteni ext_version_valid_from a ext_version_valid_until
		SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
		FROM @extschema@.cm_ext_config_function AS cmcf
		WHERE cmcf.active = TRUE
		AND cmcf.config_function = _config_function
		INTO
			_ext_version_valid_from,
			_ext_version_valid_until;		
	ELSE
		-- varianta VxR => _config_function = 300

		WITH
		w1 AS	(
			SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
			FROM @extschema@.cm_ext_config_function AS cmcf
			WHERE cmcf.active = TRUE
			AND cmcf.config_function = 100
			),
		w2 AS	(
			SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
			FROM @extschema@.cm_ext_config_function AS cmcf
			WHERE cmcf.active = TRUE
			AND cmcf.config_function = 200
			),
		w3 AS	(
			SELECT * FROM w1 UNION ALL
			SELECT * FROM w2
			)
		SELECT
			max(ext_version_valid_from),
			max(ext_version_valid_until)
		FROM
			w3
		INTO
			_ext_version_valid_from,
			_ext_version_valid_until;
	END IF;	
	
	IF (_ext_version_valid_from IS NULL OR _ext_version_valid_until IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 14: fn_get_configs4aux_data_app: Pro interni promennou _config_function = % nenalezeno ext_version_valid_from nebo ext_version_valid_until v tabulce cm_ext_config_function!',_config_function;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_from  INTO _ext_version_valid_from_label;
	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_until INTO _ext_version_valid_until_label;
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	-- proces naplneni promennych pro VLAKNA
	-------------------------------------------------------------------------------------------
	-- volani funkce pro ziskani casti pro VLAKNA [v pripade, ze uzivatel nepozaduje vypocet
	-- na vice vlaken, vstupni promenna _interval_points je hodnota 0, pak funkce
	-- fn_get_interval_points_app vrati jeden radek, kde gid_start_res je jedno prvkove pole
	-- a obsahuje min(gid) z bodove vrstvy, u end je to analogicky stejne a jde pak o max]
	-- poznamka: ve funkci je kontrola, ze hodnota total_points ulozena v konfiguraci odpovida realite,
	-- pokud tomu tak je, pak v pripade, ze je _interval_points hodnota 0, funkce vrati hodnotu
	-- _interval_points rovnu hodnote _total_points pro vstupni _ref_id_layer_points
	WITH
	w AS	(
		SELECT * FROM @extschema@.fn_get_interval_points_app(_ref_id_layer_points,_interval_points)
		)
	SELECT
		array_agg(w.step4interval ORDER BY w.step4interval) AS step4interval_res,
		array_agg(w.gid_start ORDER BY w.step4interval) AS gid_start_res,
		array_agg(w.gid_end ORDER BY w.step4interval) AS gid_end_res,
		array_agg(w.interval_points ORDER BY w.step4interval) AS interval_points_res
	FROM
		w
	INTO
		_step4interval_res,
		_gid_start_res,
		_gid_end_res,
		_interval_points_res;
	-----------------------------------------
	IF _step4interval_res IS NULL
	THEN
		RAISE EXCEPTION 'Error 15: fn_get_configs4aux_data_app: Nenaplnena interni promenna _step4interval_res!';
	END IF;
	-----------------------------------------
	IF _gid_start_res IS NULL
	THEN
		RAISE EXCEPTION 'Error 16: fn_get_configs4aux_data_app: Nenaplnena interni promenna _gid_start_res!';
	END IF;
	-----------------------------------------
	IF _gid_end_res IS NULL
	THEN
		RAISE EXCEPTION 'Error 17: fn_get_configs4aux_data_app: Nenaplnena interni promenna _gid_end_res!';
	END IF;
	-----------------------------------------
	IF _interval_points_res IS NULL
	THEN
		RAISE EXCEPTION 'Error 18: fn_get_configs4aux_data_app: Nenaplnena interni promenna _interval_points_res!';
	END IF;	
	-----------------------------------------
	IF	(
		SELECT count(t.*) > 0
		FROM (SELECT unnest(_step4interval_res) AS step4interval_res) AS t
		WHERE t.step4interval_res IS NULL
		)
	THEN
		RAISE EXCEPTION 'Error 19: fn_get_configs4aux_data_app: Interni promenna _step4interval_res obsahuje hodnotu NULL!';
	END IF;
	-----------------------------------------
	IF	(
		SELECT count(t.*) > 0
		FROM (SELECT unnest(_gid_start_res) AS gid_start_res) AS t
		WHERE t.gid_start_res IS NULL
		)
	THEN
		RAISE EXCEPTION 'Error 20: fn_get_configs4aux_data_app: Interni promenna _gid_start_res obsahuje hodnotu NULL!';
	END IF;
	-----------------------------------------
	IF	(
		SELECT count(t.*) > 0
		FROM (SELECT unnest(_gid_end_res) AS gid_end_res) AS t
		WHERE t.gid_end_res IS NULL
		)
	THEN
		RAISE EXCEPTION 'Error 21: fn_get_configs4aux_data_app: Interni promenna _gid_end_res obsahuje hodnotu NULL!';
	END IF;
	-----------------------------------------
	IF	(
		SELECT count(t.*) > 0
		FROM (SELECT unnest(_interval_points_res) AS interval_points_res) AS t
		WHERE t.interval_points_res IS NULL
		)
	THEN
		RAISE EXCEPTION 'Error 22: fn_get_configs4aux_data_app: Interni promenna _interval_points_res obsahuje hodnotu NULL!';
	END IF;	
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------


	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------
	-- CYKLUS po jednotlivych konfiguracnich KATEGORIICH
	FOR i IN 1..array_length(_ref_id_total_categories,1)
	LOOP
		-----------------------------------------------------------------------------------
		IF _ref_id_total_config_query[i] IS NULL
		THEN
			RAISE EXCEPTION 'Error 23: fn_get_configs4aux_data_app: Pro argument _ref_id_total = % [ID konfigurace uhrnu] neni u nektere jeji kategorie vyplnena hodnota config_query!',_ref_id_total;
		END IF;
		-----------------------------------------------------------------------------------
		-- proces naplneni promenne _intersects
		IF	(
			_config_function = ANY(array[100,200])	AND
			_ref_id_total_config_query[i] = 100 AND
			_aggregated = FALSE
			)
		THEN
			_intersects := TRUE;
		ELSE
			_intersects := FALSE;
		END IF;
		-----------------------------------------------------------------------------------
		-- proces zjisteni _config_id_base a _config_collection_base
		IF _ref_id_total_config_query[i] = 500 -- REFERENCE
		THEN
			-- config_id_base
			SELECT tc.categories::integer
			FROM @extschema@.t_config AS tc
			WHERE tc.id = _ref_id_total_categories[i]
			INTO _config_id_reference;

			IF _config_id_reference IS NULL
			THEN
				RAISE EXCEPTION 'Error 24: fn_get_configs4aux_data_app: U argumentu _ref_id_total = % [ID konfigurace uhrnu] je jeho i-ta kategorie [_ref_id_total_config_query[i] = %] config_query 500 [reference] a v konfiguraci [v tabulce t_config] nema ve sloupci categories nakonfigurovanou referenci!',_ref_id_total,_ref_id_total_config_query[i];
			END IF;

			_config_id_base := _config_id_reference;

			-- config_collection_base
			SELECT tcc.id FROM @extschema@.t_config_collection AS tcc
			WHERE tcc.ref_id_total = (SELECT tc.config_collection FROM @extschema@.t_config AS tc WHERE tc.id = _config_id_base)
			AND tcc.ref_id_layer_points = _ref_id_layer_points
			INTO _config_collection_base;
		ELSE
			_config_id_base := _ref_id_total_categories[i];
			_config_collection_base := _config_collection;
		END IF;
		-----------------------------------------------------------------------------------
		-- 1. KONTROLA EXISTENCE v DB
		-----------------------------------------------------------------------------------
		IF _recount = FALSE
		
		THEN	-- kontrola existence v db => vetev pro VYPOCET bodu

			-- CYKLUS po jednotlivych INTERVALECH
			FOR ii IN 1..array_length(_step4interval_res,1)
			LOOP
				-- => kontrola, ze dane _config_collection, dane _config a pro verzi danou povolenym intervalem VERZI
				-- JE nebo NENI v DB vypocitano [poznamka: verze u vsech bodu bude prakticky vzdy stejna !!!]
				WITH
				w1 AS	(
					SELECT DISTINCT tad.gid FROM @extschema@.t_auxiliary_data AS tad
					WHERE tad.config_collection = _config_collection_base
					AND tad.config = _config_id_base
					AND tad.ext_version >= _ext_version_valid_from
					AND tad.ext_version <= _ext_version_valid_until
					AND tad.gid >= _gid_start_res[ii]
					AND tad.gid <= _gid_end_res[ii]
					)
				SELECT count(*) FROM w1
				INTO _check;

				-- varianty:
				-- 1. body pro i-ty interval uz v DB    existuji a pocet bodu se    shoduje s promennou _interval_points => tento zaznam do vypoctu NEPUJDE
				-- 2. body pro i-ty interval uz v DB    existuji a pocet bodu se ne-shoduje s promennou _interval_points => tento zaznam do vypoctu   PUJDE
				-- 3. body pro i-ty interval    v DB ne-existuji => tento zaznam do vypoctu PUJDE

				IF _check > 0
				THEN
					IF _check = _interval_points_res[ii]
					THEN
						_check_exists_ii := TRUE; -- interval nepujde do vypisu
					ELSE
						_check_exists_ii := FALSE; -- interval pujde do vypisu
					END IF;
				ELSE
					IF _ref_id_total_config_query[i] = 500	-- REFERENCE
					THEN
						RAISE EXCEPTION 'Error 25: fn_get_configs4aux_data_app: U argumentu _ref_id_total = % [ID konfigurace uhrnu] je jeho kategorie _ref_id_total_config_query[i] = % reference, pro kterou ale neni doposud proveden vypocet kategorie z neagregovane konfigurace uhrhu!',_ref_id_total,_ref_id_total_config_query[i];
					ELSE
						_check_exists_ii := FALSE; -- interval pujde do vypisu
					END IF;
				END IF;

				-- naplneni promenych do RETURN QUERY v cyklu ii
				IF ii = 1
				THEN
					_res_step_ii := array[i];
					_res_config_collection_ii := array[_config_collection];
					_res_config_ii := array[_ref_id_total_categories[i]];
					_res_intersects_ii := array[_intersects];
					_res_config_query_ii := array[_ref_id_total_config_query[i]];
					_res_step4interval_ii := array[ii];
					_res_check_exists_ii := array[_check_exists_ii];
				ELSE
					_res_step_ii := _res_step_ii || array[i];
					_res_config_collection_ii := _res_config_collection_ii || array[_config_collection];
					_res_config_ii := _res_config_ii || array[_ref_id_total_categories[i]];
					_res_intersects_ii := _res_intersects_ii || array[_intersects];
					_res_config_query_ii := _res_config_query_ii || array[_ref_id_total_config_query[i]];
					_res_step4interval_ii := _res_step4interval_ii || array[ii];
					_res_check_exists_ii := _res_check_exists_ii || array[_check_exists_ii];
				END IF;				
			END LOOP;
		ELSE
			-- kontrola existence v db => vetev pro PREPOCET bodu

			-- 1. prepocet   JE MOZNY pokud v db existuji vsechny kategorie dane konfigurace [na intervalech a na verzi extenze NEZALEZI]

				-- => pocet bodu i-teho config v ii_tem intervalu musi odpovidat poctu bodu ii-teho intervalu
				-- => musi se brat gid pro max_ext_version a pro max_est_date
				
			-- 2. prepocet NENI MOZNY pokud v db existuji vsechny kategorie dane konfigurace u vsech intervalu [na verzi extenze ZALEZI, respektive verze spada do povoleneho intervalu VERZI]


			-- CYKLUS po jednotlivych INTERVALECH
			FOR ii IN 1..array_length(_step4interval_res,1)
			LOOP
				-- => kontrola add 1.
				WITH
				w1 AS	(
					SELECT
					tad.*,
					max(tad.est_date) OVER (PARTITION BY tad.gid, tad.config_collection, tad.config) as max_est_date
					FROM @extschema@.t_auxiliary_data AS tad
					WHERE tad.config_collection = _config_collection_base
					AND tad.config = _config_id_base
					AND tad.gid >= _gid_start_res[ii]
					AND tad.gid <= _gid_end_res[ii]
					),
				w2 AS	(
					SELECT w1.* FROM w1 WHERE w1.est_date = w1.max_est_date
					)
				SELECT count(*) FROM w2
				INTO _check_1;

				IF _check_1 > 0
				THEN
					IF _check_1 = _interval_points_res[ii]
						-- konrola add 1. povoluje prepocet
						-- => v intervalu jsou vsechny body [na verzi extenzi nezalezi]
						-- => dale to pokracuje kontrolou, kde uz na verzi extenze zalezi
					THEN
						-- => kontrola add 2.
						WITH
						w1 AS	(
							SELECT
							tad.*,
							max(tad.est_date) OVER (PARTITION BY tad.gid, tad.config_collection, tad.config) as max_est_date
							FROM @extschema@.t_auxiliary_data AS tad
							WHERE tad.config_collection = _config_collection_base
							AND tad.config = _config_id_base
							AND tad.ext_version >= _ext_version_valid_from -- na verzi extenze ZALEZI
							AND tad.ext_version <= _ext_version_valid_until
							AND tad.gid >= _gid_start_res[ii]
							AND tad.gid <= _gid_end_res[ii]
							),
						w2 AS	(
							SELECT w1.* FROM w1 WHERE w1.est_date = w1.max_est_date
							)							
						SELECT count(*) FROM w2
						INTO _check_2;

						IF _check_2 = _interval_points_res[ii]
						THEN
							_check_exists_ii := TRUE; -- interval obsahuje pozadovany pocet bodu ve verzi spadajici do povoleneho intevalu VERZI => interval neni kandidatem pro PREPOCET
						ELSE
							_check_exists_ii := FALSE; -- tento interval je kandidatem pro prepocet
						END IF;						
					ELSE
						RAISE EXCEPTION 'Error 26: fn_get_configs4aux_data_app: Prepocet NENI mozny [ID vstupni KONFIGURACE bodove vrstvy a uhrnu: _config_collection = %; ID konfigurace UHRNU: _config_collection = %; ID kategorie UHRNU: _config = %] => pro prepocet nejsou doposud vypocitany vsechny body intervalu urceneho podle gid <%;%>! Poznamka: na verzi extenze nezalezi.',_config_collection,_config_collection_base,_config_id_base,_gid_start_res[ii],_gid_end_res[ii]; -- Prepocet NENI mozny => pro prepocet nejsou vypocitany vsechny body v ii-tem intervalu ikdyz na verzi extenze nezalezi
					END IF;
				ELSE
					IF _ref_id_total_config_query[i] = 500	-- REFERENCE
					THEN
						RAISE EXCEPTION 'Error 27: fn_get_configs4aux_data_app: Prepocet NENI mozny [ID vstupni KONFIGURACE bodove vrstvy a uhrnu: _config_collection = %; ID konfigurace UHRNU: _config_collection = %; ID referencni kategorie UHRNU: _config = %] => pro prepocet nejsou doposud vypocitany zadne body intervalu urceneho podle gid <%;%>! Poznamka: na verzi extenze nezalezi.',_config_collection,_config_collection_base,_config_id_base,_gid_start_res[ii],_gid_end_res[ii]; -- Prepocet NENI MOZNY => pro prepocet nejsou vypocitany zadne body v ii-tem intervalu
					ELSE
						RAISE EXCEPTION 'Error 28: fn_get_configs4aux_data_app: Prepocet NENI mozny [ID vstupni KONFIGURACE bodove vrstvy a uhrnu: _config_collection = %; ID konfigurace UHRNU: _config_collection = %; ID kategorie UHRNU: _config = %] => pro prepocet nejsou doposud vypocitany zadne body intervalu urceneho podle gid <%;%>! Poznamka: na verzi extenze nezalezi.',_config_collection,_config_collection_base,_config_id_base,_gid_start_res[ii],_gid_end_res[ii]; -- Prepocet NENI MOZNY => pro prepocet nejsou vypocitany zadne body v ii-tem intervalu
					END IF;
				END IF;

				-- naplneni promenych do RETURN QUERY v cyklu ii
				IF ii = 1
				THEN
					_res_step_ii := array[i];
					_res_config_collection_ii := array[_config_collection];
					_res_config_ii := array[_ref_id_total_categories[i]];
					_res_intersects_ii := array[_intersects];
					_res_config_query_ii := array[_ref_id_total_config_query[i]];
					_res_step4interval_ii := array[ii];
					_res_check_exists_ii := array[_check_exists_ii];
				ELSE
					_res_step_ii := _res_step_ii || array[i];
					_res_config_collection_ii := _res_config_collection_ii || array[_config_collection];
					_res_config_ii := _res_config_ii || array[_ref_id_total_categories[i]];
					_res_intersects_ii := _res_intersects_ii || array[_intersects];
					_res_config_query_ii := _res_config_query_ii || array[_ref_id_total_config_query[i]];
					_res_step4interval_ii := _res_step4interval_ii || array[ii];
					_res_check_exists_ii := _res_check_exists_ii || array[_check_exists_ii];
				END IF;				
			END LOOP;
		END IF;
		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------

		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------
		-- naplneni promenych do RETURN QUERY v cyklu i
		IF i = 1
		THEN
			_res_step := _res_step_ii;
			_res_config_collection := _res_config_collection_ii;
			_res_config := _res_config_ii;
			_res_intersects := _res_intersects_ii;
			_res_config_query := _res_config_query_ii;
			_res_step4interval := _res_step4interval_ii;
			_res_check_exists := _res_check_exists_ii;
		ELSE
			_res_step := _res_step || _res_step_ii;
			_res_config_collection := _res_config_collection || _res_config_collection_ii;
			_res_config := _res_config || _res_config_ii;
			_res_intersects := _res_intersects || _res_intersects_ii;
			_res_config_query := _res_config_query || _res_config_query_ii;
			_res_step4interval := _res_step4interval || _res_step4interval_ii;
			_res_check_exists := _res_check_exists || _res_check_exists_ii;
		END IF;
									
	END LOOP;

	-------------------------------------------------------------------------------------------
	-- kontrola pro RECOUNT = TRUE
	IF _recount = TRUE
	THEN
		IF	(
			SELECT count(t.res_check_exists_recount) = 0
			FROM (SELECT unnest(_res_check_exists) AS res_check_exists_recount) AS t
			WHERE t.res_check_exists_recount = FALSE
			)
		THEN
			RAISE EXCEPTION 'Error 29: fn_get_configs4aux_data_app: Prepocet NENI mozny [ID vstupni KONFIGURACE bodove vrstvy a uhrnu: _config_collection = %; ID konfigurace UHRNU: _config_collection = %; ID kategorie UHRNU: _config = %] => vsechny body jsou ve verzi od verze % do %! Poznamka: na verzi extenze zalezi.',_config_collection,_config_collection_base,_config_id_base,_ext_version_valid_from_label,_ext_version_valid_until_label; -- Prepocet NENI MOZNY => vsechny body jsou v aktualni verzi
		END IF;
	END IF;
	-------------------------------------------------------------------------------------------
					
	-------------------------------------------------------------------------------------------
	-- sestaveni vysledneho _q pro RETURN QUERY
	-------------------------------------------------------------------------------------------
	_q :=	'
		WITH
		w1 AS	(
			SELECT
				unnest($1) AS step,
				unnest($2) AS config_collection,
				unnest($3) AS config,
				unnest($4) AS intersects,
				unnest($5) AS config_query,
				unnest($6) AS step4interval,
				unnest($7) AS check_exists
			),
		w2 AS	(
			SELECT
				unnest($8) AS step4interval_res,
				unnest($9) AS gid_start_res,
				unnest($10) AS gid_end_res
			),
		w3 AS	(
			SELECT
				w1.step,
				w1.config_collection,
				w1.config,
				w1.intersects,
				w1.step4interval
			FROM
				w1
			WHERE
				w1.config_query != 500 AND w1.check_exists = FALSE
			)
		SELECT
			w3.step,
			w3.config_collection,
			w3.config,
			w3.intersects,
			w3.step4interval,
			w2.gid_start_res,
			w2.gid_end_res
		FROM
			w3 INNER JOIN w2
		ON
			w3.step4interval = w2.step4interval_res
		ORDER
			BY w3.step, w3.config, w3.step4interval, w2.gid_start_res
		';
	-------------------------------------------------------------------------------------------		
	-------------------------------------------------------------------------------------------
	-- !!! pozor, protinaji se jen tyto konfigurace: (_config_query = 100 a _config_function = ANY(array[100,200]) AND _aggregated = false)
	-------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- kontrola ze _q vraci alespon nejakou konfiguraci pro protinani
	EXECUTE 'WITH w_configs AS ('||_q||') SELECT (count(*))::integer FROM w_configs'
	USING
		_res_step,			-- $1
		_res_config_collection,		-- $2
		_res_config,			-- $3
		_res_intersects,		-- $4
		_res_config_query,		-- $5
		_res_step4interval,		-- $6
		_res_check_exists,		-- $7
		_step4interval_res,		-- $8
		_gid_start_res,			-- $9
		_gid_end_res			-- $10
	INTO
		_check_3;
	-----------------------------------------------------------------------------------
	IF _recount = FALSE AND (_check_3 = 0 OR _check_3 IS NULL)
	THEN
		RAISE EXCEPTION 'Error 30: fn_get_configs4aux_data_app: Pro bodove protinani nenalezeny zadne konfigurace pro protinani. Vse jiz bylo vyprotinano!';
	END IF;
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||''
	USING
		_res_step,			-- $1
		_res_config_collection,		-- $2
		_res_config,			-- $3
		_res_intersects,		-- $4
		_res_config_query,		-- $5
		_res_step4interval,		-- $6
		_res_check_exists,		-- $7
		_step4interval_res,		-- $8
		_gid_start_res,			-- $9
		_gid_end_res;			-- $10
	--------------------------------------------------------------------------------------------
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_configs4aux_data_app(integer,integer,integer,boolean) IS
'Funkce vraci seznam konfiguraci pro funkci fn_get_aux_data_app.';
-- </function>
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- <function name="fn_get_gids4aux_total_app" schema="@extschema@" src="functions/extschema/fn_get_gids4aux_total_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_gids4aux_total_app
(
	_config_id			integer,
	_estimation_cell		integer[],
	_gui_version			integer,
	_recount			boolean DEFAULT FALSE
)
RETURNS TABLE
(
	step				integer,
	config_id			integer,
	estimation_cell			integer,
	gid				integer
) AS
$BODY$
DECLARE
	-- spolecne promenne:
	_config_query					integer;
	_config_collection				integer;
	_configs					integer[];
	
	_ext_version_current				integer;
	_q						text;
	_complete					character varying;
	_categories					character varying;
	_config_ids_text				text;
	_config_ids_length				integer;
	_config_ids					integer[];
	_config_ids4check				integer[];
	_check_avc4categories				integer;
	_config_id_reference				integer;
	_config_id_base					integer;
	_check						integer;
			
	-- promenne pro vypocet:
	_gids4estimation_cell_i				integer[];
	_gids4estimation_cell_pocet			integer;
	_pocet						integer[];
	_max_pocet					integer;
	_gids4estimation_cell				integer[];
	_pocet_gids4estimation_cell			integer;
	_doplnek					integer[];
	_res_estimation_cell				integer[];
	_res_gids					integer[];
	_res_estimation_cell_summarization		integer[];
	_summarization					boolean[];
	
	-- promenne pro prepocet:
	_gids4estimation_cell_i_prepocet		integer[];
	_gids4estimation_cell_pocet_prepocet		integer;
	_pocet_prepocet					integer[];
	_max_pocet_prepocet				integer;
	_gids4estimation_cell_prepocet			integer[];
	_pocet_gids4estimation_cell_prepocet		integer;
	_doplnek_prepocet				integer[];
	_res_estimation_cell_prepocet			integer[][];
	_res_gids_prepocet				integer[][];
	_res_estimation_cell_prepocet_simple		integer[];
	_res_gids_prepocet_simple			integer[];
	_res_estimation_cell_prepocet_simple_original	integer[];
	_ecc4prepocet					integer;
	_eccl4prepocet					integer;
	_gids4estimation_cell_i_prepocet_mezistupen	integer[];
	_gids4estimation_cell_prepocet_mezistupen	integer[];
	_gids_mezistupen_with_vstupni_estimation_cell	integer[];
	_estimation_cells_mezistupen_i			integer[];
	_res_estimation_cell_prepocet_4_summarization	integer[];
	_res_gids_prepocet_4_recount			integer[];
	_last_ext_version_from_table			character varying;
	_last_ext_version_from_db			character varying;
	_res_estimation_cell_prepocet_4_recount		integer[];
	_res_estimation_cell_prepocet_4_check		integer[];

	_ext_version_current_label			character varying;

	_config_query_ii				integer;
	_config_id_reference_ii				integer;
	_config_id_base_ii				integer;
	_check_ii_1					integer;
	_check_ii_2					integer;
	_check_ii_boolean_1				integer[];
	_check_ii_boolean_2				integer[];
	_check_ii					integer;
	_check_ii_boolean				integer[];

	_res_gids_count_count				integer;
	_res_estimation_cell_4_with_i			integer[];
	_res_estimation_cell_4_with			integer[][];

	-- NEW --
	_config_function				integer;
	_ext_version_valid_from				integer;
	_ext_version_valid_until			integer;
	_ext_version_valid_from_label			character varying;
	_ext_version_valid_until_label			character varying;	
BEGIN
	--------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_gids4aux_total_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_gids4aux_total_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;

	IF _gui_version IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_gids4aux_total_app: Vstupni argument _gui_version nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- kontrola vyplnenosti null zaznamu pro vsechny nejvyssi urovne estimation_cell_collection 
	-- v tabulce cm_f_a_cell u sloupce cell_sup
	PERFORM @extschema@.fn_check_cell_sup_app();
	--------------------------------------------------------------------------------------------
	-- zjisteni konfiguracnich promennych z tabulky t_config pro vstupni _config_id
	SELECT
		tc.config_query,
		tc.config_collection
	FROM
		@extschema@.t_config AS tc
	WHERE
		tc.id = _config_id
	INTO
		_config_query,
		_config_collection;
	--------------------------------------------------------------------------------------------
	IF _config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_gids4aux_total_app: Pro vstupni argument _config_id = % nenalezeno config_query v tabulce t_config!',_config_id;
	END IF;

	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_gids4aux_total_app: Pro vstupni argument _config_id = % nenalezeno config_collection v tabulce t_config!',_config_id;
	END IF;
	--------------------------------------------------------------------------------------------
	-- NEW --
	-- zjisteni config_function pro zjistene _config_collection
	SELECT tcc.config_function FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = _config_collection
	INTO _config_function;

	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_gids4aux_total_app: Pro interni promennou _config_collection = % nenalezeno config_function v tabulce t_config_collection!',_config_collection;
	END IF;

	IF NOT(_config_function = ANY(array[100,200,300,400]))
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_gids4aux_total_app: Interni promenna _config_function = % musi byt hodnota 100, 200, 300 nebo 400!',_config_function;
	END IF;

	-- zjisteni ext_version_valid_from a ext_version_valid_until
	SELECT cmcf.ext_version_valid_from, cmcf.ext_version_valid_until
	FROM @extschema@.cm_ext_config_function AS cmcf
	WHERE cmcf.active = TRUE
	AND cmcf.config_function = _config_function
	INTO
		_ext_version_valid_from,
		_ext_version_valid_until;

	IF (_ext_version_valid_from IS NULL OR _ext_version_valid_until IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_gids4aux_total_app: Pro interni promennou _config_function = % nenalezeno ext_version_valid_from nebo ext_version_valid_until v tabulce cm_ext_config_function!',_config_function;
	END IF;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_from  INTO _ext_version_valid_from_label;
	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_valid_until INTO _ext_version_valid_until_label;
	--------------------------------------------------------------------------------------------
	-- zjisteni vsech config_id pro _config_collection
	SELECT array_agg(tc.id ORDER BY tc.id) FROM @extschema@.t_config AS tc
	WHERE tc.config_collection = _config_collection
	INTO _configs;
	--------------------------------------------------------------------------------------------
	-- nalezeni nejaktualnejsi verze extenze pro vstupni verzi GUI aplikace
	select max(ext_version) FROM @extschema@.cm_ext_gui_version
	WHERE gui_version = _gui_version
	INTO _ext_version_current;

	SELECT label FROM @extschema@.c_ext_version WHERE id = _ext_version_current
	INTO _ext_version_current_label;
	--------------------------------------------------------------------------------------------
	-- NEW --
	-- kontrola, ze nejaktualnejsi verze extenze je v rozmezi aktivniho intervalu verzi pro danou variantu uhrnu, ktera je indentifikovana pomoci hodnoty config_function !!!
	IF NOT((_ext_version_current >= _ext_version_valid_from) AND (_ext_version_current <= _ext_version_valid_until))
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_gids4aux_total_app: Aktualni verze extenze _ext_version_current [%] = % neni v rozmezi aktivniho intervalu verzi pro danou variantu uhrnu !',_ext_version_current_label,_ext_version_current;
	END IF;	
	--------------------------------------------------------------------------------------------
	IF _recount = TRUE -- prepocet
	THEN	
		-----------------------------------------------------------------------------------
		-- KONTROLA EXISTENCE v DB => kontrola prepocet bud povoli nebo ne <= kontrola probiha na urovni CELE KONFIGURACE !!!

		-- 1. [na verzi extenze NE-ZALEZI]
		--	A: prepocet ANO => pro danou celu existuji v DB vsechny kategorie dane konfigurace
		--	B: prepocet NE	=> pro danou celu nektera kategorie z dane konfigurace v DB schazi => RESIT klasickym VYPOCTEM !!!

		-- 2. [na verzi extenze ZALEZI]
		--	A: prepocet NE	=> pro danou celu jsou hodnoty jednotlivych kategorie v extenzi, ktera odpovida (spada) do povoleneho intervalu VERZI => zde prepocet postrada smysl
		--	B: prepocet ANO	=> pro danou celu je hodnota u nektere kategorie v extenzi, ktera NE-SPADA do povoleneho intervalu VERZI, pozn. prepocet bude, respektive by mel probihat
		--			   jen u tech kategorii, jejichz extenze nespadaji do intervalu verzi

		-----------------------------------------------------------------------------------
		FOR i IN 1..array_length(_estimation_cell,1) -- cyklus pres cely
		LOOP
			FOR ii IN 1..array_length(_configs,1) -- cyklus pres jednotlive kategorie dane konfigurace
			LOOP
				SELECT tc.config_query FROM @extschema@.t_config AS tc
				WHERE tc.id = _configs[ii]
				INTO _config_query_ii;
						
				IF _config_query_ii = 500 -- REFERENCE
				THEN
					SELECT tc.categories::integer
					FROM @extschema@.t_config AS tc
					WHERE tc.id = _configs[ii]
					INTO _config_id_reference_ii;

					IF _config_id_reference_ii IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 10: fn_get_gids4aux_total_app: Kontrolovane _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema ve sloupci categories nakonfigurovanou referenci!',_configs[ii];
					END IF;

					_config_id_base_ii := _config_id_reference_ii;
				ELSE
					_config_id_base_ii := _configs[ii]; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
				END IF;

				-----------------------------------------------
				-- kontrola 1. => [na verzi extenze NE-ZALEZI]
				WITH
				w1 AS	(
					SELECT tat.* FROM @extschema@.t_aux_total AS tat
					WHERE tat.estimation_cell = _estimation_cell[i]
					AND tat.config = _config_id_base_ii
					)
				SELECT count(*) FROM w1	-- pokud je pocet vetsi jak 0 tak zaznam(y) exisuje(i)
				INTO _check_ii_1;

				IF _check_ii_1 > 0
				THEN
					IF ii = 1
					THEN
						_check_ii_boolean_1 := array[0];
					ELSE
						_check_ii_boolean_1 := _check_ii_boolean_1 || array[0];
					END IF;
				ELSE
					IF ii = 1
					THEN
						_check_ii_boolean_1 := array[1];
					ELSE
						_check_ii_boolean_1 := _check_ii_boolean_1 || array[1];
					END IF;
				END IF;
				-----------------------------------------------
				-- kontrola 2. => [na verzi extenze ZALEZI]
				WITH
				w1 AS	(
					SELECT tat.* FROM @extschema@.t_aux_total AS tat
					WHERE tat.estimation_cell = _estimation_cell[i]
					AND tat.config = _config_id_base_ii
					AND tat.ext_version >= _ext_version_valid_from
					AND tat.ext_version <= _ext_version_valid_until
					)
				SELECT count(*) FROM w1
				INTO _check_ii_2;

				IF _check_ii_2 > 0 -- pokud je pocet vetsi jak 0 tak zaznam(y) exisuje(i)
				THEN
					IF ii = 1
					THEN
						_check_ii_boolean_2 := array[0];
					ELSE
						_check_ii_boolean_2 := _check_ii_boolean_2 || array[0];
					END IF;
				ELSE
					IF ii = 1
					THEN
						_check_ii_boolean_2 := array[1];
					ELSE
						_check_ii_boolean_2 := _check_ii_boolean_2 || array[1];
					END IF;
				END IF;
				-----------------------------------------------
			END LOOP;
			
			-- vyhodnoceni vysledku kontroly 1
			IF	(
				SELECT sum(t.check_ii_boolean_1) > 0
				FROM (SELECT unnest(_check_ii_boolean_1) AS check_ii_boolean_1) AS t
				)
			THEN
				RAISE EXCEPTION 'Chyba 11: fn_get_gids4aux_total_app: Prepocet neni mozny => pro zadanou estimation_cell = % doposud neexistuje v tabulce t_aux_total hodnota aux_total pro nekterou z config_id = %. Resit klasickym vypoctem!',_estimation_cell[i],_configs;
			END IF;	

			-- vyhodnoceni vysledku kontroly 2
			IF	(
				SELECT sum(t.check_ii_boolean_2) = 0
				FROM (SELECT unnest(_check_ii_boolean_2) AS check_ii_boolean_2) AS t
				)
			THEN
				RAISE EXCEPTION 'Chyba 12: fn_get_gids4aux_total_app: Prepocet neni mozny => pro zadanou estimation_cell = % jiz v tabulce t_aux_total existuji hodnoty aux_total pro vsechny kategorie =  %, ktere jsou ve verzi, jez odpovida nektere verzi od % do verze %!',_estimation_cell[i],_configs,_ext_version_valid_from_label,_ext_version_valid_until_label;

				-- tato podminka defakto ze vstupniho argumentu _estimation_cell (muze obsahovat i ZSJ) donuti uzivatele tam mit jen cely, ktere jsou opravdu povoleny pro prepocet !!!
				-- navic prepocet je konstruovan tak, ze pri prepoctu se prepocitaji i vsechny cely podrizene, a to jen ty podrizenene cely, ktere uz jsou v DB vypocitany a nespadaji do povoleneho intervalu VERZI
			END IF;
			
		END LOOP;
		-----------------------------------------------------------------------------------
		-----------------------------------------------------------------------------------
		IF _config_query = ANY(array[100,200,300,400]) -- => protinani, soucet a doplnky
		THEN
			-- proces ziskani:
			-- pole poli estimation_cell [jde jen o vstupni estimation_cell (muze jit i o estimation_cell, ktere jsou ZSJ)] <= _res_estimation_cell_prepocet
			-- pole poli ZSJ [jde o gidy, ktere tvori vstupni estimation_cell (gidy se zde mohou opakovat)] <= _res_gids_prepocet
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-- cyklus pro ziskani POCTu u PREPOCTU
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni gidu z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i_prepocet;

				IF _gids4estimation_cell_i_prepocet IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 13: fn_get_gids4aux_total_app: Pro _estimation_cell[i] = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell[i];
				END IF;

				_gids4estimation_cell_pocet_prepocet := array_length(@extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i_prepocet),1);

				IF i = 1
				THEN
					_pocet_prepocet := array[_gids4estimation_cell_pocet_prepocet];
				ELSE
					_pocet_prepocet := _pocet_prepocet || array[_gids4estimation_cell_pocet_prepocet];
				END IF;
				
			END LOOP;
			-----------------------------------------------------
			-- zjisteni nejdelsiho pole _gids4estimation_cell_pocet_prepocet
			SELECT max(t.pocet) FROM (SELECT unnest(_pocet_prepocet) as pocet) AS t
			INTO _max_pocet_prepocet;
			-----------------------------------------------------
			-- cyklus ziskani GIDu nejnizsi urovne pro vstupni _estimation_cell[]
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni gidu z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i_prepocet;

				-- volani funkce fn_get_lowest_gids
				-- funkce by mela vratit seznam gidu nejnizsi urovne
				_gids4estimation_cell_prepocet := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i_prepocet);

				-- zjisteni poctu prvku(gidu) v poli _gids4estimation_cell_prepocet
				_pocet_gids4estimation_cell_prepocet := array_length(_gids4estimation_cell_prepocet,1);

				-- vytvoreni pole s nulama o poctu _max_pocet - _pocet_gids4estimation_cell_prepocet
				SELECT array_agg(t.gid_gs) FROM (SELECT 0 AS gid_gs FROM generate_series(1,(_max_pocet_prepocet - _pocet_gids4estimation_cell_prepocet))) AS t
				INTO _doplnek_prepocet;

				-- proces doplneni 0 tak aby pole _gids4estimation_cell melo delku _max_pocet
				_gids4estimation_cell_prepocet := _gids4estimation_cell_prepocet || _doplnek_prepocet;

				IF i = 1
				THEN
					_res_estimation_cell_prepocet := array[array[_estimation_cell[i]]];	-- do pole poli se ulozi pozadovane vstupni estimation_cell pro prepocet
					_res_gids_prepocet := array[_gids4estimation_cell_prepocet];		-- do pole poli se ulozi pozadovane gidy nejnizsi urovne pro prepocet
				ELSE
					_res_estimation_cell_prepocet := _res_estimation_cell_prepocet || array[array[_estimation_cell[i]]];
					_res_gids_prepocet := _res_gids_prepocet || array[_gids4estimation_cell_prepocet];
				END IF;

			END LOOP;
			-----------------------------------------------------
			-- vysvetleni internich argumentu:
			-- _res_estimation_cell_prepocet	=> jde o pole poli s estimation_cell ze vstupu uzivatele urcenych pro prepocet [muze obsahovat i zakladni stavebni jednotku]
								-- pokud to doslo az sem, tak se v poli nemuze vyskytovat estimation_cell nebo defakto ZSJ, ktera je ve verzi, ktere spada
								-- do povoleneho intervalu VERZI (pozn. v intervalu je vzdy zahrnuta i aktualni systemova verze extenze)
			-- _res_gids_prepocet			=> jde o pole poli gidu zakladnich stavebnich jednotek pro vstupni estimation_cell urcenych pro prepocet, [mozny vyskyt DUPLICIT]
								-- pokud to doslo az sem, tak se zde mohou vyskytovat ZSJ, ktere uz jsou v databazi ve verzi, ktera spada do povoleneho intervalu
								-- VERZI => tyto gidy neni nutno prepocitavat, prepocitaji se jen gidy verze, ktere NE-SPADAJI do povoleneho intervalu VERZI
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
	

			-----------------------------------------------------
			-- CONFIG_ID_BASE --
			-----------------------------------------------------
			IF _config_query = 500 -- REFERENCE
			THEN
				SELECT tc.categories::integer
				FROM @extschema@.t_config AS tc
				WHERE tc.id = _config_id
				INTO _config_id_reference;

				IF _config_id_reference IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 14: fn_get_gids4aux_total_app: Vstupni _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema nakonfigurovanou referenci!',_config_id;
				END IF;

				_config_id_base := _config_id_reference;
			ELSE
				_config_id_base := _config_id; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
			END IF;
			-----------------------------------------------------
			-----------------------------------------------------

		
			-----------------------------------------------------
			-- proces kontroly, ze vstupni ESTIMATION_CELL uz
			-- byly alespon jednou v minulosti vypocitany, tzn.
			-- ze na verzi ext_version nezalezi
			-- => kontrola probiha na urovni KATEGORIE dane konfigurace
			-----------------------------------------------------
			-- prevedeni _res_estimation_cell_prepocet na simple pole
			SELECT array_agg(t.recp) FROM (SELECT unnest(_res_estimation_cell_prepocet) AS recp) AS t
			INTO _res_estimation_cell_prepocet_simple;

			-- kontrola pro _res_estimation_cell_prepocet_simple
			FOR i IN 1..array_length(_res_estimation_cell_prepocet_simple,1)
			LOOP
				IF	(
					SELECT count(tat.*) = 0
					FROM @extschema@.t_aux_total AS tat
					WHERE tat.config = _config_id_base
					AND tat.estimation_cell = _res_estimation_cell_prepocet_simple[i]
					-- zde ext_version nehraje roli
					)
				THEN
					RAISE EXCEPTION 'Chyba 15: fn_get_gids4aux_total_app: Pro config_id = % a estimation_cell = % doposud neexistuje zaznam v tabulce t_aux_total. Prepocet tedy neni mozny! Nutno dopocitat klasickym vypoctem!',_config_id_base,_res_estimation_cell_prepocet_simple[i];
				END IF;
			END LOOP;
			-----------------------------------------------------
			-----------------------------------------------------


			-----------------------------------------------------
			-- proces kontroly, ze ZSJ, ktere tvori jednotlive
			-- vstupni estimation_cell, uz byly alespon jednou
			-- v minulosti vypocitany, verze extenze zde nehraje
			-- roli		
			-- => kontrola probiha na urovni KATEGORIE dane konfigurace
			-----------------------------------------------------
			-- proces prevedeni _res_gids_prepocet na simple pole
			-- a provedeni distinctu gidu ZSJ
			WITH
			w AS	(
				SELECT DISTINCT t.rgp
				FROM (SELECT unnest(_res_gids_prepocet) AS rgp) AS t
				WHERE t.rgp IS DISTINCT FROM 0
				)
			SELECT array_agg(w.rgp) FROM w
			INTO _res_gids_prepocet_simple;

			-- samotna kontrola pro _res_gids_prepocet_simple
			FOR i IN 1..array_length(_res_gids_prepocet_simple,1)
			LOOP
				-- jde-li o CQ 100, pak kontrola musi probihat
				-- pres gidy, respektive pres sloupec cell
				-- v tabulce t_aux_total
				IF _config_query = 100
				THEN
					IF	(
						SELECT count(tat.*) = 0
						FROM @extschema@.t_aux_total AS tat
						WHERE tat.config = _config_id_base
						AND tat.cell = _res_gids_prepocet_simple[i]
						-- zde ext_version nehraje roli,
						-- poznamka: ZSJ, ktere svou verzi spadaji do povoleneho intervalu VERZI, budou nize v kodu
						-- ze seznamu vyjmuty a do samotneho prepoctu nepujdou !!!
						)
					THEN
						RAISE EXCEPTION 'Chyba 16: fn_get_gids4aux_total_app: Pro config_id = % a cell = % doposud neexistuje zaznam v tabulce t_aux_total. Prepocet tedy neni mozny! Nutno dopocitat klasickym vypoctem!',_config_id_base,_res_gids_prepocet_simple[i];
					END IF;
				END IF;

				-- jde-li o CQ 200,300,400, pak kontrola musi probihat
				-- rovnez pres gid, respektive pres sloupec
				-- cell v tabulce t_aux_total, ale je zde jeste vyjimka
				IF _config_query = ANY(array[200,300,400])
				THEN
					-- pokud je i-ty gid v _res_gids_prepocet_simple ZSJ, ktera je tvorena 2 a vice
					-- geometriemi, pak se nize uvedena kontrola provadet nemusi, a to proto, ze
					-- pro takove ZSJ se pro CQ 200,300,400 do databaze vubec neukladaji
					IF	(
						SELECT count(fac2.gid) = 1
						FROM @extschema@.f_a_cell AS fac2
						WHERE fac2.estimation_cell =
									(
									SELECT fac1.estimation_cell
									FROM @extschema@.f_a_cell AS fac1
									WHERE fac1.gid = _res_gids_prepocet_simple[i]
									)
						)
					THEN
						IF	(
							SELECT count(tat.*) = 0
							FROM @extschema@.t_aux_total AS tat
							WHERE tat.config = _config_id_base
							AND tat.cell = _res_gids_prepocet_simple[i]	
							-- zde ext_version nehraje roli
							-- poznamka: ZSJ, ktere svou verzi spadaji do povolenoho intervalu VERZI, budou nize v kodu
							-- ze seznamu vyjmuty a do samotneho prepoctu nepujdou !!!
							)
						THEN
							RAISE EXCEPTION 'Chyba 17: fn_get_gids4aux_total_app: Pro config_id = % a cell = % doposud neexistuje zaznam v tabulce t_aux_total. Prepocet tedy neni mozny! Nutno dopocitat klasickym vypoctem!',_config_id_base,_res_gids_prepocet_simple[i];
						END IF;
					END IF;
				END IF;
			END LOOP;
			-----------------------------------------------------
			-----------------------------------------------------


			-----------------------------------------------------
			-- zachovani _res_estimation_cell_prepocet_simple
			_res_estimation_cell_prepocet_simple_original := _res_estimation_cell_prepocet_simple;
			-----------------------------------------------------


			-----------------------------------------------------
			-- proces doplneni interni promenne
			-- _res_estimation_cell_prepocet_simple
			-- podrizenymi estimation_cell
			-----------------------------------------------------
			FOR i in 1..array_length(_estimation_cell,1)
			LOOP
				SELECT cec.estimation_cell_collection
				FROM @extschema@.c_estimation_cell AS cec
				WHERE cec.id = _estimation_cell[i]
				INTO _ecc4prepocet;
				
				SELECT cmecc.estimation_cell_collection_lowest
				FROM @extschema@.cm_estimation_cell_collection AS cmecc
				WHERE cmecc.estimation_cell_collection = _ecc4prepocet
				INTO _eccl4prepocet;

				IF _ecc4prepocet = _eccl4prepocet
				THEN
					-- pro prepocet uz se nemusi zjistovat podrizene estimation_cell
					_res_estimation_cell_prepocet_simple := _res_estimation_cell_prepocet_simple; -- datovy typ integer[]
				ELSE
					-- pro prepocet se musi zjistit podrizene estimation_cell
					
					-- zjisteni vsech zakladnich gidu tvorici i-tou estimation_cell
					SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
					WHERE fac.estimation_cell = _estimation_cell[i]
					INTO _gids4estimation_cell_i_prepocet_mezistupen;

					_gids4estimation_cell_prepocet_mezistupen := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i_prepocet_mezistupen);	

					_gids_mezistupen_with_vstupni_estimation_cell := @extschema@.fn_get_gids4under_estimation_cells_app(_gids4estimation_cell_prepocet_mezistupen,_gids4estimation_cell_i_prepocet_mezistupen);

					SELECT array_agg(fac.estimation_cell) FROM @extschema@.f_a_cell AS fac
					WHERE fac.gid IN (SELECT unnest(_gids_mezistupen_with_vstupni_estimation_cell))
					AND fac.estimation_cell IS DISTINCT FROM _estimation_cell[i]
					INTO _estimation_cells_mezistupen_i;

					IF _estimation_cells_mezistupen_i IS NULL
					THEN
						-- RAISE EXCEPTION 'Chyba 11: fn_get_gids4aux_total_app: Interni promenna _estimation_cells_mezistupen_i nesmi byt nikdy NULL!';
						-- tato vyjimka byla zde puvodne chybne, protoze pokud je na vstupu napr. okres, pak mezistupen pro estimation_cell mezi okresem
						-- a KU uz vlastne neni, toto nastava i mezi OPLO a PLO, takze potom to bude takto:
						_res_estimation_cell_prepocet_simple := _res_estimation_cell_prepocet_simple;

						-- chybejici PLO pro OPLO se zde ale musi doplnit => veresit ve funkci fn_get_gids4under_estimation_cells
					ELSE
						_res_estimation_cell_prepocet_simple := _res_estimation_cell_prepocet_simple || _estimation_cells_mezistupen_i;
					END IF;			
				END IF;	
			END LOOP;
			-----------------------------------------------------
			-----------------------------------------------------

			-----------------------------------------------------
			-- distinct hodnot estimation_cell v promenne
			-- _res_estimation_cell_prepocet_simple
			WITH
			w AS	(SELECT distinct t.ecp FROM (SELECT unnest(_res_estimation_cell_prepocet_simple) AS ecp) AS t)
			SELECT array_agg(w.ecp order by w.ecp) FROM w
			INTO _res_estimation_cell_prepocet_simple;

			-- _res_estimation_cell_prepocet_simple [seznam vsech moznych estimation_cell pro prepocet], obsahuje:
			-- v pripade NUTS => vstupni estimation_cell a muzou to byt i ZSJ 
			-- v pripade OPLO => vstupni estimation_cell, rozdrobene ZSJ aplikace neposle, ale napr. pro OPLO se vrati podrizeni PLOcka

			-- takze jde o seznam => VSTUPNI ESTIMATION_CELL + jejich vsechny mozne [ALL] PODRIZENE ESTIMATION_CELL
			-----------------------------------------------------
			

			-----------------------------------------------------
			-- propojeni _res_estimation_cell_prepocet_simple
			-- s databazi a zjisteni estimation_cell, ktere v
			-- databazi jeste pro dane config a povoleny interval
			-- VERZI nejou vypocitany
			-----------------------------------------------------
			WITH
			w1 AS	(
				SELECT DISTINCT tat.estimation_cell
				FROM @extschema@.t_aux_total AS tat
				WHERE tat.config = _config_id_base
				AND tat.cell IS NULL			-- cell se ukladaji jen pro ZSJ, takze toto zajisti ze ZSJ do INTO nepujdou
				AND tat.estimation_cell IN
					(SELECT unnest(_res_estimation_cell_prepocet_simple))
				--AND tat.ext_version < _ext_version_current
				AND tat.ext_version <= _ext_version_valid_until -- defakto valid_until se vzdy bude shodovat s _ext_version_current
				),
			w2 AS	(
				SELECT DISTINCT tat.estimation_cell
				FROM @extschema@.t_aux_total AS tat
				WHERE tat.config = _config_id_base
				AND tat.cell IS NULL
				AND tat.estimation_cell IN
					(SELECT unnest(_res_estimation_cell_prepocet_simple))
				AND tat.ext_version >= _ext_version_valid_from
				AND tat.ext_version <= _ext_version_valid_until
									-- tato podminka je zde proto, ze uzivatel mohl pred tim PREPOCET ZASTAVIT,
									-- tzn. ze jen cast estimation_cell se vypocitala, respektive prepocitala
									-- v ext_version_current, takze pak do prepoctu znovu potrebuji znat jen ty
									-- estimation_cell, ktere jeste nespadaji do povoleneho intervalu VERZI
				),
			w3 AS	(
				SELECT w1.estimation_cell FROM w1 EXCEPT
				SELECT w2.estimation_cell FROM w2
				)
			SELECT array_agg(w3.estimation_cell) FROM w3
			INTO _res_estimation_cell_prepocet_4_summarization;	-- THIS is A -- CQ 100,200,300,400 a NUTS1-4 nebo OPLO,PLO

			-- jde o seznam POZADOVANYCH ESTIMATION_CELL pro SUMARIZACI u PREPOCTU

			-- u CQ 100	a	NUTS1-4		=> to je v poradku
			-- u CQ 100	a	OPLO,PLO 	=> to je v poradku

			-- u CQ 300	a	NUTS1-4		=> to je v poradku <= CQ 100 uz musi existovat
			-- u CQ 300 	a	OPLO,PLO	=> to je v poradku <= CQ 100 uz musi existovat

			-- tzn. pro kontrolu proveditelnosti CQ 300 musi do cyklu jit _res_estimation_cell_prepocet_4_summarization
			-----------------------------------------------------
			-----------------------------------------------------


			--raise notice '_res_estimation_cell_prepocet_4_summarization:%',_res_estimation_cell_prepocet_4_summarization;
			--raise exception 'STOP';


			-----------------------------------------------------
			-----------------------------------------------------
			-----------------------------------------------------
			-- propojeni seznamu ZSJ s databazi, v pride NUTS1-4
			-- jde o KU, v pripade OPLO,PLO jde o rozdrobene casti
			-- [ze seznamu se musi vyjmout ZSJ, ktere uz jsou ve
			-- verzi spadajici do povoleneho intervalu VERZI

			-- u CQ 100 se ZSJ ukladaji jak pro NUTS1-4 tak pro
			-- OPLO a PLO => takze zde se musi pracovat se
			-- sloupcem cell v tabulce t_aux_total, protoze pro
			-- ZSJ u OPLO,PLO se estimation_cell neuklada

			-- u CQ 200,300,400 a pro NUTS1-4 se ZSJ do t_aux_total
			-- ukladaji => zde se muze pracovat bud se sloupcem cell
			-- nebo se sloupcem estimation_cell
			
			-- u CQ 200,300,400 a pro OPLO,PLO se ZSJ do t_aux_total
			-- NE-ukladaji (proc, jde totiz o ZSJ, ktera sama netvori
			-- celou estimation_cell, jejich aux_total hodnoty se
			-- neukladaji, ukladaji se az jejich sumarizace), zde
			-- se musi pracovat ze sloupce estimation_cell
			-----------------------------------------------------
			IF _config_query = 100
			THEN
				WITH
				w1 AS	(SELECT unnest(_res_gids_prepocet_simple) AS gid),	-- _res_gids_prepocet_simple [ALL KU nebo ALL rozdrobene casti PLO tvorici vstupni estimation_cell pro prepocet]
				w2 AS	(
					SELECT DISTINCT tat.cell
					FROM @extschema@.t_aux_total AS tat
					WHERE tat.config = _config_id_base
					AND tat.cell IN (SELECT w1.gid FROM w1)
					AND tat.ext_version >= _ext_version_valid_from
					AND tat.ext_version <= _ext_version_valid_until
					),
				w3 AS	(
					SELECT
						w1.gid,
						w2.cell
					FROM
						w1 LEFT JOIN w2 ON w1.gid = w2.cell
					)
				SELECT
					array_agg(w3.gid) FROM w3 WHERE w3.cell IS NULL
				INTO
					_res_gids_prepocet_4_recount;	-- THIS is B -- CQ 100 a NUTS1-4 nebo OPLO,PLO
			END IF;

			-- seznam ZSJ [KU nebo rozdrobene casti PLO] POZADOVANE
			-- pro prepocet [jde o gidy, ktere jsou starsi verze nez
			-- je povoleny interval VERZI
			-----------------------------------------------------
			-----------------------------------------------------

			
			-----------------------------------------------------
			-----------------------------------------------------
			IF _config_query = ANY(array[200,300,400])
			THEN			
				WITH
				w1 AS	(SELECT unnest(_res_gids_prepocet_simple) AS gid),	-- _res_gids_prepocet_simple [ALL KU nebo ALL rozdrobene casti PLO tvorici vstupni estimation_cell pro prepocet] 
				w2 AS	(-- ALL estimation_cell --				-- vyber estimation_cell z f_a_cell na zaklade gidu (ZSJ)
					SELECT DISTINCT fac.estimation_cell			-- provedeni distinctu estimation_cell
					FROM @extschema@.f_a_cell AS fac
					WHERE fac.gid IN (SELECT w1.gid FROM w1)
					),
					-- vyber (propojeni s DB) estimation_cell co je v DB, zde nejprve nezalezi na verzi ext_version
				w3 AS	(-- DB estimation_cell --
					SELECT DISTINCT tat1.estimation_cell
					FROM @extschema@.t_aux_total AS tat1
					WHERE tat1.config = _config_id_base
					AND tat1.estimation_cell IN (SELECT w2.estimation_cell FROM w2)
					),
				w4 AS	(-- DB estimation_cell + povoleny interval VERZI
					SELECT DISTINCT tat2.estimation_cell
					FROM @extschema@.t_aux_total AS tat2
					WHERE tat2.config = _config_id_base
					AND tat2.estimation_cell IN (SELECT w3.estimation_cell FROM w3)
					AND tat2.ext_version >= _ext_version_valid_from
					AND tat2.ext_version <= _ext_version_valid_until
					),
				w5 AS	(
					SELECT
						w3.estimation_cell AS estimation_cell_w3,
						w4.estimation_cell AS estimation_cell_w4
					FROM
						w3 LEFT JOIN w4 ON w3.estimation_cell = w4.estimation_cell
					)
				SELECT
					array_agg(w5.estimation_cell_w3) FROM w5 WHERE w5.estimation_cell_w4 IS NULL
				INTO
					_res_estimation_cell_prepocet_4_recount; -- THIS is C -- CQ 200,300,400 a NUTS1-4 nebo OPLO,PLO
			END IF;

			-- seznam ZSJ [KU nebo cele PLO] kodovanych pres estimation_cell,
			-- ktere jsou POZADOVANY pro prepocet [jde o ZSJ, ktere jsou
			-- starsi verze nez je aktualni systemova verze extenze
			----------------------------------------------------
			----------------------------------------------------
		END IF;
		-----------------------------------------------------
		-----------------------------------------------------
		

		-------------------------------------------
		--raise notice '_res_estimation_cell_prepocet_4_summarization:%',_res_estimation_cell_prepocet_4_summarization;
		--raise notice '_res_gids_prepocet_4_recount:%',_res_gids_prepocet_4_recount;
		--raise notice '_res_estimation_cell_prepocet_4_recount:%',_res_estimation_cell_prepocet_4_recount;
		--raise exception 'STOP';
		-------------------------------------------

		IF _config_query = 100	-- vetev pro zakladni protinani [ZSJ] a sumarizaci
		THEN
			_q :=
				'
				WITH
				---------------------------------------------------------
				w1 AS	(SELECT unnest($8) AS gid),	-- THIS is B
				w2 AS	(
					SELECT
						1 AS step,
						$3 AS config,
						fac.estimation_cell,
						w1.gid
					FROM
						w1
					LEFT
					JOIN	@extschema@.f_a_cell AS fac ON w1.gid = fac.gid -- toto doplni estimation_cell [v aplikaci by se mohlo spojit LABEL a GID]
					),
				---------------------------------------------------------
				w3 AS	(SELECT unnest($9) AS estimation_cell), -- THIS is A
				w4 AS	(
					SELECT
						2 AS step,
						$3 AS config,
						w3.estimation_cell,
						0::integer AS gid
					FROM
						w3
					ORDER
						BY w3.estimation_cell
					),
				w5 AS	(
					SELECT
						w2.step,
						w2.config,
						w2.estimation_cell,
						w2.gid
					FROM
						w2
					ORDER
						BY w2.step, w2.config, w2.gid, w2.estimation_cell
					)
				---------------------------------------------------------
				SELECT * FROM w5 UNION ALL
				SELECT * FROM w4					
				';
			
		ELSE
			-- _config_query = ANY(array[200,300,400,500]) -- vetev pro soucet a doplnky

			-- spojeni _res_estimation_cell_prepocet_4_summarization a _estimation_cell a udelan distinct
			-- proc? vstupni estimation_cell muze obsahovat i estimation_cell ZSJ
			-- _res_estimation_cell_prepocet_4_summarization neobsahuje estimation_cell ZSJ

			-- pridani jeste spojeni _res_estimation_cell_prepocet_4_recount
		
			WITH
			w1 AS	(SELECT unnest(_res_estimation_cell_prepocet_4_summarization) AS estimation_cell), 	-- seznam estimation_cell pro sumarizaci, odpovida co je v DB
			w2 AS	(SELECT unnest(_estimation_cell) AS estimation_cell),					-- vstupni estimation_cell, mohou obsahovat i ZSJ
			w3 AS	(SELECT unnest(_res_estimation_cell_prepocet_4_recount) AS estimation_cell),		-- estimation_cell jako ZSJ, odpovida tomu co je v DB
			w4 AS	(
				SELECT w1.estimation_cell FROM w1
				UNION					-- union provede defakto distinct
				SELECT w2.estimation_cell FROM w2
				UNION
				SELECT w3.estimation_cell FROM w3
				)
			SELECT array_agg(w4.estimation_cell) FROM w4
			INTO _res_estimation_cell_prepocet_4_check;

			-------------------------------------------------------
			-- raise notice '_res_estimation_cell_prepocet_4_check:%',_res_estimation_cell_prepocet_4_check;			
			-------------------------------------------------------

			-- KONTROLA prepocitatelnosti pro CONFIG_QUERY:
			-- 200,300,400	=> jsou v t_aux_total prepocitany data zakladu a odpovidaji verzi spadajici do povoleneho intervalu VERZI
			-- 500		=> je v t_aux_total reference odpovidajici verzi spadajici do povoleneho intervalu VERZI => zde probihaji jen kontroly => vystupem je bud vyjimka nebo 0 radku
		
			-- zjisteni potrebnych promennych z konfigurace pro vetev soucet nebo doplnky
			SELECT
				tc.complete,
				tc.categories
			FROM
				@extschema@.t_config AS tc WHERE tc.id = _config_id
			INTO
				_complete,
				_categories;

			-- raise notice '_complete: %',_complete;
			-- raise notice '_categories: %',_categories;

			-- spojeni konfiguracnich idecek _complete/_categories
			IF _complete IS NULL
			THEN
				_config_ids_text := concat('array[',_categories,']');
			ELSE
				_config_ids_text := concat('array[',_complete,',',_categories,']');
			END IF;

			-- raise notice '_config_ids_text: %',_config_ids_text;

			-- zjisteni poctu idecek tvorici _complete/_categories
			EXECUTE 'SELECT array_length('||_config_ids_text||',1)'
			INTO _config_ids_length;

			-- raise notice '_config_ids_length: %',_config_ids_length;

			-- zjisteni existujicich konfiguracnich idecek pro (_complete/_categories)
			EXECUTE 'SELECT array_agg(tc.id ORDER BY tc.id) FROM @extschema@.t_config AS tc WHERE tc.id in (select unnest('||_config_ids_text||'))'
			INTO _config_ids;

			-- raise notice '_config_ids: %',_config_ids;

			-- splneni podminky stejneho poctu idecek
			IF _config_ids_length = array_length(_config_ids,1)
			THEN
				-- kontrola zda i obsah je stejny
				EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text||') AS ids) AS t'
				INTO _config_ids4check;

				IF _config_ids4check = _config_ids
				THEN
					-- cyklus kontroly, zda pro soucty/doplnky a referenci jsou vypocitany vsechny potrebne
					-- kategorie a odpovidaji verzim spadajici do povoleneho intervalu VERZI
					-- pozn. zde si uz muzu sahat do tabulky t_aux_total, protoze pro tento typ vypoctu
					-- uz musi existovat zakladni (protinaci) aux_total hodnoty, respektive musely pro
					-- complete nebo categories probehnout _config_query 100 !!!

					--raise notice '_res_estimation_cell_prepocet_4_check: %',_res_estimation_cell_prepocet_4_check;
					
					FOR i IN 1..array_length(_res_estimation_cell_prepocet_4_check,1)
					LOOP
						FOR y IN 1..array_length(_config_ids,1)
						LOOP
							WITH
							w1 AS	(
								SELECT
								tat.*,
								max(tat.est_date) OVER (PARTITION BY tat.estimation_cell, tat.config) as max_est_date
								FROM @extschema@.t_aux_total AS tat
								WHERE tat.config = _config_ids[y]
								AND tat.estimation_cell = _res_estimation_cell_prepocet_4_check[i]
								AND tat.ext_version >= _ext_version_valid_from
								AND tat.ext_version <= _ext_version_valid_until
								)
							,w2 AS	(
								SELECT w1.* FROM w1 WHERE w1.est_date = w1.max_est_date
								)
							SELECT count(*) FROM w2
							INTO _check_avc4categories;
						

							IF _check_avc4categories != 1
							THEN		
								IF _config_query = 500
								THEN
									RAISE EXCEPTION 'Chyba 18: fn_get_gids4aux_total_app: Vstupni _config_id = % je reference na _config_id = % a pro estimation_cell = % neexistuje v tabulce t_aux_total vypocitana hodnota aux_total ve verzi od % do %!',_config_id_base,_config_ids[y],_res_estimation_cell_prepocet_4_check[i],_ext_version_valid_from_label,_ext_version_valid_until_label;
								ELSE
									RAISE EXCEPTION 'Chyba 19: fn_get_gids4aux_total_app: Vstupni _config_id = % je soucet nebo doplnek a pro konfiguraci [z complete/categories] = % a pro estimation_cell = % neexistuje v tabulce t_aux_total vypocitana hodnota aux_total ve verzi do % do %!',_config_id_base,_config_ids[y],_res_estimation_cell_prepocet_4_check[i],_ext_version_valid_from_label,_ext_version_valid_until_label;
								END IF;
							END IF;
						END LOOP;
					END LOOP;
				ELSE
					RAISE EXCEPTION 'Chyba 20: fn_get_gids4aux_total_app: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v tabulce t_config!',_complete,_categories;
				END IF;
			ELSE
				RAISE EXCEPTION 'Chyba 21: fn_get_gids4aux_total_app: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v t_config!',_complete,_categories;
			END IF;

			IF _config_query = 500 -- u reference probihaji jen kontroly a do vystupu se nic nevraci
			THEN
				_q :=	'
					WITH
					w1 AS	(
						SELECT
							1 AS step,
							$3 AS config_id,
							NULL::integer AS estimation_cell,
							NULL::integer AS gid
						)
					SELECT
						w1.step,
						w1.config_id,
						w1.estimation_cell,
						w1.gid
					FROM
						w1 WHERE w1.step = 0;
					';
			ELSE	-- vetev pro config_query 200,300,400 => zde bude promenna _res_estimation_cell_prepocet_4_check, ta obsahuje jak pripadne ZSJ,
				-- tak estimation_cell ze vstupu + estimation_cell podrizene
				_q :=
					'
					WITH
					w1 AS	(SELECT unnest($10) AS estimation_cell),
					w2 AS	(
						SELECT
							1 AS step,
							$3 AS config,
							w1.estimation_cell,
							NULL::integer AS gid
						FROM
							w1
						)
					SELECT
						w2.step,
						w2.config,
						w2.estimation_cell,
						w2.gid
					FROM
						w2
					ORDER
						BY w2.step, w2.config, w2.estimation_cell
					';
			END IF;	
		END IF;		
	ELSE
		-- vetev pro VYPOCET

		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- Tento proces zatim neimplementuju, podle me neni nutny, tim ze to uzivateli pada na
		-- chybe 19, tak podle me spatne zadal geograficke urovne pro vypocet.
		-- proces odstraneni tech vstupnich ZSJ (ktere tvori estimation_cell jen jednou geometrii)
		-- ze vstupni promenne _estimation_cell, pokud _estimation_cell obsahuje jeji vyssi
		-- geografickou jednotku
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- KONTROLA EXISTENCE v DB --

		-- zde se v databazi kontroluje pritomnost zaznamu, a to
		-- pro i-tou estimation_cell a pro vsechny config_id dane
		-- "konfigurace" => pokud tato podminka je splnena, pak
		-- nastane vyjimka => uzivatel toto uz v aplikaci zadat nesmi

		-- tato kontrola je zde z duvodu, kdy uzivatel zastavi vypocet,
		-- verze extenze je zde nutna a verze extenze pro jednotlive kategorie konfigurace
		-- musi odpovidat (spadat) do povoleneho intervalu VERZI
		
		FOR i IN 1..array_length(_estimation_cell,1)
		LOOP
			FOR ii IN 1..array_length(_configs,1)
			LOOP
				SELECT tc.config_query FROM @extschema@.t_config AS tc
				WHERE tc.id = _configs[ii]
				INTO _config_query_ii;
				
				IF _config_query_ii = 500 -- REFERENCE
				THEN
					SELECT tc.categories::integer
					FROM @extschema@.t_config AS tc
					WHERE tc.id = _configs[ii]
					INTO _config_id_reference_ii;

					IF _config_id_reference_ii IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 22: fn_get_gids4aux_total_app: Kontrolovane _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema ve sloupci categories nakonfigurovanou referenci!',_configs[ii];
					END IF;

					_config_id_base_ii := _config_id_reference_ii;
				ELSE
					_config_id_base_ii := _configs[ii]; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
				END IF;

				WITH
				w1 AS	(
					SELECT tat.* FROM @extschema@.t_aux_total AS tat
					WHERE tat.estimation_cell = _estimation_cell[i]
					AND tat.config = _config_id_base_ii
					--AND tat.ext_version = _ext_version_current
					AND tat.ext_version >= _ext_version_valid_from AND tat.ext_version <= _ext_version_valid_until	-- NEW --
					)
				SELECT count(*) FROM w1	-- pokud je pocet vetsi jak 0 tak zaznam(y) exisuje(i)
				INTO _check_ii;

				IF _check_ii > 0
				THEN
					IF ii = 1
					THEN
						_check_ii_boolean := array[0];
					ELSE
						_check_ii_boolean := _check_ii_boolean || array[0];
					END IF;
				ELSE
					IF ii = 1
					THEN
						_check_ii_boolean := array[1];
					ELSE
						_check_ii_boolean := _check_ii_boolean || array[1];
					END IF;
				END IF;

			END LOOP;

			IF	(
				SELECT sum(t.check_ii_boolean) = 0
				FROM (SELECT unnest(_check_ii_boolean) AS check_ii_boolean) AS t
				)
			THEN
				IF _config_query = 500
				THEN
					RAISE NOTICE 'NOTICE: fn_get_gids4aux_total_app: Vstupni kategorie _config_id = % z konfigurace	config_collection = % je REFERENCE a pro zadanou i_tou estimation_cell = % jiz v tabulce t_aux_total existuji hodnoty aux_total pro vsechny kategorie dane konfigurace ve verzi %. Tzn. ze nektera z predchozich kategorii dane konfigurace	nebyla referenci a jejim vypoctem doslo k vypoctu vsech kategorii dane konfigurace u i-te estimation_cell, coz je v poradku a neni duvod k vyjimce. Vstupni seznam pocitanych estimation_cell: %.',_config_id,_config_collection,_estimation_cell[i],_ext_version_current_label,_estimation_cell;
				ELSE
					RAISE EXCEPTION 'Chyba 23: fn_get_gids4aux_total_app: Vypocet neni mozny => pro zadanou estimation_cell = % jiz v tabulce t_aux_total existuji hodnoty aux_total pro vsechny kategorie dane konfigurace (config_collection = %) ve verzi %! Vstupni seznam estimation_cell: %.',_estimation_cell[i],_config_collection,_ext_version_current_label,_estimation_cell;
				END IF;
			END IF;

		END LOOP;
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- CONFIG_ID_BASE
		IF _config_query = 500 -- REFERENCE
		THEN
			SELECT tc.categories::integer
			FROM @extschema@.t_config AS tc
			WHERE tc.id = _config_id
			INTO _config_id_reference;

			IF _config_id_reference IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 24: fn_get_gids4aux_total_app: Vstupni _config_id = % je reference [config_query = 500] a v konfiguraci [v tabulce t_config] nema nakonfigurovanou referenci!',_config_id;
			END IF;

			_config_id_base := _config_id_reference;
		ELSE
			_config_id_base := _config_id; -- vstupnim config_id je bud konfigurace ze zakladni skupiny [100,300,400] nebo konfigurace z agregacni skupiny ci navazujici kolekce [200,300,400]
		END IF;
		--------------------------------------------------------------------------------------------
		-- pokud jde o REFERENCI, pak kontrola, ze _config_id_base pro povoleny interval VERZI
		-- a i_tou estimation_cell uz v DB existuje, resp. musi existovat, jinak vyjimka !!!
		IF _config_query = 500 -- reference
		THEN
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				IF	(
					SELECT count(tat.*) = 0
					FROM @extschema@.t_aux_total AS tat
					WHERE tat.config = _config_id_base
					AND tat.estimation_cell = _estimation_cell[i]
					--AND tat.ext_version = _ext_version_current
					AND tat.ext_version >= _ext_version_valid_from AND tat.ext_version <= _ext_version_valid_until	-- NEW --
					)
				THEN
					RAISE EXCEPTION 'Chyba 25: fn_get_gids4aux_total_app: Vstupni _config_id = % je reference [config_query = 500, referencni id = %], jde o kategorii z konfigurace = %, a pro tuto referenci a estimation_cell = % nejsou doposud vypocitana data v nektere z verzi od % do % verze extenze nfiesta_gisdata!',_config_id,_config_id_base,_config_collection,_estimation_cell[i],_ext_version_valid_from_label,_ext_version_valid_until_label;
				END IF;
			END LOOP;			
		END IF;
		--------------------------------------------------------------------------------------------
		-- KONTROLA proveditelnosti pro "VSTUPNI" CONFIG_QUERY [200,300,400]:
		-- 200,300,400	=> jsou v t_aux_total vypocitany data zakladu v current ext_version
		IF _config_query = ANY(array[200,300,400]) -- vetev pro soucet a doplnky
		THEN
			-- zjisteni potrebnych promennych z konfigurace pro vetev soucet nebo doplnky
			SELECT
				tc.complete,
				tc.categories
			FROM
				@extschema@.t_config AS tc WHERE tc.id = _config_id_base
			INTO
				_complete,
				_categories;

			-- raise notice '_complete: %',_complete;
			-- raise notice '_categories: %',_categories;

			IF _complete IS NULL AND _categories IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 26: fn_get_gids4aux_total_app: Vstupni _config_id = % [jde o kategorii z konfigurace %] je soucet nebo doplnek, a pro jeho konfiguraci nejsou spravne vyplnena pole _complete a _categories!',_config_id,_config_collection;
			END IF;

			-- spojeni konfiguracnich idecek _complete/_categories
			IF _complete IS NULL
			THEN
				_config_ids_text := concat('array[',_categories,']');
			ELSE
				_config_ids_text := concat('array[',_complete,',',_categories,']');
			END IF;

			-- raise notice '_config_ids_text: %',_config_ids_text;

			-- zjisteni poctu idecek tvorici _complete/_categories
			EXECUTE 'SELECT array_length('||_config_ids_text||',1)'
			INTO _config_ids_length;

			-- raise notice '_config_ids_length: %',_config_ids_length;

			-- zjisteni existujicich konfiguracnich idecek pro (_complete/_categories)
			EXECUTE 'SELECT array_agg(tc.id ORDER BY tc.id) FROM @extschema@.t_config AS tc WHERE tc.id in (select unnest('||_config_ids_text||'))'
			INTO _config_ids;

			-- raise notice '_config_ids: %',_config_ids;

			-- splneni podminky stejneho poctu idecek
			IF _config_ids_length = array_length(_config_ids,1)
			THEN
				-- kontrola zda i obsah je stejny
				EXECUTE 'SELECT array_agg(t.ids ORDER BY t.ids) FROM (SELECT unnest('||_config_ids_text||') AS ids) AS t'
				INTO _config_ids4check;

				IF _config_ids4check = _config_ids
				THEN
					-- cyklus kontroly, zda pro souctet nebo doplnek jsou vypocitany vsechny potrebne
					-- kategorie a odpovidaji verzi spadajici do povoleneho intervalu VERZI
					-- pozn. zde si uz muzu sahat do tabulky t_aux_total, protoze pro tento typ vypoctu
					-- uz musi existovat zakladni (protinaci) aux_total hodnoty, respektive musely pro
					-- complete nebo categories probehnout _config_query 100 !!!

					-- raise notice '_estimation_cell: %',_estimation_cell;
					
					FOR i IN 1..array_length(_estimation_cell,1)
					LOOP
						FOR y IN 1..array_length(_config_ids,1)
						LOOP
							WITH
							w1 AS	(
								SELECT
								tat.*,
								max(tat.est_date) OVER (PARTITION BY tat.estimation_cell, tat.config) as max_est_date
								FROM @extschema@.t_aux_total AS tat
								WHERE tat.config = _config_ids[y]
								AND tat.estimation_cell = _estimation_cell[i]
								AND tat.ext_version >= _ext_version_valid_from and tat.ext_version <= _ext_version_valid_until								
								)
							,w2 AS	(
								SELECT w1.* FROM w1 WHERE w1.est_date = w1.max_est_date
								)
							SELECT count(*) FROM w2
							INTO _check_avc4categories;

							IF _check_avc4categories != 1
							THEN		
								RAISE EXCEPTION 'Chyba 27: fn_get_gids4aux_total_app: Vstupni _config_id = % [kategorie z konfigurace %] je soucet nebo doplnek a pro konfiguraci [z complete/categories] = % a pro estimation_cell = % neexistuje v tabulce t_aux_total zadna vypocitana hodnota aux_total ve verzi od % do %!',_config_id_base,_config_collection,_config_ids[y],_estimation_cell[i],_ext_version_valid_from_label,_ext_version_valid_until_label;
							END IF;
						END LOOP;
					END LOOP;
				ELSE
					RAISE EXCEPTION 'Chyba 28: fn_get_gids4aux_total_app: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v t_config!',_complete,_categories;
				END IF;
			ELSE
				RAISE EXCEPTION 'Chyba 29: fn_get_gids4aux_total_app: Pro nakonfigurovane complete = % nebo pro nektere nakonfigurovane categories = % neexistuje konfigurace v t_config!',_complete,_categories;
			END IF;
		END IF;
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------

		IF _config_query = ANY(array[100,200,300,400])
		THEN
			-- zde je implementovan proces ziskani zakladnich stavebnich jednotek (ZSJ)
			---------------------------------------------------------------------------
				
			-- cyklus pro ziskani POCTu
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni zakladnich stavebnich jednotek (gidu) z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i;

				IF _gids4estimation_cell_i IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 30: fn_get_gids4aux_total_app: Pro _estimation_cell[i] = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell[i];
				END IF;

				-- volani funkce fn_get_lowest_gids [funkce si saha do mapovaci tabulky cm_f_a_cell]
				-- funkce by mela vratit seznam zakladnich stavebnich jednotek (gidu), ktere tvori zadany seznam vyssich gidu
				-- v tomto cyklu ale nejprve pozaduju pocet gidu
				_gids4estimation_cell_pocet := array_length(@extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i),1);

				IF i = 1
				THEN
					_pocet := array[_gids4estimation_cell_pocet];
				ELSE
					_pocet := _pocet || array[_gids4estimation_cell_pocet];
				END IF;
				
			END LOOP;

			-- zjisteni nejdelsiho pole _gids4estimation_cell
			SELECT max(t.pocet) FROM (SELECT unnest(_pocet) as pocet) AS t
			INTO _max_pocet;

			-- cyklus ziskani GIDu
			FOR i IN 1..array_length(_estimation_cell,1)
			LOOP
				-- zjisteni gidu z tabulky f_a_cell pro i-tou _estimation_cell
				SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
				WHERE fac.estimation_cell = _estimation_cell[i]
				INTO _gids4estimation_cell_i;

				-- volani funkce fn_get_lowest_gids
				-- funkce by mela vratit seznam gidu nejnizsi urovne, ktere tvori zadany seznam vyssich gidu
				_gids4estimation_cell := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell_i);

				-- zjisteni poctu prvku(gidu) v poli _gids4estimation_cell
				_pocet_gids4estimation_cell := array_length(_gids4estimation_cell,1);

				-- vytvoreni pole s nulama o poctu _max_pocet - _pocet_gids4estimation_cell
				SELECT array_agg(t.gid_gs) FROM (SELECT 0 AS gid_gs FROM generate_series(1,(_max_pocet - _pocet_gids4estimation_cell))) AS t
				INTO _doplnek;

				-- proces doplneni 0 tak aby pole _gids4estimation_cell melo delku _max_pocet
				_gids4estimation_cell := _gids4estimation_cell || _doplnek;

				IF i = 1
				THEN
					_res_estimation_cell := array[array[_estimation_cell[i]]];
					_res_gids := array[_gids4estimation_cell];
				ELSE
					_res_estimation_cell := _res_estimation_cell || array[array[_estimation_cell[i]]];
					_res_gids := _res_gids || array[_gids4estimation_cell];
				END IF;

			END LOOP;
		END IF;

		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------

		-- po sem mam:
			-- _res_estimation_cell => pole poli vstupnich estimation_cell co si uzivatel zvolil [muze
			-- jit o estimation_cell z ruznych geografickych urovni a muzou se zde vyskytovat estimation_cell jako ZSJ]
			-- _res_gids => seznam zakladnich stavebnich jednotek (gidu) pro zadane estimation_cell [pozor gidy se zde mohou duplikovat]

		--------------------------------------------------------------------------------------------
		--raise notice '_res_estimation_cell: %',_res_estimation_cell;
		--raise notice '_res_gids: %',_res_gids;
		--------------------------------------------------------------------------------------------
		--raise notice '_unnest_text:%',_unnest_text;
		--------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------
		-- sestaveni vysledneho textu pro _config_query
		IF _config_query = 100	-- vetev pro zakladni (protinani)
		THEN
			-- kontrola ze pocet prvku v poli poli _res_estimation_cell se rovna poctu prvku v poli poli _res_gids
			IF (array_length(_res_estimation_cell,1) != array_length(_res_gids,1))
			THEN
				RAISE EXCEPTION 'Chyba 31: fn_get_gids4aux_total_app: Pocet prvku v interni promenne _res_estimation_cell neodpovida poctu prvku v interni promenne _res_gids!';
			END IF;

			-- zjisteni poctu prvku v jednotlivych prvcich u _res_gids [u multi array je vsude stejny pocet]
			SELECT array_length(_res_gids,2)
			INTO _res_gids_count_count;		

			-- proces sestaveni _res_estimation_cell_4_with do return query
			FOR i IN 1..array_length(_res_estimation_cell,1)
			LOOP
				WITH
				w AS	(
					SELECT
						unnest(_res_estimation_cell[i:i]) as estimation_cell,
						generate_series(1,_res_gids_count_count)
					)
				SELECT array_agg(w.estimation_cell) FROM w
				INTO _res_estimation_cell_4_with_i;

				IF i = 1
				THEN
					_res_estimation_cell_4_with := array[_res_estimation_cell_4_with_i];
				ELSE
					_res_estimation_cell_4_with := _res_estimation_cell_4_with || array[_res_estimation_cell_4_with_i];
				END IF;
			END LOOP;
			--------------------------------------------------------------------------------------------
			-- proces zjisteni k jednotlivym _res_estimation_cell techto informaci:
			-- je-li estimation_cell zakladni stavebni jednotkou nebo neni
			-- je-li estimation_cell zakladni stavebni jednotkou a je nutna jeste jeji sumarizace,
			WITH
			w1 AS	(
				SELECT unnest(_res_estimation_cell) AS estimation_cell
				),
			w2 AS	(	
				SELECT
					w1.estimation_cell,
					t1.estimation_cell_collection,
					t2.estimation_cell_collection_lowest
				FROM
						w1
				INNER JOIN 	@extschema@.c_estimation_cell 			AS t1	ON w1.estimation_cell = t1.id
				INNER JOIN	@extschema@.cm_estimation_cell_collection	AS t2	ON t1.estimation_cell_collection = t2.estimation_cell_collection
				),
			w3 AS	(
				SELECT
					w2.*,
					CASE
					WHEN w2.estimation_cell_collection = w2.estimation_cell_collection_lowest
					THEN TRUE ELSE FALSE
					END AS estimation_cell_w4
				FROM
					w2
				),
			w4 AS	(
				select
					fac.estimation_cell,
					count(fac.gid) as count_gids
				from @extschema@.f_a_cell AS fac
				where fac.estimation_cell in	(
								SELECT w1.estimation_cell FROM w1
								)
				group by fac.estimation_cell
				),
			w5 AS	(
				SELECT
					w3.*,
					w4.count_gids
				FROM
					w3 LEFT JOIN w4 ON w3.estimation_cell = w4.estimation_cell
				),
			w6 AS	(
				SELECT
					w5.*,
					CASE
						WHEN w5.estimation_cell_w4 = FALSE THEN TRUE
						ELSE
							CASE
							WHEN w5.count_gids > 1 THEN TRUE
							ELSE FALSE
							END
					END AS summarization
				FROM
					w5
				)	
			SELECT
				array_agg(w6.estimation_cell ORDER BY w6.estimation_cell),
				array_agg(w6.summarization ORDER BY w6.estimation_cell)
			FROM
				w6
			INTO
				_res_estimation_cell_summarization,
				_summarization;
			--------------------------------------------------------------------------------------------
			_q := 	'
				WITH
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------
				-- tato cast je plny (i s duplicitama) seznam gidu zakladnich stavebnich jednotek,
				-- ktery tvori jednotlive pozadovane _estimation_cell[]
				w1 AS	(
					SELECT
						$11 AS estimation_cell,	-- jde o pole poli => identifikace estimation_cell ze vstupu teto funkce [muze obsahovat estimation_cell i ZSJ]
						$2 AS gid		-- jde o pole poli => seznam gidu (ZSJ) tvorici danou estimation_cell
					),
				w2 AS 	(
					SELECT
						unnest(w1.estimation_cell) AS estimation_cell,
						unnest(w1.gid) AS gid
					FROM
						w1
					),
				w3 AS	(SELECT * FROM w2 WHERE gid IS DISTINCT FROM 0),	-- zde je roz-unnestovan with w1 a odstraneny nulove radky
				---------------------------------------------------------------------------
				-- pripojeni informace summarization k estimation_cell
				w4 AS	(
					SELECT
						unnest($5) AS estimation_cell_summarization,
						unnest($6) AS summarization
					),
				w5 AS	(
					SELECT
						w3.estimation_cell,
						w3.gid,
						w4.summarization
					FROM
						w3 LEFT JOIN w4 ON w3.estimation_cell = w4.estimation_cell_summarization
					),
				---------------------------------------------------------------------------
				-- tato cast je vyber zakladnich stavebnich jednotek (gidu), ktere jsou
				-- doposud vypocitany v t_aux_total a odpovidaji verzi, ktera spada do povolenoho intervalu VERZI
				-- Ty, ktere neodpovidaji se budou pocitat znovu !!!
				w9a AS 	(
					SELECT
					tat.*,
					max(tat.est_date) OVER (PARTITION BY tat.cell, tat.config) as max_est_date
					FROM @extschema@.t_aux_total AS tat
					WHERE tat.config = $3
					AND tat.cell IS NOT NULL					-- ve sloupci cell se ukladaji jen gidy nejnizsi urovne z f_a_cell 
					AND tat.cell IN (SELECT DISTINCT gid FROM w3)
					AND tat.ext_version >= $12
					AND tat.ext_version <= $13		
					),
				w9 AS	(
					SELECT w9a.* FROM w9a WHERE w9a.est_date = w9a.max_est_date				
					),
				---------------------------------------------------------------------------
				-- tato cast je pripojeni withu w9 k withu w5
				w10 AS	(
					SELECT
						w5.estimation_cell,
						w5.gid,			-- gid nejnizsi urovne, ktery tvori danou estimation_cell
						w5.summarization,			
						w9.cell			-- gid nejsizsi urovne, ktery je doposud vypocitan v t_aux_total
					FROM
						w5
					LEFT
					JOIN	w9	ON w5.gid = w9.cell
					),
				---------------------------------------------------------------------------
				-- tato cast rika, zda gid nejnizsi urovne je pro danou estimation_cell[i] jiz vypocitan nebo nikoliv
				-- pro danou estimation_cell => gidy s calculated vsude TRUE		=> vsechny pozadovane gidy uz jsou v DB vypocitany
				-- pro danou estimation_cell => gidy s calculated vsude FALSE		=> zadny z pozadovanych gidu jeste neni v DB vypocitan
				-- pro danou estimation_cell => gidy s calculated TRUE nebo FALSE	=> z pozadovanych gidu uz nektere v DB vypocitany jsou a nektere ne
				w11 AS	(
					SELECT
						$3 AS config_id,
						w10.estimation_cell,			-- vstupni hodnota i-te estimation_cell
						w10.gid,				-- gid nejnizsi urovne tvorici danou i-tou estimation_cell
						w10.summarization,

						CASE
						WHEN w10.cell IS NOT NULL THEN TRUE	-- toto plati je-li gid nejnizsi urovne pro danou i-tou estimation_cell je jiz vypocitan
						ELSE FALSE				-- toto plati je-li gid nejnizsi urovne pro danou i-tou estimation_cell neni jeste vypocitan
						END AS calculated
					FROM
						w10
					),
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------
				-- vyber zakladnich stavebnich jednotek pro vypocet
				w12 AS	(SELECT DISTINCT gid FROM w11 WHERE calculated = FALSE ORDER BY gid),
				w13 AS	(
					SELECT
						1 AS step,
						$3 AS config_id,
						fac.estimation_cell,
						w12.gid
					FROM
						w12
					---------------------------------------------------------------------
					LEFT JOIN	@extschema@.f_a_cell AS fac ON w12.gid = fac.gid	-- toto doplnuje zpatky estimation_cell [v aplikaci by se mohlo spojit LABEL a GID]
					---------------------------------------------------------------------
					ORDER
						BY w12.gid
					),
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------
				-- ted reseni SUMARIZACE => bude se provadet vzdy
				---------------------------------------------------------------------------
				w14 AS	(SELECT DISTINCT w11.estimation_cell FROM w11 WHERE summarization = TRUE),
				w15a AS	( -- napojeni na DB
					SELECT
						tat.*,
						max(tat.est_date) OVER (PARTITION BY tat.estimation_cell, tat.config) as max_est_date
					FROM
						@extschema@.t_aux_total AS tat
					WHERE
						tat.config = $3
					AND
						tat.cell IS NULL
					AND
						tat.estimation_cell IN (SELECT w14.estimation_cell FROM w14)
					AND
						tat.ext_version >= $12
					AND
						tat.ext_version <= $13
					),
				w15 AS	(
					select w15a.* FROM w15a WHERE w15a.est_date = w15a.max_est_date				
					),
				w16 AS	(
					SELECT
						w14.estimation_cell,
						CASE
						WHEN w15.estimation_cell IS NOT NULL THEN TRUE	-- estimation_cell, ktera uz je v DB sumarizovana a jeji verze odpovida (spada) do povoleneho intervalu VERZI
						ELSE FALSE					-- estimation_cell, kterou je nutno do-sumarizovat
						END AS calculated
					FROM
						w14
					LEFT
					JOIN	w15	ON w14.estimation_cell = w15.estimation_cell
					),
				w17 AS	(
					SELECT
						2 AS step,
						$3 AS config_id,
						w16.estimation_cell,
						0::integer AS gid
					FROM
						w16
					WHERE
						w16.calculated = FALSE
					
					ORDER
						BY w16.estimation_cell
					)
				---------------------------------------------------------------------------
				---------------------------------------------------------------------------	
				SELECT * FROM w13	-- => VZDY NUTNY DOPLNEK
				UNION ALL
				SELECT * FROM w17	-- => VZDY NUTNA SUMMARIZACE
				---------------------------------------------------------------------------			
				';
		ELSE
			IF _config_query = 500
			THEN
				_q :=	'
					WITH
					w1 AS	(
						SELECT
							1 AS step,
							$3 AS config_id,
							NULL::integer AS estimation_cell,
							NULL::integer AS gid
						)
					SELECT
						w1.step,
						w1.config_id,
						w1.estimation_cell,
						w1.gid
					FROM
						w1 WHERE w1.step = 0;
					';
			ELSE
				-- vetev pro config_query [200,300,400] => soucet a doplnky

				_q :=	'
					WITH
					w1 AS	(SELECT unnest($2) AS gids),	-- jde o seznam gidu (ZSJ), ktere tvori vstupni estimation_cell [vyskyt duplicitnich gidu]
					w2 AS	(SELECT DISTINCT gids FROM w1),	-- z-unikatneni gidu (ZSJ)
					w3 AS	(				-- vyber zaznamu z f_a_cell pro unikatni gidy (ZSJ)
						SELECT
							fac.gid,
							fac.estimation_cell
						FROM
							@extschema@.f_a_cell AS fac
						WHERE
							fac.gid IN (SELECT gids FROM w2)
						),
					w4 AS	(				-- zjisteni poctu gidu tvorici estimation_cell
						SELECT
							w3.estimation_cell,
							count(w3.gid) AS pocet
						FROM
							w3 GROUP BY w3.estimation_cell
						),
					w5 AS	(				-- vyber ZSJ, ktere tvori estimation_cell jen jednou geometrii
						SELECT w4.estimation_cell FROM w4
						WHERE w4.pocet = 1
						),
					w6 AS	(SELECT unnest($4) AS estimation_cell),	-- roz-unnestovani vstupnich estimation_cell
					w7 AS	(					-- odstraneni vstupnich estimation_cell z w5.estimation_cell
						SELECT w5.estimation_cell FROM w5 EXCEPT
						SELECT w6.estimation_cell FROM w6	-- vysledkem jsou estimation_cell (ZSJ) pro DOPLNENI
						),
					w8a AS	(				-- zjisteni gidu (ZSJ) pres estimation_cell co je opravdu v DB pro danou ext_version_current
						SELECT				-- u (ZSJ) je estimation_cell vzdy vyplneno pokud je estimation_cell tvoreno jednou geometrii
							tat.*,
							max(tat.est_date) OVER (PARTITION BY tat.estimation_cell, tat.config) as max_est_date
						FROM
							@extschema@.t_aux_total AS tat
						WHERE
							config = $3
						AND
							tat.ext_version = $12
						AND
							tat.ext_version = $13
						AND
							tat.estimation_cell IN (SELECT w7.estimation_cell FROM w7)
						),
					w8 AS	(
						SELECT w8a.* FROM w8a WHERE w8a.est_date = w8a.max_est_date
						),						
					w9 AS	(
						SELECT
							$3 AS config_id,
							w7.estimation_cell,
						
							CASE
							WHEN w8.estimation_cell IS NOT NULL THEN TRUE	-- toto plati je-li estimation_cell nejnizsi urovne pro danou ext_version_current => jiz vypocitano
							ELSE FALSE					-- toto plati je-li estimation_cell nejnizsi urovne pro danou ext_version_current => neni jeste vypocitano
							END AS calculated
						FROM
							w7 LEFT JOIN w8 ON w7.estimation_cell = w8.estimation_cell					
						),
					w10 AS	(				-- 1. cast do UNIONu
						SELECT
							1 AS step,
							w9.config_id,
							w9.estimation_cell,
							NULL::integer AS gid
						FROM
							w9 WHERE w9.calculated = FALSE
						ORDER
							BY w9.estimation_cell
						),
					-------------------------------------------------
					w11 AS	(
						SELECT
							2 AS step,
							$3 AS config_id,
							$4 AS estimation_cell,
							NULL::integer AS gid
						),
					w12 AS	(	
						SELECT
							w11.step,
							w11.config_id,
							unnest(w11.estimation_cell) AS estimation_cell,
							w11.gid
						FROM
							w11
						),
					w13a AS	( -- napojeni na DB
						SELECT
							tat.*,
							max(tat.est_date) OVER (PARTITION BY tat.estimation_cell, tat.config) as max_est_date
						FROM
							@extschema@.t_aux_total AS tat
						WHERE
							tat.config = $3
						AND
							tat.cell IS NULL
						AND
							tat.estimation_cell IN (SELECT w12.estimation_cell FROM w12)
						AND
							tat.ext_version >= $12
						AND
							tat.ext_version <= $13
						),
					w13 AS	(
						SELECT w13a.* FROM w13a WHERE w13a.est_date = w13a.max_est_date
						),
					w14 AS	(
						SELECT
							w12.step,
							w12.config_id,
							w12.estimation_cell,
							w12.gid,
							CASE
							WHEN w13.estimation_cell IS NOT NULL THEN TRUE	-- estimation_cell, ktera uz je v DB sumarizovana ve verzi spadajici do povoleneho intervalu VERZI
							ELSE FALSE					-- estimation_cell, kterou je nutno do-sumarizovat
							END AS calculated
						FROM
							w12
						LEFT
						JOIN	w13	ON w12.estimation_cell = w13.estimation_cell
						),						
					w15 AS	(				-- 2. cast do UNIONu
						SELECT
							w14.step,
							w14.config_id,
							w14.estimation_cell,
							w14.gid
						FROM
							w14
						WHERE
							w14.calculated = FALSE
						ORDER
							BY w14.estimation_cell
						)
					-------------------------------------------------------
					SELECT w10.* FROM w10 UNION ALL
					SELECT w15.* FROM w15
					';
			END IF;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||''
	USING
		_res_estimation_cell,				-- $1
		_res_gids,					-- $2
		_config_id,					-- $3
		_estimation_cell,				-- $4
		_res_estimation_cell_summarization,		-- $5
		_summarization,					-- $6
		_ext_version_current,				-- $7
		_res_gids_prepocet_4_recount,			-- $8
		_res_estimation_cell_prepocet_4_summarization,	-- $9
		_res_estimation_cell_prepocet_4_check,		-- $10
		_res_estimation_cell_4_with,			-- $11
		_ext_version_valid_from,			-- $12
		_ext_version_valid_until;			-- $13
	--------------------------------------------------------------------------------------------	
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


ALTER FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_gids4aux_total_app(integer,integer[],integer,boolean) IS
'Funkce vraci seznam vypocetnich udaju pro funkci fn_get_aux_total_app.';
-- </function>
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- <function name="fn_sql_aux_total_raster_app" schema="@extschema@" src="functions/extschema/fn_sql_aux_total_raster_app.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------;
--	fn_sql_aux_total_raster_app
--------------------------------------------------------------------------------;

DROP FUNCTION IF EXISTS @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_total_raster_app
(
	config_id			integer,
	schema_name			character varying,
	table_name			character varying,
	column_name			character varying,
	band				integer,
	reclass				integer,
	condition			character varying,
	unit				double precision,
	gid				character varying
)
RETURNS text
LANGUAGE plpgsql
IMMUTABLE
SECURITY INVOKER
AS
$$
DECLARE
	reclass_text		text;
	result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_total_raster_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_total_raster_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_name
	IF ((column_name IS NULL) OR (trim(column_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_total_raster_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- band
	IF (band IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_total_raster_app: Hodnota parametru band nesmí být NULL.';
	END IF;

	-- condition [povoleno NULL]
	IF ((condition IS NULL) OR (trim(condition) = ''))
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_total_raster_app: Hodnota parametru unit nesmí být NULL.';
	END IF;

	-- gid
	IF (gid IS NULL) OR (trim(gid) = '')
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_total_raster_app: Hodnota parametru gid nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro vypocet uhrnu
	result :=
		'
		WITH 
		---------------------------------------------------------------
		w_rast_1 AS	(
				SELECT
					t1.gid, 
					t1.estimation_cell,
					t1.geom,
					CASE WHEN t2.#COLUMN_NAME# IS NULL THEN false ELSE true END AS ident_null,
					#RECLASS# AS rast_reclass,
					t2.*
				FROM
					(SELECT * FROM @extschema@.f_a_cell WHERE gid = #GID#) AS t1
				LEFT JOIN
					#SCHEMA_NAME#.#TABLE_NAME# AS t2
				ON
					t1.geom && ST_Convexhull(t2.#COLUMN_NAME#)
				AND
					ST_Intersects(t1.geom, ST_Convexhull(t2.#COLUMN_NAME#))
				)
		---------------------------------------------------------------		
		,w_rast AS	(
				SELECT
					gid,
					estimation_cell,
					geom,
					ident_null,
					rast_reclass AS rast
				FROM
					w_rast_1
				WHERE
					#CONDITION#
				)
		---------------------------------------------------------------
		,w_covers AS	(
				SELECT
					gid,
					estimation_cell,
					rast,
					geom,
					ST_Covers(geom, ST_Convexhull(rast)) AS covers
				FROM
					w_rast
				WHERE
					ident_null
				)
		---------------------------------------------------------------	
		,w_uhrn AS 	(
				SELECT
					gid,
					estimation_cell,
					(ST_ValueCount(rast, #BAND#, true)) AS val_count,
					(ST_PixelWidth(rast) * ST_PixelHeight(rast))*#UNIT# AS pixarea
				FROM
					w_covers
				WHERE
					covers = true
				)
		---------------------------------------------------------------	
		,w_uhrn_not AS	(
				SELECT
					gid,
					estimation_cell,
					ST_Intersection(geom, rast, #BAND#) AS vector_intersect_record
				FROM
					w_covers
				WHERE
					covers = false
				)
		---------------------------------------------------------------	
		,w_sum AS	(
				SELECT
					SUM(((ST_Area((vector_intersect_record).geom))*#UNIT#)*((vector_intersect_record).val)) AS sum_part
				FROM 
					w_uhrn_not
				---------------------------
				UNION ALL
				---------------------------
				SELECT
					SUM((val_count).value*(val_count).count*pixarea) AS sum_part
				FROM 
					w_uhrn 
				---------------------------
				UNION ALL
				---------------------------
				SELECT
					0.0 AS sum_part
				FROM
					w_rast
				WHERE
					ident_null = false
				)
		---------------------------------------------------------------	
		SELECT
			coalesce(sum(sum_part),0) AS aux_total_#CONFIG_ID#_#GID#
		FROM 
			w_sum;			
		';
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace
	IF (reclass IS NULL)
	THEN
		reclass_text := 't2.#COLUMN_NAME#';
	ELSE
		reclass_text := '@extschema@.fn_get_reclass_app(t2.#COLUMN_NAME#,#BAND#,ST_BandPixelType(t2.#COLUMN_NAME#,#BAND#),#RECLASS_VALUE#)';
	END IF;
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	result := replace(result, '#RECLASS#', reclass_text);
	result := replace(result, '#CONFIG_ID#', config_id::character varying);
	result := replace(result, '#SCHEMA_NAME#', schema_name);
	result := replace(result, '#TABLE_NAME#', table_name);
	result := replace(result, '#COLUMN_NAME#', column_name);
	result := replace(result, '#BAND#', band::character varying);
	result := replace(result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	result := replace(result, '#UNIT#', unit::character varying);
	result := replace(result, '#GID#', gid);

	IF (reclass IS NOT NULL)
	THEN
		result := replace(result, '#RECLASS_VALUE#', reclass::character varying);
	END IF;
	-----------------------------------------------------------------------------------
	RETURN result;
	-----------------------------------------------------------------------------------
END;
$$;

--------------------------------------------------------------------------------;

ALTER FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
TO app_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
IS
'Funkce vrací SQL textový řetězec pro výpočet úhrnu pomocné proměnné z rasterové vrstvy.';

--------------------------------------------------------------------------------;

-- </function>
------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------



---------------------------------------------------------------------------------------------------;
-- create triggers
---------------------------------------------------------------------------------------------------;
CREATE TRIGGER trg__cm_ext_config_function__after_insert
  AFTER INSERT
  ON @extschema@.cm_ext_config_function
  FOR EACH ROW
  EXECUTE PROCEDURE @extschema@.fn_cm_ext_config_function_after_insert_app();
  
COMMENT ON TRIGGER trg__cm_ext_config_function__after_insert ON @extschema@.cm_ext_config_function IS
'Trigger.';

CREATE TRIGGER trg__cm_ext_config_function__after_update
  AFTER UPDATE
  ON @extschema@.cm_ext_config_function
  FOR EACH ROW
  EXECUTE PROCEDURE @extschema@.fn_cm_ext_config_function_after_update_app();
  
COMMENT ON TRIGGER trg__cm_ext_config_function__after_update ON @extschema@.cm_ext_config_function IS
'Trigger.';
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- btree index for est_date in table t_aux_total and t_auxiliary_data
---------------------------------------------------------------------------------------------------;
CREATE INDEX idx__t_aux_total__est_date
  ON @extschema@.t_aux_total
  USING btree
  (est_date);
COMMENT ON INDEX @extschema@.idx__t_aux_total__est_date
  IS 'B-tree index on column est_date.';

CREATE INDEX idx__t_auxiliary_data__est_date
  ON @extschema@.t_auxiliary_data
  USING btree
  (est_date);
COMMENT ON INDEX @extschema@.idx__t_auxiliary_data__est_date
  IS 'B-tree index on column est_date.';
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- dumps for tables
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_estimation_cell_collection', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_ext_config_function', '');
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- dumps for sequences
---------------------------------------------------------------------------------------------------;
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_estimation_cell_collection_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_ext_config_function_id_seq', '');
---------------------------------------------------------------------------------------------------;


--------------------------------------------------------------------------------;
-- setting public privileges for sequences
--------------------------------------------------------------------------------;
GRANT ALL ON SEQUENCE @extschema@.cm_ext_config_function_id_seq TO app_nfiesta_gisdata;
--------------------------------------------------------------------------------;

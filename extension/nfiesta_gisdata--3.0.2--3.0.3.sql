--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------
-- schema export_api views
--------------------------------------------------------

-- <view name="estimation_cell_hierarchy" schema="export_api" src="views/export_api/estimation_cell_hierarchy.sql">
-- </view>

--------------------------------------------------------------------------------;
-- add new version into c_ext_version
-- shift function version valid until
--------------------------------------------------------------------------------;

	INSERT INTO @extschema@.c_ext_version(id, label, description)
	VALUES
		(1500,'3.0.3','Version 3.0.3 - estimation_cell_hierarchy changed ids in return to labels from estimation cells.');

	UPDATE @extschema@.cm_ext_config_function
	SET ext_version_valid_until = 1500
	WHERE ext_version_valid_until = 1400;
--------------------------------------------------------------------------------;


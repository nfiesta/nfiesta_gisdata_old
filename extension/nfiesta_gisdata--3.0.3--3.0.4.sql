--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------
-- schema export_api views
--------------------------------------------------------
drop view export_api.estimation_cell_hierarchy;

-- <view name="estimation_cell_hierarchy" schema="export_api" src="views/export_api/estimation_cell_hierarchy.sql">
--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DDL
create or replace view export_api.estimation_cell_hierarchy as
	with w_data as (
	select
		c_estimation_cell.id as c_estimation_cell__id,
		c_estimation_cell.label as c_estimation_cell__label,
		c_estimation_cell_sup.id as c_estimation_cell_sup__id,
		c_estimation_cell_sup.label as c_estimation_cell_sup__label,
		c_estimation_cell_sup.estimation_cell_collection as c_estimation_cell_sup__estimation_cell_collection
	from @extschema@.cm_f_a_cell
	inner join @extschema@.f_a_cell 						on cm_f_a_cell.cell = f_a_cell.gid
	inner join @extschema@.c_estimation_cell 					ON c_estimation_cell.id = f_a_cell.estimation_cell
	inner join @extschema@.f_a_cell 		as f_a_cell_sup 		on cm_f_a_cell.cell_sup = f_a_cell_sup.gid
	inner join @extschema@.c_estimation_cell 	as c_estimation_cell_sup 	ON c_estimation_cell_sup.id = f_a_cell_sup.estimation_cell
	group by 
		c_estimation_cell.id, 
		c_estimation_cell.label, 
		c_estimation_cell_sup.id, 
		c_estimation_cell_sup.label, 
		c_estimation_cell_sup.estimation_cell_collection
	)
	select 
		c_estimation_cell__label as cell,
		/*c_estimation_cell__label,
		c_estimation_cell_sup__estimation_cell_collection,
		array_agg(c_estimation_cell_sup__id) as agg__c_estimation_cell_sup__id,
		array_agg(c_estimation_cell_sup__label) as agg__c_estimation_cell_sup__label,*/
		(array_agg(c_estimation_cell_sup__label))[1] as cell_superior
	from w_data
	group by 
		c_estimation_cell__id, 
		c_estimation_cell__label, 
		c_estimation_cell_sup__estimation_cell_collection
	having count(*) = 1
	order by c_estimation_cell__id, c_estimation_cell_sup__estimation_cell_collection
;

-- authorization
ALTER TABLE export_api.estimation_cell_hierarchy OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.estimation_cell_hierarchy TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.estimation_cell_hierarchy TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.estimation_cell_hierarchy TO public;

-- documentation
COMMENT ON VIEW export_api.estimation_cell_hierarchy IS 'View providing hierarchy of estimation cells to nfi_esta ETL process.';
COMMENT ON COLUMN export_api.estimation_cell_hierarchy.cell IS 'Foreign key to c_estimation_cell.';
COMMENT ON COLUMN export_api.estimation_cell_hierarchy.cell_superior IS 'Foreign key to superior c_estimation_cell.';


-- </view>

--------------------------------------------------------------------------------;
-- add new version into c_ext_version
-- shift function version valid until
--------------------------------------------------------------------------------;

	INSERT INTO @extschema@.c_ext_version(id, label, description)
	VALUES
		(1600,'3.0.4','Version 3.0.4 - corrected changeskript 3.0.3, where the .sql of the view was forgotten to add.');

	UPDATE @extschema@.cm_ext_config_function
	SET ext_version_valid_until = 1600
	WHERE ext_version_valid_until = 1500;
--------------------------------------------------------------------------------;


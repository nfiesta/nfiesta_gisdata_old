--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance WITH the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes WITHin any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

WITH
w1 AS 	(SELECT * FROM gisdata.fn_get_configs4aux_data_app(108,(SELECT max(id) FROM gisdata.c_gui_version),0,false)),
w2 AS 	(
	SELECT
	(gisdata.fn_get_aux_data_app(config_collection,config,intersects,gid_start,gid_end)) AS res
	FROM w1 WHERE step = 1 ORDER BY config_collection, config
	)
INSERT INTO gisdata.t_auxiliary_data(config_collection,config,value,ext_version,gui_version,gid)
SELECT
	(res).config_collection,
	(res).config,
	(res).value,
	(res).ext_version,
	(SELECT max(id) FROM gisdata.c_gui_version) AS gui_version,
	(res).gid
FROM
	w2 ORDER BY config_collection, config, gid
;
WITH
w1 AS 	(SELECT * FROM gisdata.fn_get_configs4aux_data_app(108,(SELECT max(id) FROM gisdata.c_gui_version),0,false)),
w2 AS 	(
	SELECT
	(gisdata.fn_get_aux_data_app(config_collection,config,intersects,gid_start,gid_end)) AS res
	FROM w1 WHERE step = 2 ORDER BY config_collection, config
	)
INSERT INTO gisdata.t_auxiliary_data(config_collection,config,value,ext_version,gui_version,gid)
SELECT
	(res).config_collection,
	(res).config,
	(res).value,
	(res).ext_version,
	(SELECT max(id) FROM gisdata.c_gui_version) AS gui_version,
	(res).gid
FROM
	w2 ORDER BY config_collection, config, gid
;
WITH
w1 AS 	(SELECT * FROM gisdata.fn_get_configs4aux_data_app(108,(SELECT max(id) FROM gisdata.c_gui_version),0,false)),
w2 AS 	(
	SELECT
	(gisdata.fn_get_aux_data_app(config_collection,config,intersects,gid_start,gid_end)) AS res
	FROM w1 WHERE step = 3 ORDER BY config_collection, config
	)
INSERT INTO gisdata.t_auxiliary_data(config_collection,config,value,ext_version,gui_version,gid)
SELECT
	(res).config_collection,
	(res).config,
	(res).value,
	(res).ext_version,
	(SELECT max(id) FROM gisdata.c_gui_version) AS gui_version,
	(res).gid
FROM
	w2 ORDER BY config_collection, config, gid
;
WITH
w1 AS 	(SELECT * FROM gisdata.fn_get_configs4aux_data_app(109,(SELECT max(id) FROM gisdata.c_gui_version),0,false)),
w2 AS 	(
	SELECT
	(gisdata.fn_get_aux_data_app(config_collection,config,intersects,gid_start,gid_end)) AS res
	FROM w1 WHERE step = 1 ORDER BY config_collection, config
	)
INSERT INTO gisdata.t_auxiliary_data(config_collection,config,value,ext_version,gui_version,gid)
SELECT
	(res).config_collection,
	(res).config,
	(res).value,
	(res).ext_version,
	(SELECT max(id) FROM gisdata.c_gui_version) AS gui_version,
	(res).gid
FROM
	w2 ORDER BY config_collection, config, gid
;
WITH
w1 AS 	(SELECT * FROM gisdata.fn_get_configs4aux_data_app(109,(SELECT max(id) FROM gisdata.c_gui_version),0,false)),
w2 AS 	(
	SELECT
	(gisdata.fn_get_aux_data_app(config_collection,config,intersects,gid_start,gid_end)) AS res
	FROM w1 WHERE step = 2 ORDER BY config_collection, config
	)
INSERT INTO gisdata.t_auxiliary_data(config_collection,config,value,ext_version,gui_version,gid)
SELECT
	(res).config_collection,
	(res).config,
	(res).value,
	(res).ext_version,
	(SELECT max(id) FROM gisdata.c_gui_version) AS gui_version,
	(res).gid
FROM
	w2 ORDER BY config_collection, config, gid
;
WITH
w1 AS 	(SELECT * FROM gisdata.fn_get_configs4aux_data_app(109,(SELECT max(id) FROM gisdata.c_gui_version),0,false)),
w2 AS 	(
	SELECT
	(gisdata.fn_get_aux_data_app(config_collection,config,intersects,gid_start,gid_end)) AS res
	FROM w1 WHERE step = 3 ORDER BY config_collection, config
	)
INSERT INTO gisdata.t_auxiliary_data(config_collection,config,value,ext_version,gui_version,gid)
SELECT
	(res).config_collection,
	(res).config,
	(res).value,
	(res).ext_version,
	(SELECT max(id) FROM gisdata.c_gui_version) AS gui_version,
	(res).gid
FROM
	w2 ORDER BY config_collection, config, gid
;
WITH
w1 AS 	(SELECT * FROM gisdata.fn_get_configs4aux_data_app(109,(SELECT max(id) FROM gisdata.c_gui_version),0,false)),
w2 AS 	(
	SELECT
	(gisdata.fn_get_aux_data_app(config_collection,config,intersects,gid_start,gid_end)) AS res
	FROM w1 WHERE step = 4 ORDER BY config_collection, config
	)
INSERT INTO gisdata.t_auxiliary_data(config_collection,config,value,ext_version,gui_version,gid)
SELECT
	(res).config_collection,
	(res).config,
	(res).value,
	(res).ext_version,
	(SELECT max(id) FROM gisdata.c_gui_version) AS gui_version,
	(res).gid
FROM
	w2 ORDER BY config_collection, config, gid
;
WITH
w1 AS 	(SELECT * FROM gisdata.fn_get_configs4aux_data_app(109,(SELECT max(id) FROM gisdata.c_gui_version),0,false)),
w2 AS 	(
	SELECT
	(gisdata.fn_get_aux_data_app(config_collection,config,intersects,gid_start,gid_end)) AS res
	FROM w1 WHERE step = 5 ORDER BY config_collection, config
	)
INSERT INTO gisdata.t_auxiliary_data(config_collection,config,value,ext_version,gui_version,gid)
SELECT
	(res).config_collection,
	(res).config,
	(res).value,
	(res).ext_version,
	(SELECT max(id) FROM gisdata.c_gui_version) AS gui_version,
	(res).gid
FROM
	w2 ORDER BY config_collection, config, gid
;
WITH
w1 AS 	(SELECT * FROM gisdata.fn_get_configs4aux_data_app(109,(SELECT max(id) FROM gisdata.c_gui_version),0,false)),
w2 AS 	(
	SELECT
	(gisdata.fn_get_aux_data_app(config_collection,config,intersects,gid_start,gid_end)) AS res
	FROM w1 WHERE step = 6 ORDER BY config_collection, config
	)
INSERT INTO gisdata.t_auxiliary_data(config_collection,config,value,ext_version,gui_version,gid)
SELECT
	(res).config_collection,
	(res).config,
	(res).value,
	(res).ext_version,
	(SELECT max(id) FROM gisdata.c_gui_version) AS gui_version,
	(res).gid
FROM
	w2 ORDER BY config_collection, config, gid
;
WITH
w AS	(
	SELECT gid,config,value
	FROM gisdata.t_auxiliary_data
	WHERE config_collection in (108,109)
	)
SELECT
	f_p_plot.country,
	f_p_plot.strata_set,
	f_p_plot.stratum,
	f_p_plot.panel,
	f_p_plot.cluster,
	f_p_plot.plot,
	t_config.label AS auxiliary_variable_category,
	w.value
FROM w
INNER JOIN gisdata.f_p_plot ON w.gid = f_p_plot.gid
INNER JOIN gisdata.t_config ON w.config = t_config.id
ORDER BY country, strata_set, stratum, panel, cluster, plot, auxiliary_variable_category
;
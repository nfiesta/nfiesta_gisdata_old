--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

\set srcdir `echo $SRC_DIR`
--
-- PostgreSQL database dump
--

--
-- Data for Name: c_estimation_cell_collection; Type: TABLE DATA; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

INSERT INTO gisdata.c_estimation_cell_collection(id,label,description,label_en,description_en) VALUES
(1,	'NFR'			,'Natural Forest Regions','NFR'			,'Natural Forest Regions'),
(2,	'NFRD'			,'Natural Forest Region Districts','NFRD'			,'Natural Forest Region Districts'),
(3,	'INSPIRE 50x50km'	,'INSPIRE grid 50x50km','INSPIRE 50x50km'	,'INSPIRE grid 50x50km'),
(4,	'INSPIRE 25x25km'	,'INSPIRE grid 25x25km','INSPIRE 25x25km'	,'INSPIRE grid 25x25km');


--
-- Data for Name: c_estimation_cell; Type: TABLE DATA; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

INSERT INTO gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en) VALUES
(1	,1	,'NFR16'		,'NFR16 - Českomoravská vrchovina','',''),
(2	,1	,'NFR27'		,'NFR27 - Hrubý Jeseník','',''),
(3	,1	,'NFR28'		,'NFR28 - Předhoří Hrubého Jeseníku','',''),
(4	,1	,'NFR29'		,'NFR29 - Nízký Jeseník','',''),
(5	,1	,'NFR30'		,'NFR30 - Drahanská vrchovina','',''),
(6	,1	,'NFR31'		,'NFR31 - Českomoravské mezihoří','',''),
(7	,1	,'NFR32'		,'NFR32 - Slezská nížina','',''),
(8	,1	,'NFR33'		,'NFR33 - Předhoří Českomoravské vrchoviny','',''),
(9	,1	,'NFR34'		,'NFR34 - Hornomoravský úval','',''),
(10	,1	,'NFR35'		,'NFR35 - Jihomoravské úvaly','',''),
(11	,1	,'NFR36'		,'NFR36 - Středomoravské Karpaty','',''),
(12	,1	,'NFR37'		,'NFR37 - Kelečská pahorkatina','',''),
(13	,1	,'NFR38'		,'NFR38 - Bílé Karpaty a Vizovické vrchy','',''),
(14	,1	,'NFR39'		,'NFR39 - Podbeskydská pahorkatina','',''),
(15	,1	,'NFR40'		,'NFR40 - Moravskoslezské Beskydy','',''),
(16	,1	,'NFR41'		,'NFR41 - Hostýnské a Vsetínské vrchy a Javorníky','',''),
(17	,3	,'50kmE460N290'		,'50kmE460N290','',''),
(18	,3	,'50kmE460N295'		,'50kmE460N295','',''),
(19	,3	,'50kmE465N285'		,'50kmE465N285','',''),
(20	,3	,'50kmE465N290'		,'50kmE465N290','',''),
(21	,3	,'50kmE465N295'		,'50kmE465N295','',''),
(22	,3	,'50kmE470N285'		,'50kmE470N285','',''),
(23	,3	,'50kmE470N290'		,'50kmE470N290','',''),
(24	,3	,'50kmE470N295'		,'50kmE470N295','',''),
(25	,3	,'50kmE470N300'		,'50kmE470N300','',''),
(26	,3	,'50kmE475N285'		,'50kmE475N285','',''),
(27	,3	,'50kmE475N290'		,'50kmE475N290','',''),
(28	,3	,'50kmE475N295'		,'50kmE475N295','',''),
(29	,3	,'50kmE475N300'		,'50kmE475N300','',''),
(30	,3	,'50kmE480N285'		,'50kmE480N285','',''),
(31	,3	,'50kmE480N290'		,'50kmE480N290','',''),
(32	,3	,'50kmE480N295'		,'50kmE480N295','',''),
(33	,3	,'50kmE480N300'		,'50kmE480N300','',''),
(34	,3	,'50kmE480N305'		,'50kmE480N305','',''),
(35	,3	,'50kmE485N285'		,'50kmE485N285','',''),
(36	,3	,'50kmE485N290'		,'50kmE485N290','',''),
(37	,3	,'50kmE485N295'		,'50kmE485N295','',''),
(38	,3	,'50kmE485N300'		,'50kmE485N300','',''),
(39	,3	,'50kmE485N305'		,'50kmE485N305','',''),
(40	,3	,'50kmE490N290'		,'50kmE490N290','',''),
(41	,3	,'50kmE490N295'		,'50kmE490N295','',''),
(42	,3	,'50kmE490N300'		,'50kmE490N300','',''),
(43	,3	,'50kmE495N295'		,'50kmE495N295','',''),
(44	,4	,'25kmE4625N2925'	,'25kmE4625N2925','',''),
(45	,4	,'25kmE4625N2950'	,'25kmE4625N2950','',''),
(46	,4	,'25kmE4650N2900'	,'25kmE4650N2900','',''),
(47	,4	,'25kmE4650N2925'	,'25kmE4650N2925','',''),
(48	,4	,'25kmE4650N2950'	,'25kmE4650N2950','',''),
(49	,4	,'25kmE4675N2875'	,'25kmE4675N2875','',''),
(50	,4	,'25kmE4675N2900'	,'25kmE4675N2900','',''),
(51	,4	,'25kmE4675N2925'	,'25kmE4675N2925','',''),
(52	,4	,'25kmE4675N2950'	,'25kmE4675N2950','',''),
(53	,4	,'25kmE4675N2975'	,'25kmE4675N2975','',''),
(54	,4	,'25kmE4700N2875'	,'25kmE4700N2875','',''),
(55	,4	,'25kmE4700N2900'	,'25kmE4700N2900','',''),
(56	,4	,'25kmE4700N2925'	,'25kmE4700N2925','',''),
(57	,4	,'25kmE4700N2950'	,'25kmE4700N2950','',''),
(58	,4	,'25kmE4700N2975'	,'25kmE4700N2975','',''),
(59	,4	,'25kmE4725N2875'	,'25kmE4725N2875','',''),
(60	,4	,'25kmE4725N2900'	,'25kmE4725N2900','',''),
(61	,4	,'25kmE4725N2925'	,'25kmE4725N2925','',''),
(62	,4	,'25kmE4725N2950'	,'25kmE4725N2950','',''),
(63	,4	,'25kmE4725N2975'	,'25kmE4725N2975','',''),
(64	,4	,'25kmE4725N3000'	,'25kmE4725N3000','',''),
(65	,4	,'25kmE4750N2850'	,'25kmE4750N2850','',''),
(66	,4	,'25kmE4750N2875'	,'25kmE4750N2875','',''),
(67	,4	,'25kmE4750N2900'	,'25kmE4750N2900','',''),
(68	,4	,'25kmE4750N2925'	,'25kmE4750N2925','',''),
(69	,4	,'25kmE4750N2950'	,'25kmE4750N2950','',''),
(70	,4	,'25kmE4750N2975'	,'25kmE4750N2975','',''),
(71	,4	,'25kmE4750N3000'	,'25kmE4750N3000','',''),
(72	,4	,'25kmE4775N2850'	,'25kmE4775N2850','',''),
(73	,4	,'25kmE4775N2875'	,'25kmE4775N2875','',''),
(74	,4	,'25kmE4775N2900'	,'25kmE4775N2900','',''),
(75	,4	,'25kmE4775N2925'	,'25kmE4775N2925','',''),
(76	,4	,'25kmE4775N2950'	,'25kmE4775N2950','',''),
(77	,4	,'25kmE4775N2975'	,'25kmE4775N2975','',''),
(78	,4	,'25kmE4775N3000'	,'25kmE4775N3000','',''),
(79	,4	,'25kmE4800N2850'	,'25kmE4800N2850','',''),
(80	,4	,'25kmE4800N2875'	,'25kmE4800N2875','',''),
(81	,4	,'25kmE4800N2900'	,'25kmE4800N2900','',''),
(82	,4	,'25kmE4800N2925'	,'25kmE4800N2925','',''),
(83	,4	,'25kmE4800N2950'	,'25kmE4800N2950','',''),
(84	,4	,'25kmE4800N2975'	,'25kmE4800N2975','',''),
(85	,4	,'25kmE4800N3000'	,'25kmE4800N3000','',''),
(86	,4	,'25kmE4800N3025'	,'25kmE4800N3025','',''),
(87	,4	,'25kmE4800N3050'	,'25kmE4800N3050','',''),
(88	,4	,'25kmE4825N2850'	,'25kmE4825N2850','',''),
(89	,4	,'25kmE4825N2875'	,'25kmE4825N2875','',''),
(90	,4	,'25kmE4825N2900'	,'25kmE4825N2900','',''),
(91	,4	,'25kmE4825N2925'	,'25kmE4825N2925','',''),
(92	,4	,'25kmE4825N2950'	,'25kmE4825N2950','',''),
(93	,4	,'25kmE4825N2975'	,'25kmE4825N2975','',''),
(94	,4	,'25kmE4825N3000'	,'25kmE4825N3000','',''),
(95	,4	,'25kmE4825N3025'	,'25kmE4825N3025','',''),
(96	,4	,'25kmE4825N3050'	,'25kmE4825N3050','',''),
(97	,4	,'25kmE4850N2875'	,'25kmE4850N2875','',''),
(98	,4	,'25kmE4850N2900'	,'25kmE4850N2900','',''),
(99	,4	,'25kmE4850N2925'	,'25kmE4850N2925','',''),
(100	,4	,'25kmE4850N2950'	,'25kmE4850N2950','',''),
(101	,4	,'25kmE4850N2975'	,'25kmE4850N2975','',''),
(102	,4	,'25kmE4850N3000'	,'25kmE4850N3000','',''),
(103	,4	,'25kmE4850N3025'	,'25kmE4850N3025','',''),
(104	,4	,'25kmE4850N3050'	,'25kmE4850N3050','',''),
(105	,4	,'25kmE4875N2875'	,'25kmE4875N2875','',''),
(106	,4	,'25kmE4875N2900'	,'25kmE4875N2900','',''),
(107	,4	,'25kmE4875N2925'	,'25kmE4875N2925','',''),
(108	,4	,'25kmE4875N2950'	,'25kmE4875N2950','',''),
(109	,4	,'25kmE4875N2975'	,'25kmE4875N2975','',''),
(110	,4	,'25kmE4875N3000'	,'25kmE4875N3000','',''),
(111	,4	,'25kmE4875N3025'	,'25kmE4875N3025','',''),
(112	,4	,'25kmE4900N2900'	,'25kmE4900N2900','',''),
(113	,4	,'25kmE4900N2925'	,'25kmE4900N2925','',''),
(114	,4	,'25kmE4900N2950'	,'25kmE4900N2950','',''),
(115	,4	,'25kmE4900N2975'	,'25kmE4900N2975','',''),
(116	,4	,'25kmE4900N3000'	,'25kmE4900N3000','',''),
(117	,4	,'25kmE4925N2925'	,'25kmE4925N2925','',''),
(118	,4	,'25kmE4925N2950'	,'25kmE4925N2950','',''),
(119	,4	,'25kmE4925N2975'	,'25kmE4925N2975','',''),
(120	,4	,'25kmE4925N3000'	,'25kmE4925N3000','',''),
(121	,4	,'25kmE4950N2950'	,'25kmE4950N2950','',''),
(122	,4	,'25kmE4950N2975'	,'25kmE4950N2975','',''),
(123	,2	,'NFRD7'		,'NFRD7 - Českomoravské vrchoviny','',''),
(124	,2	,'NFRD11'		,'NFRD11 - Východosudetský','',''),
(125	,2	,'NFRD12'		,'NFRD12 - Moravského podhůří','',''),
(126	,2	,'NFRD13'		,'NFRD13 - Moravských úvalů','',''),
(127	,2	,'NFRD14'		,'NFRD14 - Karpatský','','');


--
-- Data for Name: cm_estimation_cell_collection; Type: TABLE DATA; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

INSERT INTO gisdata.cm_estimation_cell_collection(id,estimation_cell_collection,estimation_cell_collection_lowest,estimation_cell_collection_highest)
VALUES
(1,1,1,2),
(2,2,1,2),
(3,3,4,3),
(4,4,4,3);


--
-- Data for Name: f_a_cell; Type: TABLE DATA; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

\set afile :srcdir '/csv/f_a_cell.csv'
CREATE FOREIGN TABLE csv.f_a_cell
(
	gid		integer,
	cell_geometry	text,
	estimation_cell	integer
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

INSERT INTO gisdata.f_a_cell(gid,geom,estimation_cell)
SELECT
	gid,
	ST_GeomFromEWKT(cell_geometry) as geom,
	estimation_cell
FROM
	csv.f_a_cell;


--
-- Data for Name: cm_f_a_cell; Type: TABLE DATA; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

\set afile :srcdir '/csv/cm_f_a_cell.csv'
CREATE FOREIGN TABLE csv.cm_f_a_cell
(
	cell		integer,
	cell_sup	integer
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

INSERT INTO gisdata.cm_f_a_cell(cell,cell_sup)
SELECT
	cell,
	cell_sup
FROM
	csv.cm_f_a_cell;


--
-- Name: cm_estimation_cell_collection_id_seq; Type: SEQUENCE SET; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

SELECT pg_catalog.setval('gisdata.cm_estimation_cell_collection_id_seq', (select max(id) from gisdata.cm_estimation_cell_collection), true);


--
-- Name: f_a_cell_gid_seq; Type: SEQUENCE SET; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

SELECT pg_catalog.setval('gisdata.f_a_cell_gid_seq', (select max(gid) from gisdata.f_a_cell), true);


--
-- Data for Name: f_p_plot; Type: TABLE DATA; Schema: gisdata; Owner: adm_nfiesta_gisdata; Info: foreing table must by created before insert datas into table t_config_collection
--

\set afile :srcdir '/csv/plots.csv'
CREATE FOREIGN TABLE csv.plots
(
	country			integer				not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	panel			character varying(20)		not null,
	cluster			character varying(20)		not null,
	plot			character varying(20)		not null,
	plot_geometry		text,
	coordinates_degraded	boolean,
	comment			text
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

/*
--
-- Data for Name: gui_version; Type: TABLE DATA; Schema: gisdata; Owner: adm_nfiesta_gisdata;
--

INSERT INTO gisdata.c_gui_version(id, label, description, label_en, description_en)
VALUES
	(600,'2.0.0','Verze 2.0.0 - GUI aplikace pro pomocná data.','2.0.0','Version 2.0.0 - GUI application for auxiliary data.'),
	(700,'3.0.0','Verze 3.0.0 - GUI aplikace pro pomocná data.','3.0.0','Version 3.0.0 - GUI application for auxiliary data.');

--
-- Data for Name: cm_ext_gui_version; Type: TABLE DATA; Schema: gisdata; Owner: adm_nfiesta_gisdata;
--

INSERT INTO gisdata.cm_ext_gui_version(ext_version, gui_version)
VALUES
	(1000,600),
	(1100,600),
	(1200,700);
*/	

--
-- PostgreSQL database dump complete
--

--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

--
-- PostgreSQL database dump
--


INSERT INTO gisdata.c_estimation_cell_collection(id,label,description,label_en,description_en) VALUES
(1,	'NUTS1-INSPIRE','Nomenclature of Territorial Units for Statistics, level 1 – state.','NUTS1-INSPIRE','Nomenclature of Territorial Units for Statistics, level 1 – state.'),
(2,	'100km-INSPIRE','A collection of estimation cells corresponding to 100km INSPIRE grid.','100km-INSPIRE','A collection of estimation cells corresponding to 100km INSPIRE grid.'),
(3,	'50km-INSPIRE','A collection of estimation cells corresponding to 50km INSPIRE grid.','50km-INSPIRE','A collection of estimation cells corresponding to 50km INSPIRE grid.'),
(4,	'25km-INSPIRE','A collection of estimation cells corresponding to 25km INSPIRE grid.','25km-INSPIRE','A collection of estimation cells corresponding to 25km INSPIRE grid.'),
(5,	'1km-INSPIRE','A collection of estimation cells corresponding to 1km INSPIRE grid.','1km-INSPIRE','A collection of estimation cells corresponding to 1km INSPIRE grid.');

update gisdata.c_estimation_cell_collection set use4estimates = false where id = 5;

drop table if exists csv.inspire_grid;
create table csv.inspire_grid as
with
w_cr as	(
		select
				1 as new_gid,
				gid,
				'NUTS1-INSPIRE' as cell_collection,
				'CZ0 [INSPIRE]' as label,
				'CZ0 – Česká republika [INSPIRE]' as description,
				geom,
				null::integer as cell_sup from gisdata.nuts1_gen
		)
-----------------
,w_100_cover as 	(
				select
				100000 as scalex, -100000 as  scaley,
				--------------------------------range of CR----------------------------
			    -- more precisely the borders of CR from EUROSTAT with a 10 km buffer
			    ST_GeomFromText('POLYGON((4400000 2800000, 5000000 2800000, 5000000 3200000, 4400000 3200000, 4400000 2800000))', 3035) as extent
				)
,w_100_grid_1 as	(
					SELECT 
						(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
						((ST_Xmax(w_100_cover.extent) - ST_Xmin(w_100_cover.extent)) / scalex)::int, ((ST_Ymax(w_100_cover.extent) - ST_Ymin(w_100_cover.extent)) / abs(scaley))::int,
						ST_Xmin(w_100_cover.extent),
						ST_Ymax(w_100_cover.extent),
						scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
					from w_100_cover
					)
,w_100_grid_2 as	(
					select
						row_number() over() as gid,
						format('%s-INSPIRE', '100km') as cell_collection,
						concat('100km', 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
					    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
							'100km', (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
						geom
					from w_100_grid_1
					)
,w_100_grid_all as	(
					select w_100_grid_2.gid, w_100_grid_2.cell_collection, w_100_grid_2.label, w_100_grid_2.description, w_100_grid_2.geom,
					st_centroid(w_100_grid_2.geom) as geom_centroid
					from w_100_grid_2, gisdata.nuts1_gen as t where st_intersects(w_100_grid_2.geom, t.geom)
					)
,w_100_grid as	(
				select
						row_number() over(order by w_100_grid_all.gid) + (select max(new_gid) from w_cr) as new_gid,
						w_100_grid_all.*
				from
						w_100_grid_all
				)
-----------------
,w_50_cover as 	(
				select
				50000 as scalex, -50000 as  scaley,
				--------------------------------range of CR----------------------------
			    -- more precisely the borders of CR from EUROSTAT with a 10 km buffer
			    ST_GeomFromText('POLYGON((4400000 2800000, 5000000 2800000, 5000000 3200000, 4400000 3200000, 4400000 2800000))', 3035) as extent
				)
,w_50_grid_1 as	(
				SELECT 
					(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
					((ST_Xmax(w_50_cover.extent) - ST_Xmin(w_50_cover.extent)) / scalex)::int, ((ST_Ymax(w_50_cover.extent) - ST_Ymin(w_50_cover.extent)) / abs(scaley))::int,
					ST_Xmin(w_50_cover.extent),
					ST_Ymax(w_50_cover.extent),
					scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
				from w_50_cover
				)
,w_50_grid_2 as	(
				select
					row_number() over() as gid,
					format('%s-INSPIRE', '50km') as cell_collection,
					concat('50km', 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
				    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
						'50km', (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
					geom
				from w_50_grid_1
				)
,w_50_grid_all as	(
				select w_50_grid_2.gid, w_50_grid_2.cell_collection, w_50_grid_2.label, w_50_grid_2.description, w_50_grid_2.geom,
				st_centroid(w_50_grid_2.geom) as geom_centroid
				from w_50_grid_2, gisdata.nuts1_gen as t where st_intersects(w_50_grid_2.geom, t.geom)
				)
,w_50_grid as	(
				select
						row_number() over(order by w_50_grid_all.gid) + (select max(new_gid) from w_100_grid) as new_gid,
						w_50_grid_all.*
				from w_50_grid_all
				)
-----------------
,w_25_cover as 	(
				select
				25000 as scalex, -25000 as  scaley,
				--------------------------------range of CR----------------------------
			    -- more precisely the borders of CR from EUROSTAT with a 10 km buffer
			    ST_GeomFromText('POLYGON((4400000 2800000, 5000000 2800000, 5000000 3200000, 4400000 3200000, 4400000 2800000))', 3035) as extent
				)
,w_25_grid_1 as	(
				SELECT 
					(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
					((ST_Xmax(w_25_cover.extent) - ST_Xmin(w_25_cover.extent)) / scalex)::int, ((ST_Ymax(w_25_cover.extent) - ST_Ymin(w_25_cover.extent)) / abs(scaley))::int,
					ST_Xmin(w_25_cover.extent),
					ST_Ymax(w_25_cover.extent),
					scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
				from w_25_cover
				)
,w_25_grid_2 as	(
				select
					row_number() over() as gid,
					format('%s-INSPIRE', '25km') as cell_collection,
					concat('25km', 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
				    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
						'25km', (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
					geom
				from w_25_grid_1
				)
,w_25_grid_all as	(
				select w_25_grid_2.gid, w_25_grid_2.cell_collection, w_25_grid_2.label, w_25_grid_2.description, w_25_grid_2.geom,
				st_centroid(w_25_grid_2.geom) as geom_centroid
				from w_25_grid_2, gisdata.nuts1_gen as t where st_intersects(w_25_grid_2.geom, t.geom)
				)
,w_25_grid as	(
				select
						row_number() over(order by w_25_grid_all.gid) + (select max(new_gid) from w_50_grid) as new_gid,
						w_25_grid_all.*
				from
						w_25_grid_all
				)
-----------------				
,w_1_cover as 	(
				select
				1000 as scalex, -1000 as  scaley,
				--------------------------------range of CR----------------------------
			    -- more precisely the borders of CR from EUROSTAT with a 10 km buffer
			    ST_GeomFromText('POLYGON((4400000 2800000, 5000000 2800000, 5000000 3200000, 4400000 3200000, 4400000 2800000))', 3035) as extent
				)
,w_1_grid_1 as	(
				SELECT 
					(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
					((ST_Xmax(w_1_cover.extent) - ST_Xmin(w_1_cover.extent)) / scalex)::int, ((ST_Ymax(w_1_cover.extent) - ST_Ymin(w_1_cover.extent)) / abs(scaley))::int,
					ST_Xmin(w_1_cover.extent),
					ST_Ymax(w_1_cover.extent),
					scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
				from w_1_cover
				)
,w_1_grid_2 as	(
				select
					row_number() over() as gid,
					format('%s-INSPIRE', '1km') as cell_collection,
					concat('1km', 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
				    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
						'1km', (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
					geom
				from w_1_grid_1
				)
,w_1_grid_all as	(
					select w_1_grid_2.gid, w_1_grid_2.cell_collection, w_1_grid_2.label, w_1_grid_2.description, w_1_grid_2.geom,
					st_centroid(w_1_grid_2.geom) as geom_centroid
					from w_1_grid_2, gisdata.nuts1_gen as t where st_intersects(w_1_grid_2.geom, t.geom)
					)
,w_1_grid as	(
				select
						row_number() over(order by w_1_grid_all.gid) + (select max(new_gid) from w_25_grid) as new_gid,
						w_1_grid_all.*
				from
						w_1_grid_all
				)
-----------------------------------------
-----------------------------------------
-- CELL_SUP --
,w_1_grid_25 as		(select w_1_grid.*, w_25_grid.new_gid as cell_sup from w_1_grid join w_25_grid on st_intersects(w_1_grid.geom_centroid, w_25_grid.geom))
,w_25_grid_50 as	(select w_25_grid.*, w_50_grid.new_gid as cell_sup from w_25_grid join w_50_grid on st_intersects(w_25_grid.geom_centroid, w_50_grid.geom))
,w_50_grid_100 as	(select w_50_grid.*, w_100_grid.new_gid as cell_sup from w_50_grid join w_100_grid on st_intersects(w_50_grid.geom_centroid, w_100_grid.geom))
,w_100_grid_cr as	(select w_100_grid.*, (select max(new_gid) from w_cr) as cell_sup from w_100_grid)
-----------------------------------------
-----------------------------------------
,w_100_grid_ring as	(-- vyber co lezi na hranici CR
					select w_100_grid_cr.* from w_100_grid_cr, gisdata.nuts1_gen as t where st_intersects(w_100_grid_cr.geom, st_exteriorring(t.geom))
					)
,w_100_grid_cut as	(-- orezani a vytvoreni geometrie na hracici CR
					select w_100_grid_ring.new_gid, w_100_grid_ring.cell_collection, w_100_grid_ring.label, w_100_grid_ring.description,
					st_intersection(w_100_grid_ring.geom,t.geom) as geom, w_100_grid_ring.cell_sup
					from w_100_grid_ring, gisdata.nuts1_gen as t
					)
,w_100_grid_cut_type as	(-- prevedeni polygonu na multipolygon
						select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup
						from w_100_grid_cut where st_geometrytype(geom) = 'ST_Polygon'
						union all
						select new_gid, cell_collection, label, description, geom, cell_sup
						from w_100_grid_cut where st_geometrytype(geom) = 'ST_MultiPolygon'
						)
,w_100_res as	(-- kompletace
				select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup from w_100_grid_cr
				where new_gid not in (select new_gid from w_100_grid_cut_type)
				union all
				select new_gid, cell_collection, label, description, geom, cell_sup from w_100_grid_cut_type
				)
-----------------------------------------
,w_50_grid_ring as	(-- vyber co lezi na hranici CR
					select w_50_grid_100.* from w_50_grid_100, gisdata.nuts1_gen as t where st_intersects(w_50_grid_100.geom, st_exteriorring(t.geom))
					)
,w_50_grid_cut as	(-- orezani a vytvoreni geometrie na hracici CR
					select w_50_grid_ring.new_gid, w_50_grid_ring.cell_collection, w_50_grid_ring.label, w_50_grid_ring.description,
					st_intersection(w_50_grid_ring.geom,t.geom) as geom, w_50_grid_ring.cell_sup
					from w_50_grid_ring, gisdata.nuts1_gen as t
					)
,w_50_grid_cut_type as	(-- prevedeni polygonu na multipolygon
						select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup
						from w_50_grid_cut where st_geometrytype(geom) = 'ST_Polygon'
						union all
						select new_gid, cell_collection, label, description, geom, cell_sup
						from w_50_grid_cut where st_geometrytype(geom) = 'ST_MultiPolygon'
						)
,w_50_res as	(-- kompletace
				select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup from w_50_grid_100
				where new_gid not in (select new_gid from w_50_grid_cut_type)
				union all
				select new_gid, cell_collection, label, description, geom, cell_sup from w_50_grid_cut_type
				)
-----------------------------------------
,w_25_grid_ring as	(-- vyber co lezi na hranici CR
					select w_25_grid_50.* from w_25_grid_50, gisdata.nuts1_gen as t where st_intersects(w_25_grid_50.geom, st_exteriorring(t.geom))
					)
,w_25_grid_cut as	(-- orezani a vytvoreni geometrie na hracici CR
					select w_25_grid_ring.new_gid, w_25_grid_ring.cell_collection, w_25_grid_ring.label, w_25_grid_ring.description,
					st_intersection(w_25_grid_ring.geom,t.geom) as geom, w_25_grid_ring.cell_sup
					from w_25_grid_ring, gisdata.nuts1_gen as t
					)
,w_25_grid_cut_type as	(-- prevedeni polygonu na multipolygon
						select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup
						from w_25_grid_cut where st_geometrytype(geom) = 'ST_Polygon'
						union all
						select new_gid, cell_collection, label, description, geom, cell_sup
						from w_25_grid_cut where st_geometrytype(geom) = 'ST_MultiPolygon'
						)
,w_25_res as	(-- kompletace
				select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup from w_25_grid_50
				where new_gid not in (select new_gid from w_25_grid_cut_type)
				union all
				select new_gid, cell_collection, label, description, geom, cell_sup from w_25_grid_cut_type
				)
-----------------------------------------
,w_1_grid_ring as	(-- vyber co lezi na hranici CR
					select w_1_grid_25.* from w_1_grid_25, gisdata.nuts1_gen as t where st_intersects(w_1_grid_25.geom, st_exteriorring(t.geom))
					)
,w_1_grid_cut as	(-- orezani a vytvoreni geometrie na hracici CR
					select w_1_grid_ring.new_gid, w_1_grid_ring.cell_collection, w_1_grid_ring.label, w_1_grid_ring.description,
					st_intersection(w_1_grid_ring.geom,t.geom) as geom, w_1_grid_ring.cell_sup
					from w_1_grid_ring, gisdata.nuts1_gen as t
					)
,w_1_grid_cut_type as	(-- prevedeni polygonu na multipolygon
						select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup
						from w_1_grid_cut where st_geometrytype(geom) = 'ST_Polygon'
						union all
						select new_gid, cell_collection, label, description, geom, cell_sup
						from w_1_grid_cut where st_geometrytype(geom) = 'ST_MultiPolygon'
						)
,w_1_res as	(-- kompletace
				select new_gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup from w_1_grid_25
				where new_gid not in (select new_gid from w_1_grid_cut_type)
				union all
				select new_gid, cell_collection, label, description, geom, cell_sup from w_1_grid_cut_type
				)
-----------------------------------------
-----------------------------------------
,w_res as	(
			select new_gid as gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup from w_cr union all
			select new_gid as gid, cell_collection, label, label as description, geom, cell_sup from w_100_res union all
			select new_gid as gid, cell_collection, label, label as description, geom, cell_sup from w_50_res union all
			select new_gid as gid, cell_collection, label, label as description, geom, cell_sup from w_25_res union all
			select new_gid as gid, cell_collection, label, label as description, geom, cell_sup from w_1_res
			)
select gid, cell_collection, label, description, geom, cell_sup from w_res order by gid;

ALTER TABLE csv.inspire_grid ADD PRIMARY KEY (gid);
CREATE INDEX idx_inspire_grid_geom ON csv.inspire_grid USING gist (geom);
alter table csv.inspire_grid owner to adm_nfiesta_gisdata;
grant select on table csv.inspire_grid to public;


insert into gisdata.c_estimation_cell (id, estimation_cell_collection, label, description, label_en, description_en)
select
		gid,
		(select id from gisdata.c_estimation_cell_collection where label = cell_collection),
		label,
		description,
		label,
		description
from
		csv.inspire_grid order by gid;

analyze gisdata.c_estimation_cell;


INSERT INTO gisdata.cm_estimation_cell_collection(id,estimation_cell_collection,estimation_cell_collection_lowest,estimation_cell_collection_highest)
VALUES
(1,1,5,1), -- nuts1
(2,2,5,1), -- inspire 100km
(3,3,5,1), -- inspire 50km
(4,4,5,1), -- inspire 25km
(5,5,5,1); -- inspire 1km


with
w as	(
		select
				cec.id as gid,
				geom,
				cec.id as estimation_cell 
		from
				gisdata.c_estimation_cell as cec
		inner join
				csv.inspire_grid as t
		on
				cec.label = t.label
		)
insert into gisdata.f_a_cell(gid,geom,estimation_cell)
select w.gid, w.geom, w.estimation_cell from w order by w.gid;


analyze gisdata.f_a_cell;


insert into gisdata.cm_f_a_cell(cell,cell_sup)
select gid, cell_sup from csv.inspire_grid order by gid;


analyze gisdata.cm_f_a_cell;


drop table if exists csv.inspire_grid;

--ALTER TABLE gisdata.f_a_cell ALTER COLUMN geom TYPE geometry(MultiPolygon, 3035) USING ST_SetSRID(geom, 3035);


--SELECT pg_catalog.setval('gisdata.c_estimation_cell_collection_id_seq', (select max(id) from gisdata.c_estimation_cell_collection), true);
--SELECT pg_catalog.setval('gisdata.c_estimation_cell_id_seq', (select max(id) from gisdata.c_estimation_cell), true);
SELECT pg_catalog.setval('gisdata.cm_estimation_cell_collection_id_seq', (select max(id) from gisdata.cm_estimation_cell_collection), true);
SELECT pg_catalog.setval('gisdata.f_a_cell_gid_seq', (select max(gid) from gisdata.f_a_cell), true);
--SELECT pg_catalog.setval('gisdata.cm_f_a_cell_id_seq', (select max(id) from gisdata.cm_f_a_cell), true);


/*
CREATE FOREIGN TABLE csv.plots
(
	country			integer				not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	panel			character varying(20)		not null,
	cluster			character varying(20)		not null,
	plot			character varying(20)		not null,
	plot_geometry		text,
	coordinates_degraded	boolean,
	comment			text
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename '/builds/nfiesta/nfiesta_gisdata/extension/csv/plots.csv' );
*/


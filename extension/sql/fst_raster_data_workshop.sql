------------------------------------------------------------------------------;
\set srcdir `echo $SRC_DIR`
------------------------------------------------------------------------------;
\set afile :srcdir '/csv/fty_10_100m_cr.csv'
CREATE FOREIGN TABLE csv.r_fty_10_100m_cr
(
    rid integer NOT NULL,
	tile_count integer NOT NULL,
    rast public.raster
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

INSERT INTO gisdata.r_fty_10_100m_cr(rid,tile_count,rast)
SELECT
	rid::integer,
    tile_count::integer,
	rast
FROM
	csv.r_fty_10_100m_cr;
------------------------------------------------------------------------------;
\set afile :srcdir '/csv/tcd_10_100m_cr.csv'
CREATE FOREIGN TABLE csv.r_tcd_10_100m_cr
(
    rid integer NOT NULL,
	tile_count integer NOT NULL,
    rast public.raster
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

INSERT INTO gisdata.r_tcd_10_100m_cr(rid,tile_count,rast)
SELECT
	rid::integer,
    tile_count::integer,
	rast
FROM
	csv.r_tcd_10_100m_cr;
------------------------------------------------------------------------------;
\set afile :srcdir '/csv/gfch_10_100m_cr.csv'
CREATE FOREIGN TABLE csv.r_gfch_10_100m_cr
(
    rid integer NOT NULL,
	tile_count integer NOT NULL,
    rast public.raster
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

INSERT INTO gisdata.r_gfch_10_100m_cr(rid,tile_count,rast)
SELECT
	rid::integer,
    tile_count::integer,
	rast
FROM
	csv.r_gfch_10_100m_cr;
------------------------------------------------------------------------------;

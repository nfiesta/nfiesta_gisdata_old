--
-- PostgreSQL database dump
--

-- Dumped from database version 12.6 (Debian 12.6-1.pgdg100+1)
-- Dumped by pg_dump version 12.6 (Debian 12.6-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: dlt_2012_100m; Type: TABLE; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

CREATE TABLE gisdata.dlt_2012_100m (
    rid integer NOT NULL,
    rast public.raster,
    CONSTRAINT enforce_height_rast CHECK ((public.st_height(rast) = 2)),
    CONSTRAINT enforce_nodata_values_rast CHECK ((public._raster_constraint_nodata_values(rast) = '{0.0000000000}'::numeric[])),
    CONSTRAINT enforce_num_bands_rast CHECK ((public.st_numbands(rast) = 1)),
    CONSTRAINT enforce_out_db_rast CHECK ((public._raster_constraint_out_db(rast) = '{f}'::boolean[])),
    CONSTRAINT enforce_pixel_types_rast CHECK ((public._raster_constraint_pixel_types(rast) = '{8BUI}'::text[])),
    CONSTRAINT enforce_same_alignment_rast CHECK (public.st_samealignment(rast, '0100000000000000000000594000000000000059C042F05DE6DA2A21C1BC37C27B8B412FC1000000000000000000000000000000008A15000001000100'::public.raster)),
    CONSTRAINT enforce_scalex_rast CHECK ((round((public.st_scalex(rast))::numeric, 10) = round((100)::numeric, 10))),
    CONSTRAINT enforce_scaley_rast CHECK ((round((public.st_scaley(rast))::numeric, 10) = round((- (100)::numeric), 10))),
    CONSTRAINT enforce_srid_rast CHECK ((public.st_srid(rast) = 5514)),
    CONSTRAINT enforce_width_rast CHECK ((public.st_width(rast) = ANY (ARRAY[2, 1])))
);


ALTER TABLE gisdata.dlt_2012_100m OWNER TO adm_nfiesta_gisdata;

--
-- Name: TABLE dlt_2012_100m; Type: COMMENT; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

COMMENT ON TABLE gisdata.dlt_2012_100m IS '
1. Data are comming from Copernicus pan Eropean forest monitoring program
https://land.copernicus.eu/pan-european/high-resolution-layers/forests.
Namely:
* Dominant Leaf Type (DLT) 2012.
See
https://land.copernicus.eu/pan-european/high-resolution-layers/forests/forest-type-1/status-maps/2012?tab=metadata 
for the original Constraints related to access and use.
2. This project is not officially endorsed by the Union.
3. Original rasters are clipped to the area of interest -- Moravia region of Czech Republic.
4. The data remain the sole property of the European Union.
';


--
-- Name: dlt_2012_100m_rid_seq; Type: SEQUENCE; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

CREATE SEQUENCE gisdata.dlt_2012_100m_rid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gisdata.dlt_2012_100m_rid_seq OWNER TO adm_nfiesta_gisdata;

--
-- Name: dlt_2012_100m_rid_seq; Type: SEQUENCE OWNED BY; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER SEQUENCE gisdata.dlt_2012_100m_rid_seq OWNED BY gisdata.dlt_2012_100m.rid;


--
-- Name: tcd_2012_100m; Type: TABLE; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

CREATE TABLE gisdata.tcd_2012_100m (
    rid integer NOT NULL,
    rast public.raster,
    CONSTRAINT enforce_height_rast CHECK ((public.st_height(rast) = 2)),
    CONSTRAINT enforce_nodata_values_rast CHECK ((public._raster_constraint_nodata_values(rast) = '{0.0000000000}'::numeric[])),
    CONSTRAINT enforce_num_bands_rast CHECK ((public.st_numbands(rast) = 1)),
    CONSTRAINT enforce_out_db_rast CHECK ((public._raster_constraint_out_db(rast) = '{f}'::boolean[])),
    CONSTRAINT enforce_pixel_types_rast CHECK ((public._raster_constraint_pixel_types(rast) = '{8BUI}'::text[])),
    CONSTRAINT enforce_same_alignment_rast CHECK (public.st_samealignment(rast, '0100000000000000000000594000000000000059C042F05DE6DA2A21C1BC37C27B8B412FC1000000000000000000000000000000008A15000001000100'::public.raster)),
    CONSTRAINT enforce_scalex_rast CHECK ((round((public.st_scalex(rast))::numeric, 10) = round((100)::numeric, 10))),
    CONSTRAINT enforce_scaley_rast CHECK ((round((public.st_scaley(rast))::numeric, 10) = round((- (100)::numeric), 10))),
    CONSTRAINT enforce_srid_rast CHECK ((public.st_srid(rast) = 5514)),
    CONSTRAINT enforce_width_rast CHECK ((public.st_width(rast) = ANY (ARRAY[2, 1])))
);


ALTER TABLE gisdata.tcd_2012_100m OWNER TO adm_nfiesta_gisdata;

--
-- Name: TABLE tcd_2012_100m; Type: COMMENT; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

COMMENT ON TABLE gisdata.tcd_2012_100m IS '
1. Data are comming from Copernicus pan Eropean forest monitoring program
https://land.copernicus.eu/pan-european/high-resolution-layers/forests.
Namely:
* Tree Cover Density (TCD) 2012.
See
https://land.copernicus.eu/pan-european/high-resolution-layers/forests/forest-type-1/status-maps/2012?tab=metadata 
for the original Constraints related to access and use.
2. This project is not officially endorsed by the Union.
3. Original rasters are clipped to the area of interest -- Moravia region of Czech Republic.
4. The data remain the sole property of the European Union.
';


--
-- Name: tcd_2012_100m_rid_seq; Type: SEQUENCE; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

CREATE SEQUENCE gisdata.tcd_2012_100m_rid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gisdata.tcd_2012_100m_rid_seq OWNER TO adm_nfiesta_gisdata;

--
-- Name: tcd_2012_100m_rid_seq; Type: SEQUENCE OWNED BY; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER SEQUENCE gisdata.tcd_2012_100m_rid_seq OWNED BY gisdata.tcd_2012_100m.rid;


--
-- Name: dlt_2012_100m rid; Type: DEFAULT; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER TABLE ONLY gisdata.dlt_2012_100m ALTER COLUMN rid SET DEFAULT nextval('gisdata.dlt_2012_100m_rid_seq'::regclass);


--
-- Name: tcd_2012_100m rid; Type: DEFAULT; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER TABLE ONLY gisdata.tcd_2012_100m ALTER COLUMN rid SET DEFAULT nextval('gisdata.tcd_2012_100m_rid_seq'::regclass);


--
-- Name: dlt_2012_100m dlt_2012_100m_pkey; Type: CONSTRAINT; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER TABLE ONLY gisdata.dlt_2012_100m
    ADD CONSTRAINT dlt_2012_100m_pkey PRIMARY KEY (rid);


--
-- Name: dlt_2012_100m enforce_max_extent_rast; Type: CHECK CONSTRAINT; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER TABLE gisdata.dlt_2012_100m
    ADD CONSTRAINT enforce_max_extent_rast CHECK ((public.st_envelope(rast) OPERATOR(public.@) '01030000208A150000010000000500000042F05DE61A0D27C1DE1BE1BD85BA32C142F05DE61A0D27C1BC37C27B8B412FC184E0BBCC65581AC1BC37C27B8B412FC184E0BBCC65581AC1DE1BE1BD85BA32C142F05DE61A0D27C1DE1BE1BD85BA32C1'::public.geometry)) NOT VALID;


--
-- Name: tcd_2012_100m enforce_max_extent_rast; Type: CHECK CONSTRAINT; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER TABLE gisdata.tcd_2012_100m
    ADD CONSTRAINT enforce_max_extent_rast CHECK ((public.st_envelope(rast) OPERATOR(public.@) '01030000208A150000010000000500000042F05DE61A0D27C1DE1BE1BD85BA32C142F05DE61A0D27C1BC37C27B8B412FC184E0BBCC65581AC1BC37C27B8B412FC184E0BBCC65581AC1DE1BE1BD85BA32C142F05DE61A0D27C1DE1BE1BD85BA32C1'::public.geometry)) NOT VALID;


--
-- Name: tcd_2012_100m tcd_2012_100m_pkey; Type: CONSTRAINT; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER TABLE ONLY gisdata.tcd_2012_100m
    ADD CONSTRAINT tcd_2012_100m_pkey PRIMARY KEY (rid);


--
-- Name: dlt_2012_100m_st_convexhull_idx; Type: INDEX; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

CREATE INDEX dlt_2012_100m_st_convexhull_idx ON gisdata.dlt_2012_100m USING gist (public.st_convexhull(rast));


--
-- Name: tcd_2012_100m_st_convexhull_idx; Type: INDEX; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

CREATE INDEX tcd_2012_100m_st_convexhull_idx ON gisdata.tcd_2012_100m USING gist (public.st_convexhull(rast));



---------------------------------------------------------------------------------------------------;
-- c_dlt_2012_100m_bands
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM gisdata.c_dlt_2012_100m_bands;

	DROP TABLE IF EXISTS gisdata.c_dlt_2012_100m_bands CASCADE;

	CREATE TABLE gisdata.c_dlt_2012_100m_bands
	(
		id integer NOT NULL,
		label character varying(100) NOT NULL,
		description	text,
		CONSTRAINT pkey__c_dlt_2012_100m_bands PRIMARY KEY (id)
	);

	COMMENT ON TABLE  gisdata.c_dlt_2012_100m_bands IS 'Dial bands in raster';
	COMMENT ON COLUMN gisdata.c_dlt_2012_100m_bands.id IS 'Code of category';
	COMMENT ON COLUMN gisdata.c_dlt_2012_100m_bands.label IS 'Label of category';
	COMMENT ON COLUMN gisdata.c_dlt_2012_100m_bands.description IS 'Description of category';
	COMMENT ON CONSTRAINT pkey__c_dlt_2012_100m_bands ON gisdata.c_dlt_2012_100m_bands IS 'Primay key of the table';

	ALTER TABLE gisdata.c_dlt_2012_100m_bands OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE gisdata.c_dlt_2012_100m_bands TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE gisdata.c_dlt_2012_100m_bands TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE gisdata.c_dlt_2012_100m_bands TO public;

	INSERT INTO gisdata.c_dlt_2012_100m_bands (id, label, description)
	VALUES
	(1, 'Dominant Leaf Type', 'Dominant Leaf Type');
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;

---------------------------------------------------------------------------------------------------;
-- c_dlt_2012_100m_category
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM gisdata.c_dlt_2012_100m_category;

	DROP TABLE IF EXISTS gisdata.c_dlt_2012_100m_category CASCADE;

	CREATE TABLE gisdata.c_dlt_2012_100m_category
	(
		id integer NOT NULL,
		label character varying(100) NOT NULL,
		description	text,
		CONSTRAINT pkey__c_dlt_2012_100m_category PRIMARY KEY (id)
	);

	COMMENT ON TABLE  gisdata.c_dlt_2012_100m_category IS 'Dial categories in raster';
	COMMENT ON COLUMN gisdata.c_dlt_2012_100m_category.id IS 'Code of category';
	COMMENT ON COLUMN gisdata.c_dlt_2012_100m_category.label IS 'Label of category';
	COMMENT ON COLUMN gisdata.c_dlt_2012_100m_category.description IS 'Description of category';
	COMMENT ON CONSTRAINT pkey__c_dlt_2012_100m_category ON gisdata.c_dlt_2012_100m_category IS 'Primay key of the table';

	ALTER TABLE gisdata.c_dlt_2012_100m_category OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE gisdata.c_dlt_2012_100m_category TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE gisdata.c_dlt_2012_100m_category TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE gisdata.c_dlt_2012_100m_category TO public;

	INSERT INTO gisdata.c_dlt_2012_100m_category (id, label, description)
	VALUES
	(1, 'Coniferous', 'Coniferous'),
    (2, 'Broadleaves', 'Broadleaves');
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;

---------------------------------------------------------------------------------------------------;
-- c_tcd_2012_100m_bands
---------------------------------------------------------------------------------------------------;
--	SELECT * FROM gisdata.c_tcd_2012_100m_bands;

	DROP TABLE IF EXISTS gisdata.c_tcd_2012_100m_bands CASCADE;

	CREATE TABLE gisdata.c_tcd_2012_100m_bands
	(
		id integer NOT NULL,
		label character varying(100) NOT NULL,
		description	text,
		CONSTRAINT pkey__c_tcd_2012_100m_bands PRIMARY KEY (id)
	);

	COMMENT ON TABLE  gisdata.c_tcd_2012_100m_bands IS 'Dial bands in raster';
	COMMENT ON COLUMN gisdata.c_tcd_2012_100m_bands.id IS 'Code of category';
	COMMENT ON COLUMN gisdata.c_tcd_2012_100m_bands.label IS 'Label of category';
	COMMENT ON COLUMN gisdata.c_tcd_2012_100m_bands.description IS 'Description of category';
	COMMENT ON CONSTRAINT pkey__c_tcd_2012_100m_bands ON gisdata.c_tcd_2012_100m_bands IS 'Primay key of the table';

	ALTER TABLE gisdata.c_tcd_2012_100m_bands OWNER TO adm_nfiesta_gisdata;
	GRANT ALL ON TABLE gisdata.c_tcd_2012_100m_bands TO adm_nfiesta_gisdata;
	GRANT SELECT, UPDATE, INSERT, DELETE, TRUNCATE ON TABLE gisdata.c_tcd_2012_100m_bands TO app_nfiesta_gisdata;
	GRANT SELECT ON TABLE gisdata.c_tcd_2012_100m_bands TO public;

	INSERT INTO gisdata.c_tcd_2012_100m_bands (id, label, description)
	VALUES
	(1, 'Tree Cover Density', 'Tree Cover Density');
---------------------------------------------------------------------------------------------------;
---------------------------------------------------------------------------------------------------;

--
-- PostgreSQL database dump complete
--


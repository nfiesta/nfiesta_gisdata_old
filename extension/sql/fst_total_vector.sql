--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

---------------------------------------------------------------------------------------------------
--  [INSPIRE 25x25km] -- OLIL_2013_FOREST --
-- STEP 1
with
w1 as 	(
		select * from gisdata.fn_get_gids4aux_total_app
			(
			101,
			array[48,52,53],
			(select max(id) from gisdata.c_gui_version),
			false
			)
		),
w2 as 	(	
		select
		(gisdata.fn_get_aux_total_app(config_id,estimation_cell,gid)) as res
		from w1 where step = 1 order by estimation_cell, gid
		)
insert into gisdata.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version)
select 
	(res).config,
	(res).estimation_cell,
	(res).cell,
	(res).aux_total,
	(res).ext_version,
	(select max(id) from gisdata.c_gui_version) as gui_version
from
	w2 order by (res).cell
;
-- STEP 2
with
w1 as 	(
		select * from gisdata.fn_get_gids4aux_total_app
			(
			101,
			array[48,52],
			(select max(id) from gisdata.c_gui_version),
			false
			)
		),
w2 as 	(
		select
		(gisdata.fn_get_aux_total4estimation_cell_app(config_id,estimation_cell,gid,false)) as res
		from w1 where step = 2 order by estimation_cell
		)
insert into gisdata.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version)
select 
	(res).config,
	(res).estimation_cell,
	(res).cell,
	(res).aux_total,
	(res).ext_version,
	(select max(id) from gisdata.c_gui_version) as gui_version
from
	w2 order by (res).estimation_cell
;
---------------------------------------------------------------------------------------------------
--  [INSPIRE 25x25km] -- OLIL_2013_NON-FOREST --
with
w1 as 	(
		select * from gisdata.fn_get_gids4aux_total_app
			(
			102,
			array[48,52,53],
			(select max(id) from gisdata.c_gui_version),
			false
			)
		),
w2 as 	(	
		select
		(gisdata.fn_get_aux_total_app(config_id,estimation_cell,gid)) as res
		from w1 order by estimation_cell
		)
insert into gisdata.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version)
select 
	(res).config,
	(res).estimation_cell,
	(res).cell,
	(res).aux_total,
	(res).ext_version,
	(select max(id) from gisdata.c_gui_version) as gui_version
from
	w2 order by (res).estimation_cell
;
---------------------------------------------------------------------------------------------------	
-- [INSPIRE 50x50km] -- OLIL_2013_FOREST --
with
w1 as 	(
		select * from gisdata.fn_get_gids4aux_total_app
			(
			101,
			array[21],
			(select max(id) from gisdata.c_gui_version),
			false
			)
		),
w2 as 	(
		select
		(gisdata.fn_get_aux_total4estimation_cell_app(config_id,estimation_cell,gid,false)) as res
		from w1 where step = 2 order by estimation_cell
		)
insert into gisdata.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version)
select 
	(res).config,
	(res).estimation_cell,
	(res).cell,
	(res).aux_total,
	(res).ext_version,
	(select max(id) from gisdata.c_gui_version) as gui_version
from
	w2 order by (res).estimation_cell
;
---------------------------------------------------------------------------------------------------	
-- [INSPIRE 50x50km] -- OLIL_2013_NON-FOREST --
with
w1 as 	(
		select * from gisdata.fn_get_gids4aux_total_app
			(
			102,
			array[21],
			(select max(id) from gisdata.c_gui_version),
			false
			)
		),
w2 as 	(	
		select
		(gisdata.fn_get_aux_total_app(config_id,estimation_cell,gid)) as res
		from w1 where step = 2 order by estimation_cell
		)
insert into gisdata.t_aux_total(config,estimation_cell,cell,aux_total,ext_version,gui_version)
select 
	(res).config,
	(res).estimation_cell,
	(res).cell,
	(res).aux_total,
	(res).ext_version,
	(select max(id) from gisdata.c_gui_version) as gui_version
from
	w2 order by (res).estimation_cell
;
---------------------------------------------------------------------------------------------------
-- CHECK
WITH
w1 AS	(
		select config, estimation_cell, aux_total
		from gisdata.t_aux_total
		where estimation_cell in (SELECT id FROM gisdata.c_estimation_cell)
		)
select
	c_estimation_cell.label as estimation_cell_label,
	t_config.label as area_domain_category,
	round(w1.aux_total::numeric,7) as aux_total
from w1
inner join gisdata.t_config on w1.config = t_config.id
inner join gisdata.c_estimation_cell on w1.estimation_cell = c_estimation_cell.id
order by estimation_cell_label, area_domain_category;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

\set srcdir `echo $SRC_DIR`
CREATE SCHEMA csv;

--
-- Data for Name: olil_2013_forest; Type: TABLE DATA; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

\set afile :srcdir '/csv/olil_2013_forest.csv'
CREATE FOREIGN TABLE csv.olil_2013_forest
(
	gid			integer		not null,
	olil_2013 integer not null,
	olil_geometry		text		not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.olil_2013_forest 
(
	gid integer NOT NULL,
	olil_2013 integer NOT NULL,
	geom geometry(POLYGON, 5514) NOT NULL
);

ALTER TABLE gisdata.olil_2013_forest OWNER TO adm_nfiesta_gisdata;

--
-- Name: olil_2013_forest_gid_seq; Type: SEQUENCE; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

CREATE SEQUENCE gisdata.olil_2013_forest_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.olil_2013_forest_gid_seq OWNER TO adm_nfiesta_gisdata;

--
-- Name: olil_2013_forest_gid_seq; Type: SEQUENCE OWNED BY; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER SEQUENCE gisdata.olil_2013_forest_gid_seq OWNED BY gisdata.olil_2013_forest.gid;

--
-- Name: olil_2013_forest gid; Type: DEFAULT; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER TABLE ONLY gisdata.olil_2013_forest ALTER COLUMN gid SET DEFAULT nextval('gisdata.olil_2013_forest_gid_seq'::regclass);

--
-- Name: olil_2013_forest_pkey; Type: CONSTRAINT; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER TABLE ONLY gisdata.olil_2013_forest ADD CONSTRAINT olil_2013_forest_pkey PRIMARY KEY (gid);

--
-- Name: olil_2013_forest enforce_dims_geom; Type: CHECK CONSTRAINT; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER TABLE gisdata.olil_2013_forest ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

--
-- Name: olil_2013_forest enforce_geotype_geom; Type: CHECK CONSTRAINT; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER TABLE gisdata.olil_2013_forest ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POLYGON'::text);

--
-- Name: olil_2013_forest enforce_srid_geom; Type: CHECK CONSTRAINT; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

ALTER TABLE gisdata.olil_2013_forest ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 5514);

--
-- Name: spidx__olil_2013_forest__geom; Type: INDEX; Schema: gisdata; Owner: adm_nfiesta_gisdata
--

CREATE INDEX spidx__olil_2013_forest__geom ON gisdata.olil_2013_forest USING gist (geom);

INSERT INTO gisdata.olil_2013_forest(olil_2013,geom)
SELECT
	olil_2013,
	ST_GeomFromEWKT(olil_geometry) AS geom
FROM
	csv.olil_2013_forest;

--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

--
-- PostgreSQL database dump
--


INSERT INTO gisdata.c_estimation_cell_collection(id,label,description,label_en,description_en) VALUES
(16,	'INSPIRE-MIX','INSPIRE-MIX','INSPIRE-MIX','INSPIRE-MIX.'),
(17,	'50km-INSPIRE-MIX','50km-INSPIRE-MIX.','50km-INSPIRE-MIX','50km-INSPIRE-MIX.'),
(18,	'25km-INSPIRE-MIX','25km-INSPIRE-MIX.','25km-INSPIRE-MIX','25km-INSPIRE-MIX'),
(19,	'1km-INSPIRE-MIX','1km-INSPIRE-MIX.','1km-INSPIRE-MIX','1km-INSPIRE-MIX.');

update gisdata.c_estimation_cell_collection set use4estimates = false where label = '1km-INSPIRE-MIX';

drop table if exists csv.inspire_grid_mix;
create table csv.inspire_grid_mix as
with
w_50_cover as 	(
				select
				50000 as scalex, -50000 as  scaley,
				--------------------------------range of CR----------------------------
			    -- more precisely the borders of CR from EUROSTAT with a 10 km buffer
			    ST_GeomFromText('POLYGON((4400000 2800000, 5000000 2800000, 5000000 3200000, 4400000 3200000, 4400000 2800000))', 3035) as extent
				)
,w_50_grid_1 as	(
				SELECT 
					(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
					((ST_Xmax(w_50_cover.extent) - ST_Xmin(w_50_cover.extent)) / scalex)::int, ((ST_Ymax(w_50_cover.extent) - ST_Ymin(w_50_cover.extent)) / abs(scaley))::int,
					ST_Xmin(w_50_cover.extent),
					ST_Ymax(w_50_cover.extent),
					scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
				from w_50_cover
				)
,w_50_grid_2 as	(
				select
					row_number() over() as gid,
					format('%s-INSPIRE', '50km') as cell_collection,
					concat('50km', 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
				    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
						'50km', (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
					geom
				from w_50_grid_1
				)
-----------------
,w_heighest as	(
				select
						1 as new_gid,
						1 as gid,
						'INSPIRE-MIX' as cell_collection,
						'INSPIRE-MIX' as label,
						'INSPIRE-MIX' as description,						
						st_union(w_50_grid_2.geom) as geom,
						null::integer as cell_sup
				from
						w_50_grid_2 where label in ('50kmN3050E4650','50kmN3000E4750')
				)
				--select * from w_heighest;
-----------------
				--select * from w_heigher
,w_50_grid as	(
				select row_number() over() + (select max(new_gid) from w_heighest) as new_gid, * from w_50_grid_2 where label in ('50kmN3050E4650','50kmN3000E4750')
				)
				--select * from w_50_grid;
-----------------
,w_25_cover as 	(
				select
				25000 as scalex, -25000 as  scaley,
				--------------------------------range of CR----------------------------
			    -- more precisely the borders of CR from EUROSTAT with a 10 km buffer
			    ST_GeomFromText('POLYGON((4400000 2800000, 5000000 2800000, 5000000 3200000, 4400000 3200000, 4400000 2800000))', 3035) as extent
				)
,w_25_grid_1 as	(
				SELECT 
					(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
					((ST_Xmax(w_25_cover.extent) - ST_Xmin(w_25_cover.extent)) / scalex)::int, ((ST_Ymax(w_25_cover.extent) - ST_Ymin(w_25_cover.extent)) / abs(scaley))::int,
					ST_Xmin(w_25_cover.extent),
					ST_Ymax(w_25_cover.extent),
					scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
				from w_25_cover
				)
,w_25_grid_2 as	(
				select
					row_number() over() as gid,
					format('%s-INSPIRE', '25km') as cell_collection,
					concat('25km', 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
				    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
						'25km', (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
					geom
				from w_25_grid_1
				)				
,w_25_grid_all as	(
				select w_25_grid_2.gid, w_25_grid_2.cell_collection, w_25_grid_2.label, w_25_grid_2.description, w_25_grid_2.geom,
				st_centroid(w_25_grid_2.geom) as geom_centroid
				from w_25_grid_2, w_50_grid as t where st_intersects(st_centroid(w_25_grid_2.geom), t.geom)
				)				
,w_25_grid as	(
				select
						row_number() over(order by w_25_grid_all.gid) + (select max(new_gid) from w_50_grid) as new_gid,
						w_25_grid_all.*
				from
						w_25_grid_all
				)
				--select * from w_25_grid;
-----------------				
,w_1_cover as 	(
				select
				1000 as scalex, -1000 as  scaley,
				--------------------------------range of CR----------------------------
			    -- more precisely the borders of CR from EUROSTAT with a 10 km buffer
			    ST_GeomFromText('POLYGON((4400000 2800000, 5000000 2800000, 5000000 3200000, 4400000 3200000, 4400000 2800000))', 3035) as extent
				)
,w_1_grid_1 as	(
				SELECT 
					(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
					((ST_Xmax(w_1_cover.extent) - ST_Xmin(w_1_cover.extent)) / scalex)::int, ((ST_Ymax(w_1_cover.extent) - ST_Ymin(w_1_cover.extent)) / abs(scaley))::int,
					ST_Xmin(w_1_cover.extent),
					ST_Ymax(w_1_cover.extent),
					scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
				from w_1_cover
				)
,w_1_grid_2 as	(
				select
					row_number() over() as gid,
					format('%s-INSPIRE', '1km') as cell_collection,
					concat('1km', 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
				    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
						'1km', (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
					geom
				from w_1_grid_1
				)
,w_1_grid_all as	(
					select w_1_grid_2.gid, w_1_grid_2.cell_collection, w_1_grid_2.label, w_1_grid_2.description, w_1_grid_2.geom,
					st_centroid(w_1_grid_2.geom) as geom_centroid
					from w_1_grid_2, w_25_grid as t where st_intersects(st_centroid(w_1_grid_2.geom), t.geom)
					)
					--select * from w_1_grid_all;
,w_1_grid as	(
				select
						row_number() over(order by w_1_grid_all.gid) + (select max(new_gid) from w_25_grid) as new_gid,
						w_1_grid_all.*
				from
						w_1_grid_all
				)
				--select * from w_1_grid;
-----------------------------------------
-----------------------------------------
-- CELL_SUP --
-- CELL_SUP --
,w_1_grid_25 as			(select w_1_grid.*, w_25_grid.new_gid as cell_sup from w_1_grid join w_25_grid on st_intersects(w_1_grid.geom_centroid, w_25_grid.geom))
,w_25_grid_50 as		(select w_25_grid.*, w_50_grid.new_gid as cell_sup from w_25_grid join w_50_grid on st_intersects(w_25_grid.geom_centroid, w_50_grid.geom))
,w_50_grid_heighest as	(select w_50_grid.*, (select max(new_gid) from w_heighest) as cell_sup from w_50_grid)
-----------------------------------------
-----------------------------------------
,w_res as	(
			select new_gid as gid, cell_collection, label, description, st_multi(geom) as geom, cell_sup from w_heighest union all
			select new_gid as gid, cell_collection, label, label as description, st_multi(geom) as geom, cell_sup from w_50_grid_heighest union all
			select new_gid as gid, cell_collection, label, label as description, st_multi(geom) as geom, cell_sup from w_25_grid_50 union all
			select new_gid as gid, cell_collection, label, label as description, st_multi(geom) as geom, cell_sup from w_1_grid_25
			)
select gid, cell_collection, label, description, geom, cell_sup from w_res order by gid;

ALTER TABLE csv.inspire_grid_mix ADD PRIMARY KEY (gid);
CREATE INDEX idx_inspire_grid_mix_geom ON csv.inspire_grid_mix USING gist (geom);
alter table csv.inspire_grid_mix owner to adm_nfiesta_gisdata;
grant select on table csv.inspire_grid_mix to public;


with
w1 as	(
		select
				gid,
				case when gid is distinct from 1
				then cell_collection || '-MIX' else cell_collection
				end as cell_collection,
				label
		from
				csv.inspire_grid_mix
		)
insert into gisdata.c_estimation_cell (id, estimation_cell_collection, label, description, label_en, description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		(select id from gisdata.c_estimation_cell_collection where label = cell_collection) as cell_collection,
		label,
		label,
		label,
		label
from
		w1 order by gid;

analyze gisdata.c_estimation_cell;


INSERT INTO gisdata.cm_estimation_cell_collection(id,estimation_cell_collection,estimation_cell_collection_lowest,estimation_cell_collection_highest)
VALUES
(16,16,19,16), -- inspire-mix
(17,17,19,16), -- inspire-mix 50km
(18,18,19,16), -- inspire-mix 25km
(19,19,19,16); -- inspire-mix 1km


with
w1 as	(
		select
				gid,
				case when gid is distinct from 1
				then cell_collection || '-MIX' else cell_collection
				end as cell_collection,
				label,
				geom
		from
				csv.inspire_grid_mix
		)
,w2 as	(
		select * from gisdata.c_estimation_cell where estimation_cell_collection in
		(select id from gisdata.c_estimation_cell_collection where label in (select cell_collection from w1))
		)
,w3 as	(
		select w1.*, w2.* from w1 inner join w2 on w1.label = w2.label
		)
insert into gisdata.f_a_cell(gid,geom,estimation_cell)
select w3.id as gid, w3.geom, w3.id as estimation_cell from w3 order by w3.gid;


analyze gisdata.f_a_cell;


with
w1 as	(
		select
				gid,
				case when gid is distinct from 1
				then cell_collection || '-MIX' else cell_collection
				end as cell_collection,
				label,
				cell_sup
		from
				csv.inspire_grid_mix
		)
,w2 as	(
		select * from gisdata.c_estimation_cell where estimation_cell_collection in
		(select id from gisdata.c_estimation_cell_collection where label in (select cell_collection from w1))
		)
,w3 as	(
		select w1.*, w2.* from w1 inner join w2 on w1.label = w2.label
		)
,w4 as	(
		select w3.id as cell, w3.gid, w3.cell_sup from w3 order by w3.gid
		)
,w5 as	(
		select w4.cell, w4.cell_sup from w4 where cell_sup is null
		)
,w6 as	(
		select
				w4.cell,
				t.cell as cell_sup
		from
				w4
				inner join w4 as t on w4.cell_sup = t.gid
		)
insert into gisdata.cm_f_a_cell(cell,cell_sup)
select w5.cell, w5.cell_sup from w5 union all
select w6.cell, w6.cell_sup from w6 order by cell;


analyze gisdata.cm_f_a_cell;


drop table if exists csv.inspire_grid_mix;


--SELECT pg_catalog.setval('gisdata.c_estimation_cell_collection_id_seq', (select max(id) from gisdata.c_estimation_cell_collection), true);
--SELECT pg_catalog.setval('gisdata.c_estimation_cell_id_seq', (select max(id) from gisdata.c_estimation_cell), true);
SELECT pg_catalog.setval('gisdata.cm_estimation_cell_collection_id_seq', (select max(id) from gisdata.cm_estimation_cell_collection), true);
SELECT pg_catalog.setval('gisdata.cm_f_a_cell_id_seq', (select max(id) from gisdata.cm_f_a_cell), true);
SELECT pg_catalog.setval('gisdata.f_a_cell_gid_seq', (select max(gid) from gisdata.f_a_cell), true);



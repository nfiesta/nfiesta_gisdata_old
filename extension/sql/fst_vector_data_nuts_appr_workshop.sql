--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--


\set srcdir `echo $SRC_DIR`


--------------------------------------------------------------------------------------------------;
-- NUTS1_GEN_APPROXIMATE --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts1_gen_approximate.csv'
CREATE FOREIGN TABLE csv.nuts1_gen_approximate
(
	gid integer not null,
	nuts_geometry text not null,
	label text not null,
	description text not null,
	nuts1 integer not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts1_gen_approximate 
(
	gid integer NOT NULL,
	geom geometry(MULTIPOLYGON, 3035) NOT NULL,
	label text not null,
	description text not null,
	nuts1 integer not null
);

ALTER TABLE gisdata.nuts1_gen_approximate OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts1_gen_approximate_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts1_gen_approximate_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts1_gen_approximate_gid_seq OWNED BY gisdata.nuts1_gen_approximate.gid;

ALTER TABLE ONLY gisdata.nuts1_gen_approximate ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts1_gen_approximate_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts1_gen_approximate ADD CONSTRAINT nuts1_gen_approximate_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts1_gen_approximate ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts1_gen_approximate ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text);

ALTER TABLE gisdata.nuts1_gen_approximate ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts1_gen_approximate__geom ON gisdata.nuts1_gen_approximate USING gist (geom);
CREATE INDEX idx__nuts1_gen_approximate__label ON gisdata.nuts1_gen_approximate USING btree(label);
CREATE INDEX idx__nuts1_gen_approximate__nuts1 ON gisdata.nuts1_gen_approximate USING btree(nuts1);

INSERT INTO gisdata.nuts1_gen_approximate(gid,geom,label,description,nuts1)
SELECT
	gid,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	label,
	description,
	nuts1
FROM
	csv.nuts1_gen_approximate order by gid;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;


--------------------------------------------------------------------------------------------------;
-- NUTS2_GEN_APPROXIMATE --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts2_gen_approximate.csv'
CREATE FOREIGN TABLE csv.nuts2_gen_approximate
(
	gid integer not null,
	nuts_geometry text not null,
	label text not null,
	description text not null,
	nuts1 integer not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts2_gen_approximate 
(
	gid integer NOT NULL,
	geom geometry(MULTIPOLYGON, 3035) NOT NULL,
	label text not null,
	description text not null,
	nuts1 integer not null
);

ALTER TABLE gisdata.nuts2_gen_approximate OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts2_gen_approximate_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts2_gen_approximate_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts2_gen_approximate_gid_seq OWNED BY gisdata.nuts2_gen_approximate.gid;

ALTER TABLE ONLY gisdata.nuts2_gen_approximate ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts2_gen_approximate_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts2_gen_approximate ADD CONSTRAINT nuts2_gen_approximate_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts2_gen_approximate ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts2_gen_approximate ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text);

ALTER TABLE gisdata.nuts2_gen_approximate ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts2_gen_approximate__geom ON gisdata.nuts2_gen_approximate USING gist (geom);
CREATE INDEX idx__nuts2_gen_approximate__label ON gisdata.nuts2_gen_approximate USING btree(label);
CREATE INDEX idx__nuts2_gen_approximate__nuts1 ON gisdata.nuts2_gen_approximate USING btree(nuts1);

INSERT INTO gisdata.nuts2_gen_approximate(gid,geom,label,description,nuts1)
SELECT
	gid,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	label,
	description,
	nuts1
FROM
	csv.nuts2_gen_approximate order by gid;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;


--------------------------------------------------------------------------------------------------;
-- NUTS3_GEN_APPROXIMATE --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts3_gen_approximate.csv'
CREATE FOREIGN TABLE csv.nuts3_gen_approximate
(
	gid integer not null,
	nuts_geometry text not null,
	label text not null,
	description text not null,
	nuts2 integer not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts3_gen_approximate 
(
	gid integer NOT NULL,
	geom geometry(MULTIPOLYGON, 3035) NOT NULL,
	label text not null,
	description text not null,
	nuts2 integer not null
);

ALTER TABLE gisdata.nuts3_gen_approximate OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts3_gen_approximate_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts3_gen_approximate_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts3_gen_approximate_gid_seq OWNED BY gisdata.nuts3_gen_approximate.gid;

ALTER TABLE ONLY gisdata.nuts3_gen_approximate ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts3_gen_approximate_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts3_gen_approximate ADD CONSTRAINT nuts3_gen_approximate_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts3_gen_approximate ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts3_gen_approximate ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text);

ALTER TABLE gisdata.nuts3_gen_approximate ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts3_gen_approximate__geom ON gisdata.nuts3_gen_approximate USING gist (geom);
CREATE INDEX idx__nuts3_gen_approximate__label ON gisdata.nuts3_gen_approximate USING btree(label);
CREATE INDEX idx__nuts3_gen_approximate__nuts2 ON gisdata.nuts3_gen_approximate USING btree(nuts2);

INSERT INTO gisdata.nuts3_gen_approximate(gid,geom,label,description,nuts2)
SELECT
	gid,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	label,
	description,
	nuts2
FROM
	csv.nuts3_gen_approximate order by gid;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;


--------------------------------------------------------------------------------------------------;
-- NUTS4_GEN_APPROXIMATE --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts4_gen_approximate.csv'
CREATE FOREIGN TABLE csv.nuts4_gen_approximate
(
	gid integer not null,
	nuts_geometry text not null,
	label text not null,
	description text not null,
	nuts3 integer not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts4_gen_approximate 
(
	gid integer NOT NULL,
	geom geometry(MULTIPOLYGON, 3035) NOT NULL,
	label text not null,
	description text not null,
	nuts3 integer not null
);

ALTER TABLE gisdata.nuts4_gen_approximate OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts4_gen_approximate_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts4_gen_approximate_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts4_gen_approximate_gid_seq OWNED BY gisdata.nuts4_gen_approximate.gid;

ALTER TABLE ONLY gisdata.nuts4_gen_approximate ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts4_gen_approximate_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts4_gen_approximate ADD CONSTRAINT nuts4_gen_approximate_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts4_gen_approximate ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts4_gen_approximate ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text);

ALTER TABLE gisdata.nuts4_gen_approximate ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts4_gen_approximate__geom ON gisdata.nuts4_gen_approximate USING gist (geom);
CREATE INDEX idx__nuts4_gen_approximate__label ON gisdata.nuts4_gen_approximate USING btree(label);
CREATE INDEX idx__nuts4_gen_approximate__nuts3 ON gisdata.nuts4_gen_approximate USING btree(nuts3);

INSERT INTO gisdata.nuts4_gen_approximate(gid,geom,label,description,nuts3)
SELECT
	gid,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	label,
	description,
	nuts3
FROM
	csv.nuts4_gen_approximate order by gid;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;


--------------------------------------------------------------------------------------------------;
-- NUTS1, NUTS2, NUTS3, NUTS4 and NUTS4-1km-INSPIRE => APPROXIMATE
--------------------------------------------------------------------------------------------------;
INSERT INTO gisdata.c_estimation_cell_collection(id,label,description,label_en,description_en) VALUES
(11,	'NUTS1-APPROXIMATE','Nomenclature of Territorial Units for Statistics, level 1 – state.','NUTS1-APPROXIMATE','Nomenclature of Territorial Units for Statistics, level 1 – state.'),
(12,	'NUTS2-APPROXIMATE','Nomenklatura územních statistických jednotek 2. úroveň – region.','NUTS2-APPROXIMATE','Nomenclature of Territorial Units for Statistics, level 2 – area.'),
(13,	'NUTS3-APPROXIMATE','Nomenklatura územních statistických jednotek 3. úroveň – kraj.','NUTS3-APPROXIMATE','Nomenclature of Territorial Units for Statistics, level 3 – region.'),
(14,	'NUTS4-APPROXIMATE','Nomenklatura územních statistických jednotek 4. úroveň – okres.','NUTS4-APPROXIMATE','Nomenclature of Territorial Units for Statistics, level 4 – district.'),
(15,	'NUTS4-1km-inspire-APPROXIMATE','A collection of estimation cells corresponding to 1km INSPIRE grid and nomenclature of Territorial Units for Statistics, level 4 – okres.','NUTS4-1km-inspire-APPROXIMATE','A collection of estimation cells corresponding to 1km INSPIRE grid and nomenclature of Territorial Units for Statistics, level 4 – okres.');

INSERT INTO gisdata.cm_estimation_cell_collection(id,estimation_cell_collection,estimation_cell_collection_lowest,estimation_cell_collection_highest)
VALUES
(11,11,15,11), -- nuts1-approximate
(12,12,15,11), -- nuts2-approximate
(13,13,15,11), -- nuts3-approximate
(14,14,15,11), -- nuts4-approximate
(15,15,15,11); -- nuts4-1km-inspire-approximate

insert into gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		(select id from gisdata.c_estimation_cell_collection where label = 'NUTS1-APPROXIMATE') as estimation_cell_collection,
		'CZ0 [APPROXIMATE]'::varchar as label,
		'CZ0 – Česká republika [APPROXIMATE]'::text as description,
		'CZ0 [APPROXIMATE]'::varchar as label_en,
		'CZ0 – Česká republika [APPROXIMATE]'::text as description_en
from
		gisdata.nuts1_gen_approximate order by gid;
---------------------------------------
insert into gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		(select id from gisdata.c_estimation_cell_collection where label = 'NUTS2-APPROXIMATE') as estimation_cell_collection,
		label,
		description,
		label,
		description
from
		gisdata.nuts2_gen_approximate order by gid;
---------------------------------------
insert into gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		(select id from gisdata.c_estimation_cell_collection where label = 'NUTS3-APPROXIMATE') as estimation_cell_collection,
		label,
		description,
		label,
		description	
from
		gisdata.nuts3_gen_approximate order by gid;	
---------------------------------------
insert into gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		(select id from gisdata.c_estimation_cell_collection where label = 'NUTS4-APPROXIMATE') as estimation_cell_collection,
		label,
		description,
		label,
		description	
from
		gisdata.nuts4_gen_approximate order by gid;
---------------------------------------
---------------------------------------
with
w1 as	(
		select t1.*, t2.geom from
		(
			select id as gid, id as estimation_cell, label from gisdata.c_estimation_cell
			where estimation_cell_collection in
			(select id from gisdata.c_estimation_cell_collection
			where label in ('NUTS1-APPROXIMATE','NUTS2-APPROXIMATE','NUTS3-APPROXIMATE','NUTS4-APPROXIMATE'))
		) as t1
		inner join
		(
		select geom, label || ' [APPROXIMATE]' as label from gisdata.nuts1_gen_approximate union all
		select geom, label from gisdata.nuts2_gen_approximate union all
		select geom, label from gisdata.nuts3_gen_approximate union all
		select geom, label from gisdata.nuts4_gen_approximate
		) as t2
		on t1.label = t2.label
		)
insert into gisdata.f_a_cell(gid,geom,estimation_cell)
select gid, geom, estimation_cell from w1 order by gid;
---------------------------------------
---------------------------------------
with
w_nuts1 as	(
			select gid as cell, null::integer as cell_sup from gisdata.f_a_cell
			where estimation_cell =
				(
				select id from gisdata.c_estimation_cell where estimation_cell_collection =
				(select id from gisdata.c_estimation_cell_collection where label = 'NUTS1-APPROXIMATE')
				)
			)
,w_nuts2 as	(
			select
					t1.cell,
					--t1.label,
					--t2.nuts1,
					--t3.label as label_nuts1,
					--t4.id as estimation_cell,
					t5.gid as cell_sup
			from
			(
				select a.gid as cell, b.label from gisdata.f_a_cell as a inner join
				(select id, label from gisdata.c_estimation_cell where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS2-APPROXIMATE')) as b
				on a.estimation_cell = b.id
			) as t1
			inner join gisdata.nuts2_gen_approximate as t2 on t1.label = t2.label
			inner join gisdata.nuts1_gen_approximate as t3 on t2.nuts1 = t3.gid

			inner join	(
							select * from gisdata.c_estimation_cell where estimation_cell_collection =
							(select id from gisdata.c_estimation_cell_collection where label = 'NUTS1-APPROXIMATE')
						) as t4
						on (t3.label || ' [APPROXIMATE]') = t4.label
		
			inner join gisdata.f_a_cell as t5 on t4.id = t5.estimation_cell			
			)
,w_nuts3 as	(
			select
					t1.cell,
					--t1.label,
					--t2.nuts2,
					--t3.label as label_nuts2,
					--t4.id as estimation_cell,
					t5.gid as cell_sup
			from
			(
				select a.gid as cell, b.label from gisdata.f_a_cell as a inner join
				(select id, label from gisdata.c_estimation_cell where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS3-APPROXIMATE')) as b
				on a.estimation_cell = b.id
			) as t1
			inner join gisdata.nuts3_gen_approximate as t2 on t1.label = t2.label
			inner join gisdata.nuts2_gen_approximate as t3 on t2.nuts2 = t3.gid

			inner join	(
							select * from gisdata.c_estimation_cell where estimation_cell_collection =
							(select id from gisdata.c_estimation_cell_collection where label = 'NUTS2-APPROXIMATE')
						) as t4
						on t3.label = t4.label
		
			inner join gisdata.f_a_cell as t5 on t4.id = t5.estimation_cell			
			)
,w_nuts4 as	(
			select
					t1.cell,
					--t1.label,
					--t2.nuts3,
					--t3.label as label_nuts3,
					--t4.id as estimation_cell,
					t5.gid as cell_sup
			from
			(
				select a.gid as cell, b.label from gisdata.f_a_cell as a inner join
				(select id, label from gisdata.c_estimation_cell where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS4-APPROXIMATE')) as b
				on a.estimation_cell = b.id
			) as t1
			inner join gisdata.nuts4_gen_approximate as t2 on t1.label = t2.label
			inner join gisdata.nuts3_gen_approximate as t3 on t2.nuts3 = t3.gid

			inner join	(
							select * from gisdata.c_estimation_cell where estimation_cell_collection =
							(select id from gisdata.c_estimation_cell_collection where label = 'NUTS3-APPROXIMATE')
						) as t4
						on t3.label = t4.label
		
			inner join gisdata.f_a_cell as t5 on t4.id = t5.estimation_cell			
			)
,w_res as	(
			select * from w_nuts1 union all
			select * from w_nuts2 union all
			select * from w_nuts3 union all
			select * from w_nuts4
			)
insert into gisdata.cm_f_a_cell(cell,cell_sup)
select cell, cell_sup from w_res order by cell;
---------------------------------------
---------------------------------------


--------------------------------------------------------------------------------------------------;
-- NUTS4_GEN_1km_INSPIRE_APPROXIMATE --
--------------------------------------------------------------------------------------------------;
\set afile :srcdir '/csv/nuts4_gen_1km_inspire_approximate.csv'
CREATE FOREIGN TABLE csv.nuts4_gen_1km_inspire_approximate
(
	gid integer not null,
	cell_collection text not null,
	label text not null,
	nuts_geometry text not null,
	nuts4_label text not null,
	nuts3_label text not null,
	nuts2_label text not null,
	nuts1_label text not null
)
SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');

CREATE TABLE gisdata.nuts4_gen_1km_inspire_approximate 
(
	gid integer NOT NULL,
	cell_collection text not null,
	label text not null,
	geom geometry(MULTIPOLYGON, 3035) NOT NULL,
	nuts4_label text not null,
	nuts3_label text not null,
	nuts2_label text not null,
	nuts1_label text not null
);

ALTER TABLE gisdata.nuts4_gen_1km_inspire_approximate OWNER TO adm_nfiesta_gisdata;

CREATE SEQUENCE gisdata.nuts4_gen_1km_inspire_approximate_gid_seq
	AS integer
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;

ALTER TABLE gisdata.nuts4_gen_1km_inspire_approximate_gid_seq OWNER TO adm_nfiesta_gisdata;

ALTER SEQUENCE gisdata.nuts4_gen_1km_inspire_approximate_gid_seq OWNED BY gisdata.nuts4_gen_1km_inspire_approximate.gid;

ALTER TABLE ONLY gisdata.nuts4_gen_1km_inspire_approximate ALTER COLUMN gid SET DEFAULT nextval('gisdata.nuts4_gen_1km_inspire_approximate_gid_seq'::regclass);

ALTER TABLE ONLY gisdata.nuts4_gen_1km_inspire_approximate ADD CONSTRAINT nuts4_gen_1km_inspire_approximate_pkey PRIMARY KEY (gid);

ALTER TABLE gisdata.nuts4_gen_1km_inspire_approximate ADD CONSTRAINT enforce_dims_geom CHECK (st_ndims(geom) = 2);

ALTER TABLE gisdata.nuts4_gen_1km_inspire_approximate ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text);

ALTER TABLE gisdata.nuts4_gen_1km_inspire_approximate ADD CONSTRAINT enforce_srid_geom CHECK (st_srid(geom) = 3035);

CREATE INDEX spidx__nuts4_gen_1km_inspire_approximate__geom ON gisdata.nuts4_gen_1km_inspire_approximate USING gist (geom);
CREATE INDEX idx__nuts4_gen_1km_inspire_approximate__cell_collection ON gisdata.nuts4_gen_1km_inspire_approximate USING btree(cell_collection);
CREATE INDEX idx__nuts4_gen_1km_inspire_approximate__label ON gisdata.nuts4_gen_1km_inspire_approximate USING btree(label);
CREATE INDEX idx__nuts4_gen_1km_inspire_approximate__nuts4_label ON gisdata.nuts4_gen_1km_inspire_approximate USING btree(nuts4_label);
CREATE INDEX idx__nuts4_gen_1km_inspire_approximate__nuts3_label ON gisdata.nuts4_gen_1km_inspire_approximate USING btree(nuts3_label);
CREATE INDEX idx__nuts4_gen_1km_inspire_approximate__nuts2_label ON gisdata.nuts4_gen_1km_inspire_approximate USING btree(nuts2_label);
CREATE INDEX idx__nuts4_gen_1km_inspire_approximate__nuts1_label ON gisdata.nuts4_gen_1km_inspire_approximate USING btree(nuts1_label);

INSERT INTO gisdata.nuts4_gen_1km_inspire_approximate(gid,cell_collection,label,geom,nuts4_label,nuts3_label,nuts2_label,nuts1_label)
SELECT
	gid,
	cell_collection,
	label,
	ST_GeomFromEWKT(nuts_geometry) AS geom,
	nuts4_label,
	nuts3_label,
	nuts2_label,
	nuts1_label
FROM
	csv.nuts4_gen_1km_inspire_approximate order by gid;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;

analyze gisdata.nuts4_gen_1km_inspire_approximate;


update gisdata.c_estimation_cell_collection set use4estimates = false where label = 'NUTS4-1km-inspire-APPROXIMATE';


insert into gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en)
select
		gid + (select max(id) from gisdata.c_estimation_cell) as id,
		(select id from gisdata.c_estimation_cell_collection where label = 'NUTS4-1km-inspire-APPROXIMATE') as estimation_cell_collection,
		label as label,
		label as description,
		label as label_en,
		label as description_en
from
		gisdata.nuts4_gen_1km_inspire_approximate order by gid;

insert into gisdata.f_a_cell(gid,geom,estimation_cell)
select
		t1.gid + (select max(gid) from gisdata.f_a_cell) as gid,
		t1.geom,
		t2.id as estimation_cell
from
		gisdata.nuts4_gen_1km_inspire_approximate as t1
		inner join	(
					select * from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = 'NUTS4-1km-inspire-APPROXIMATE')
					) as t2 on t1.label = t2.label
order
		by t2.id;

analyze gisdata.f_a_cell;


with
w1 as	(
		select
				t1.gid as cell,
				t1.estimation_cell,
				t3.nuts4_label,
				t4.id as id_estimation_cell_nuts4,
				t5.gid as cell_sup
		from
				(
				select * from gisdata.f_a_cell where estimation_cell in (select id from gisdata.c_estimation_cell
				where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS4-1km-inspire-APPROXIMATE'))
				) as t1
		inner join	(
					select * from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = 'NUTS4-1km-inspire-APPROXIMATE')
					) as t2 on t1.estimation_cell = t2.id
		inner join gisdata.nuts4_gen_1km_inspire_approximate as t3 on t2.label = t3.label
		inner join	(
					select * from gisdata.c_estimation_cell where estimation_cell_collection = 
					(select id from gisdata.c_estimation_cell_collection where label = 'NUTS4-APPROXIMATE')
					) as t4 on t3.nuts4_label = t4.label
		inner join	(
					select * from gisdata.f_a_cell where estimation_cell in (select id from gisdata.c_estimation_cell
					where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS4-APPROXIMATE'))
					) as t5 on t4.id = t5.estimation_cell
		)
insert into gisdata.cm_f_a_cell(cell,cell_sup)
select cell, cell_sup from w1 order by cell;

analyze gisdata.cm_f_a_cell;
--------------------------------------------------------------------------------------------------;
--------------------------------------------------------------------------------------------------;


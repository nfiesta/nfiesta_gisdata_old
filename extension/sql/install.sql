--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_raster;
CREATE EXTENSION file_fdw;
CREATE SERVER csv_files FOREIGN DATA WRAPPER file_fdw;
CREATE SCHEMA nfiesta_etl;

\set srcdir `echo $SRC_DIR`
\set afile :srcdir '/csv/plots.csv'
CREATE FOREIGN TABLE nfiesta_etl.plots (
	country			integer				not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	panel			character varying(20)		not null,
	cluster			character varying(20)		not null,
	plot			character varying(20)		not null,
	plot_geometry		text,
	coordinates_degraded	boolean,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', filename :'afile');
CREATE SCHEMA gisdata;
CREATE EXTENSION nfiesta_gisdata WITH SCHEMA gisdata;

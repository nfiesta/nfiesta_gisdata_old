--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DDL
create or replace view export_api.aux_total as
with
w_config as		(
			-- vyber vypocitanych config z tabulky t_aux_total
			-- a vyber potrebnych atributu, kde je hlavne
			-- dulezity atribut config_query, ktery v pripade hodnoty 500 nam 
			-- rika, ze jde o refefenci [pozn. referencni data se v tabulce]
			-- t_aux_total neduplikuji
			select
				t_config.id,
				t_config.auxiliary_variable_category,
				t_config.config_collection,
				t_config.config_query,
				t_config.label,
				t_config.categories
			from
				@extschema@.t_config
			where
				config_collection in	(
							select distinct config_collection from @extschema@.t_config
							where id in (select distinct config from @extschema@.t_aux_total)
							)
			order 
				by id
			)
,w_no_ref as 		(
			-- vyber zaznamu z withu w_config, ktere nejsou referenci [config_query != 500]
			select w_config.* from w_config
			where w_config.config_query is distinct from 500
			)
,w_ref as 		(
			-- vyber zaznamu z withu w_config, ktere jsou referecni [config_query = 500]
			select w_config.* from w_config
			where w_config.config_query = 500
			)
,w_data_no_ref as 	(
			-- vyber dat z tabulky t_aux_total pro zaznamy z withu w_no_ref,
			-- kde vyber lze primo provest pres atribut config = id z withu
			-- w_no_ref a HLAVNE estimation_cell is not null
			select
				t_aux_total.id,
				t_aux_total.config as config_res,
				t_aux_total.estimation_cell,
				t_aux_total.aux_total,
				t_aux_total.ext_version,
				t_aux_total.gui_version
			from @extschema@.t_aux_total
			inner join w_no_ref
			on t_aux_total.config = w_no_ref.id
			and t_aux_total.estimation_cell is not null
			)
-------------------------------------------------------------------------------
,w_data_ref as 		(
			-- vyber dat z tabulky t_aux_total pro zaznamy z withu w_ref
			select
				t_aux_total.id,
				w_ref.id as config_res,
				t_aux_total.estimation_cell,
				t_aux_total.aux_total,
				t_aux_total.ext_version,
				t_aux_total.gui_version
			from @extschema@.t_aux_total 
			inner join w_ref
			on t_aux_total.config::character varying = w_ref.categories
			order by w_ref.id
			)
-------------------------------------------------------------------------------
,w_no_ref_max as 	(
			-- z withu w_data_nor_ref a z withu w_data_ref se musi pro unikatni klic,
			-- resp pro unikatni kombinaci config, estimation_cell, ext_version
			-- se musi vybrat zaznam pro nejaktualnejsi EXT_VERSION
			select config_res, estimation_cell, max(ext_version) as max_ext_version
			from w_data_no_ref group by config_res, estimation_cell
			)
-------------------------------------------------------------------------------
,w_ref_max as 		(
			select config_res, estimation_cell, max(ext_version) as max_ext_version
			from w_data_ref group by config_res, estimation_cell
			)
-------------------------------------------------------------------------------
,w_data_no_ref_res as 	(
			select
				w_data_no_ref.config_res as config,
				w_data_no_ref.estimation_cell,
				w_data_no_ref.aux_total
			from w_data_no_ref inner join w_no_ref_max
			on w_data_no_ref.config_res = w_no_ref_max.config_res
			and w_data_no_ref.estimation_cell = w_no_ref_max.estimation_cell
			and w_data_no_ref.ext_version = w_no_ref_max.max_ext_version
			)
-------------------------------------------------------------------------------
,w_data_ref_res as 	(
			select
				w_data_ref.config_res as config,
				w_data_ref.estimation_cell,
				w_data_ref.aux_total
			from w_data_ref inner join w_ref_max
			on w_data_ref.config_res = w_ref_max.config_res
			and w_data_ref.estimation_cell = w_ref_max.estimation_cell
			and w_data_ref.ext_version = w_ref_max.max_ext_version
			)
-------------------------------------------------------------------------------
,w_data_res as 		(
			select * from w_data_no_ref_res union all
			select * from w_data_ref_res
			)
-------------------------------------------------------------------------------
select
	t_config_collection.auxiliary_variable,
	t_config.auxiliary_variable_category,
	--w_data_res.config,
	w_data_res.estimation_cell,
	w_data_res.aux_total
from 
	w_data_res

	left join @extschema@.t_config on w_data_res.config = t_config.id
	left join @extschema@.t_config_collection on t_config.config_collection = t_config_collection.id
UNION ALL
SELECT
	0 AS auxiliary_variable,
	0 AS auxiliary_variable_category,
	estimation_cell,
	aux_total
FROM
	(SELECT estimation_cell, sum(ST_Area(geom))/10000.0 AS aux_total FROM @extschema@.f_a_cell GROUP BY estimation_cell) AS t1
ORDER BY
	auxiliary_variable_category, estimation_cell
;


-- authorization
ALTER TABLE export_api.aux_total OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.aux_total TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.aux_total TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.aux_total TO public;

-- documentation
COMMENT ON VIEW export_api.aux_total IS 'The view displays data from the t_aux_total table.';
COMMENT ON COLUMN export_api.aux_total.auxiliary_variable IS 'Foreign key to superior c_auxiliary_variable.';
COMMENT ON COLUMN export_api.aux_total.auxiliary_variable_category IS 'Foreign key to c_auxiliary_variable_category.';
COMMENT ON COLUMN export_api.aux_total.estimation_cell IS 'Foreign key to c_estimation_cell.';
COMMENT ON COLUMN export_api.aux_total.aux_total IS 'Total of auxiliary local density.';

--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP VIEW IF EXISTS export_api.estimation_cell;

-- DDL
CREATE OR REPLACE VIEW export_api.estimation_cell AS
SELECT
	t2.label AS estimation_cell_collection, t1.label, t1.description
FROM
	@extschema@.c_estimation_cell AS t1
INNER JOIN
	@extschema@.c_estimation_cell_collection AS t2
ON t1.estimation_cell_collection = t2.id
;

-- authorization
ALTER TABLE export_api.estimation_cell OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.estimation_cell TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.estimation_cell TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.estimation_cell TO public;

-- documentation
COMMENT ON VIEW export_api.estimation_cell IS 'View with list of estimation cell.';


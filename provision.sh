    # turn off JIT
    sudo sed -i "s/#jit = on/jit = off/" /etc/postgresql/12/main/postgresql.conf
    sudo sed -i "s/#max_locks_per_transaction = 64/max_locks_per_transaction = 256/" /etc/postgresql/12/main/postgresql.conf
    sudo sed -i "s/#from_collapse_limit = 8/from_collapse_limit = 80/" /etc/postgresql/12/main/postgresql.conf
    sudo sed -i "s/#join_collapse_limit = 8/join_collapse_limit = 80/" /etc/postgresql/12/main/postgresql.conf
    sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" /etc/postgresql/12/main/postgresql.conf
    sudo sed -i "/host    all             all             127.0.0.1\/32            md5/ a host    all             vagrant         172.0.0.0\/8              md5" /etc/postgresql/12/main/pg_hba.conf
    # start postgres
    sudo service postgresql restart
    # create DB user (preferably with same name as Linux user to bypass password provision)
    sudo -u postgres psql -c "CREATE USER vagrant SUPERUSER;"
    sudo -u postgres psql -c "ALTER USER vagrant WITH PASSWORD 'vagrant';"
    sudo -u postgres psql -c "create role adm_nfiesta_gisdata;"
    sudo -u postgres psql -c "create role app_nfiesta_gisdata;"
    # install extension [gisdata](https://gitlab.com/nfiesta/nfiesta_gisdata/-/tree/master/extension)
    # for instruction to clone (with git LFS) see https://gitlab.com/nfiesta/nfiesta_gisdata/-/blob/master/dev_env/README.md
    cd extension
    sudo make install
    # run automated tests
    make installcheck-all
